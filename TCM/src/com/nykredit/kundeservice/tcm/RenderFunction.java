package com.nykredit.kundeservice.tcm;

import java.awt.Color;
import java.awt.Component;

import com.nykredit.kundeservice.tcm.datatypes.calculations.AHT;

/**
 * 
 * Functions used to define colors in the varous table renderers
 * @author thra
 *
 */
public class RenderFunction {

	public static void setColoringPct(Component cell, String header, Object value, double goal){
		try{
			int val = Integer.parseInt(value.toString().replace("%", ""));
			double green = goal * 100;
			double red = green - 5;
			
			if(header.contentEquals("Krav***")){
				double kravRed = green + (green - red);
				
				if(val < kravRed & val > green){
					cell.setBackground(ColorConstant.yellow);
					cell.setForeground(Color.black);
				}else if (val > green){
					cell.setBackground(ColorConstant.red);
					cell.setForeground(Color.black);
				}else{
					cell.setBackground(ColorConstant.green);
					cell.setForeground(Color.black);
				}
			} else{
				if(val>red & val < green){
					cell.setBackground(ColorConstant.yellow);
					cell.setForeground(Color.black);
				}else if (val >= green){
					cell.setBackground(ColorConstant.green);
					cell.setForeground(Color.black);
				}else{
					cell.setBackground(ColorConstant.red);
					cell.setForeground(Color.black);
				}
			}
		}catch(Exception e){}
    }
	
	public static void setColoringPctReversed(Component cell,String header,Object value,double goal){
		try{
			int val = Integer.parseInt(value.toString().replace("%",""));
			
			if(header.contentEquals("Krav***")){
				if(val >= goal * 100)
					cell.setBackground(ColorConstant.green);
				else if(val < goal * 100 * 1.05)
					cell.setBackground(ColorConstant.red);
				else
					cell.setBackground(ColorConstant.yellow);
			} else{
				if(val <= goal * 100)
					cell.setBackground(ColorConstant.green);
				else if(val >= goal * 100 * 1.05)
					cell.setBackground(ColorConstant.red);
				else
					cell.setBackground(ColorConstant.yellow);
			}
			
			cell.setForeground(Color.black);
		}catch(Exception e){}
    }
	
	public static void setColoringIndex(Component cell,String header,Object value,double goal){
		try{
			double val = Double.parseDouble(value.toString().replace(",", "."));
			double green = goal;
			double red = green * 0.95;
			
			if(header.contentEquals("Krav***")){
				double kravRed = green + (green - red);
				
				if(val < kravRed & val > green){
					cell.setBackground(ColorConstant.yellow);
					cell.setForeground(Color.black);
				}else if (val > green){
					cell.setBackground(ColorConstant.red);
					cell.setForeground(Color.black);
				}else{
					cell.setBackground(ColorConstant.green);
					cell.setForeground(Color.black);
				}
			} else{
				if(val > red & val < green){
					cell.setBackground(ColorConstant.yellow);
					cell.setForeground(Color.black);
				}else if (val >= green){
					cell.setBackground(ColorConstant.green);
					cell.setForeground(Color.black);
				}else{
					cell.setBackground(ColorConstant.red);
					cell.setForeground(Color.black);
				}
			}
		}catch(Exception e){}
    }
	
    public static void setColoringInt(Component cell,String header,Object value,double goal){
  		try{
  			double green = goal/12;
  			double dob = Double.parseDouble(value.toString().replace(".",""));
  			double red = green*.95;

  			if(
  					header.contentEquals("1. kv.")|
  					header.contentEquals("2. kv.")|
  					header.contentEquals("3. kv.")|
  					header.contentEquals("4. kv.")
  					){
  				green = (goal/12)*3;
  				red = green * .95;
  			}
  			
  			if(header.contentEquals("�TD*")){
  				if(DateConstant.completedMonths == 0){
  					green = (goal/12)*12;
  	  				red = green * .95;					
  				}else{
  					green = (goal/12)*DateConstant.completedMonths;
  	  				red = green * .95;
  				}
  			}

  			if(header.contentEquals("Estimat**")){
  				green = green * 12;
  				red = green * .95;
  			}
  			
  			if(header.contentEquals("Krav**")){
  				green = green * 12;
  				red = green * .95;
  			}  			
  			
			if(header.contentEquals("Krav***")){
				
				double kravRed = green+(green-red);
				
				if(dob<kravRed & dob>green){
						cell.setBackground(ColorConstant.yellow);
						cell.setForeground(Color.black);
				}else if (dob>green){
					cell.setBackground(ColorConstant.red);
					cell.setForeground(Color.black);
				}else{
					cell.setBackground(ColorConstant.green);
					cell.setForeground(Color.black);
				}
			}	
			else{
	  			if(dob>red & dob<green){
						cell.setBackground(ColorConstant.yellow);
						cell.setForeground(Color.black);
				}else if (dob>=green){
					cell.setBackground(ColorConstant.green);
					cell.setForeground(Color.black);
				}else{
					cell.setBackground(ColorConstant.red);
					cell.setForeground(Color.black);
				}
			}
  		}catch(Exception e){}
    }
    
    public static void setColoringAHT(Component cell,String header,Object value,double goal){
		try{
			AHT aht = (AHT)value;
			
			if(header.contentEquals("Krav***")){
				double red = goal * .95;
				
				if(aht.getResult() >= goal){
					cell.setBackground(ColorConstant.red);
				}else if (aht.getResult() <= red){
					cell.setBackground(ColorConstant.green);
				}else{
					cell.setBackground(ColorConstant.yellow);
				}
			}else{
				double red = goal * 1.05;
				if(aht.getResult() <= goal){
					cell.setBackground(ColorConstant.green);
				}else if (aht.getResult() >= red){
					cell.setBackground(ColorConstant.red);
				}else{
					cell.setBackground(ColorConstant.yellow);
				}
			}
			
			cell.setForeground(Color.black);
		}catch(Exception e){}
    }

    public static void setColoringBoolean(Component cell,String header,Object value){
  		try{
  			if(value.toString().contentEquals("Opfyldt")){
				cell.setBackground(ColorConstant.green);
				cell.setForeground(Color.black);	
  			}
  			if(value.toString().contentEquals("Ikke opfyldt")){
				cell.setBackground(ColorConstant.red);
				cell.setForeground(Color.black);	
  			}  						
  		}catch(Exception e){}
    }
    
    public static void setColoringTime(Component cell,String header,Object value, int goal){
		try{
			int val = Integer.parseInt(value.toString().replace("%",""));
			
			double green = goal*100;
			double red = green-5;
			
			if(header.contentEquals("Krav***")){
				double kravRed = green+(green-red);
				
				if(val<kravRed & val>green){
					cell.setBackground(ColorConstant.yellow);
					cell.setForeground(Color.black);
				}else if (val>green){
					cell.setBackground(ColorConstant.red);
					cell.setForeground(Color.black);
				}else{
					cell.setBackground(ColorConstant.green);
					cell.setForeground(Color.black);
				}
			} else{
				if(val>red & val<green){
					cell.setBackground(ColorConstant.yellow);
					cell.setForeground(Color.black);
				}else if (val>=green){
					cell.setBackground(ColorConstant.green);
					cell.setForeground(Color.black);
				}else{
					cell.setBackground(ColorConstant.red);
					cell.setForeground(Color.black);
				}
			}
		}catch(Exception e){}
    }
}
