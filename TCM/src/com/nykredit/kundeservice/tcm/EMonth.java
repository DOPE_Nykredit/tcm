package com.nykredit.kundeservice.tcm;

import java.util.Calendar;

/**
 * 
 * Defines the months used in the program
 *
 * @author thra
 */
	public enum EMonth{
	  JANUARY("jan"),
	  FEBRUARY("feb"),
	  MARCH("mar"),
	  APRIL("apr"),
	  MAY("maj"),
	  JUNE("jun"),
	  JULY("jul"),
	  AUGUST("aug"),
	  SEPTEMBER("sep"),
	  OCTOBER("okt"),
	  NOVEMBER("nov"),
	  DECEMBER("dec");
	
	  private String stringName;
	  
	  EMonth(String stringName){
		  this.stringName = stringName;
	  }
	  
	  public int getNumberInYear(){
		  return this.ordinal() + 1;
	  }
	  
	  public int getQuarter(){
		  return this.ordinal() / 3 + 1;
	  }
	  
	  public boolean before(EMonth month){
		  return (this.ordinal() < month.ordinal());
	  }
	  public boolean after(EMonth month){
		  return (this.ordinal() > month.ordinal());
	  }
	  
	  public static EMonth getFromMonthNumber(int monthNumber){
		  return EMonth.values()[monthNumber - 1];
	  }
	  public static EMonth getCurrentMonth(){
		  Calendar today = Calendar.getInstance();
		  
		  if(today.get(Calendar.YEAR) > DateConstant.year){
			  return DECEMBER;
		  }else{
		  	  return EMonth.values()[today.get(Calendar.MONTH)];
		  }
	  }
	  public static EMonth[] getUpToCurrentMonth(){
		  int monthsCount = EMonth.getCurrentMonth().ordinal() + 1;
		  
		  EMonth[] months = new EMonth[monthsCount];
		  
		  for(int i = 0; i < monthsCount; i++)
			  months[i] = EMonth.values()[i];
		  
		  return months;
	  }
	  public static int getNumberOfCompletedMonths(){
		  return EMonth.getCurrentMonth().ordinal() + 1;
	  }
	  
	  @Override
	  /**
	   * @returns stringName
	   */
	  public String toString(){
		  return this.stringName;
	  }
}
	
	



