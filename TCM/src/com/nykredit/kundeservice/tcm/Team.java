package com.nykredit.kundeservice.tcm;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nykredit.kundeservice.data.CTIRConnection;

/**
 * Class used in Department to collect all team and month data
 * @see Department and Month
 * @author thra
 */
public class Team {
	
	public enum TeamTableType{
		Basis,
		Forsikring,
		KoncernOmstilling,
		MailService,
		NetbankKundesupport,
		ErhvervsService,
		Support,
		EkspeditionsService1,
		EkspeditionsService2
	}
	
	private int id;
	
	private String teamCode;
	private String teamName;
	private String departmentName;
	
	private TeamTableType tableType;
	
	public int getId() {
		return this.id;
	}

	public String getTeamCode() {
		return this.teamCode;
	}
	public String getTeamName() {
		return this.teamName;
	}
	public String getDepartmentName(){
		return this.departmentName;
	}
	
	public TeamTableType getTableType(){
		return this.tableType;
	}
	
	public Team(int id, String teamCode, String teamName, String departmentName, TeamTableType tableType){
		this.id = id;
		this.teamCode = teamCode;
		this.teamName = teamName;
		this.departmentName = departmentName;
		this.tableType = tableType;
	}
	
	public static ArrayList<Team> getTeams(CTIRConnection conn) throws SQLException{
		ArrayList<Team> teams = new ArrayList<Team>();
		
		String sqlQuery = "SELECT " +   
						  	"agt.ID, " +
						  	"agt.TEAM, " +
						  	"agt.TEAM_NAVN, " +
						  	"agt.TEAM_GRUPPE, " + 
						  	"tcm.TEAM_TABLE_ID " +
						  "FROM " +     
						  	"KS_DRIFT.AGENTER_TEAMS agt " + 
						  "INNER JOIN " + 
					  		"KS_APPLIKATION.TCM_TABLE_SETTING tcm " + 
					  	  "ON " + 
					  	  	"agt.ID = tcm.TEAM_ID " +
					  	  "ORDER BY " +
					  	  	"tcm.RANK";
		
		ResultSet rs = conn.Hent_tabel(sqlQuery);
		
		while(rs.next())
			try{
				teams.add(new Team(rs.getInt("ID"),
								   rs.getString("TEAM"),
								   rs.getString("TEAM_NAVN"),
								   rs.getString("TEAM_GRUPPE"),
								   TeamTableType.values()[rs.getInt("TEAM_TABLE_ID")]));
			}catch(IndexOutOfBoundsException e){
				
			}
		return teams;
	}
	public static ArrayList<Team> getTeams(CTIRConnection conn, String departmentName) throws SQLException{
		ArrayList<Team> teams = new ArrayList<Team>();
		
		String sqlQuery = "SELECT " +   
						  	"agt.ID, " +
						  	"agt.TEAM, " +
						  	"agt.TEAM_NAVN, " +
						  	"agt.TEAM_GRUPPE, " + 
						  	"tcm.TEAM_TABLE_ID " +
						  "FROM " +     
						  	"KS_DRIFT.AGENTER_TEAMS agt " + 
						  "INNER JOIN " + 
					  		"KS_APPLIKATION.TCM_TABLE_SETTING tcm " + 
					  	  "ON " + 
					  	  	"agt.ID = tcm.TEAM_ID " +
					  	  "WHERE " +
					  	  	"agt.TEAM_GRUPPE = '" + departmentName + "' " +
					  	  "ORDER BY " +
					  	  	"tcm.RANK";
		
		ResultSet rs = conn.Hent_tabel(sqlQuery);
		
		while(rs.next())
			try{
				teams.add(new Team(rs.getInt("ID"),
								   rs.getString("TEAM"),
								   rs.getString("TEAM_NAVN"),
								   rs.getString("TEAM_GRUPPE"),
								   TeamTableType.values()[rs.getInt("TEAM_TABLE_ID")]));
			}catch(IndexOutOfBoundsException e){
				
			}
		
		return teams;
	}
	
	public static ArrayList<Integer> getTeamIds(ArrayList<Team> teams, TeamTableType teamType){
		ArrayList<Integer> teamIds = new ArrayList<Integer>();
		
		for(Team t : teams)
			if(t.getTableType() == teamType)
				teamIds.add(t.getId());
		
		return teamIds;
	}

	public static ArrayList<Team> getTeamsByDepName(ArrayList<Team> teams, String departmentName){
		ArrayList<Team> depTeams = new ArrayList<Team>();
		
		for(Team t : teams)
			if(t.getDepartmentName().equalsIgnoreCase(departmentName))
				depTeams.add(t);
		
		return depTeams;
	}
	public String toString(){
		return teamName;
	}
}