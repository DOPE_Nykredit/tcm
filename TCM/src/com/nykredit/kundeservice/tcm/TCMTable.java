package com.nykredit.kundeservice.tcm;

import java.awt.Cursor;
import java.awt.event.MouseEvent;

import javax.swing.ToolTipManager;
import javax.swing.table.DefaultTableModel;

import com.nykredit.kundeservice.swing.NTable;
import com.nykredit.kundeservice.swing.NTableRenderer;
import com.nykredit.kundeservice.tcm.datatypes.calculations.Calculation;

/**
 *
 *Standard table used for all tables in the program
 * @author thra
 * 
 */
public class TCMTable extends NTable {
	private static final long serialVersionUID = 1L;
	
	public TCMTable(DefaultTableModel tableModel,NTableRenderer renderer){
		this.setModel(tableModel);
		this.setDefaultRenderer(Object.class, renderer);
		setup();
	}
	
	private void setup(){
		this.setShowGrid(false);
		
		ToolTipManager.sharedInstance().setDismissDelay(60000);
		
		if(this.getColumnCount() > 0){	
			this.getColumnModel().getColumn(0).setPreferredWidth(250);
			this.getColumnModel().getColumn(0).setMinWidth(250);
			this.getColumnModel().getColumn(0).setMaxWidth(250);
		
			for(int count = 1;count<this.getColumnModel().getColumnCount();count++){
				this.getColumnModel().getColumn(count).setPreferredWidth(70);
				this.getColumnModel().getColumn(count).setMinWidth(70);
				this.getColumnModel().getColumn(count).setMaxWidth(70);
			}
		}
	}
	
    public String getToolTipText(MouseEvent e) {
		java.awt.Point p = e.getPoint();
		int rowIndex = rowAtPoint(p);
		int colIndex = columnAtPoint(p);
		Object value = this.getValueAt(rowIndex, colIndex);
		String header = this.getColumnModel().getColumn(this.getTableHeader().columnAtPoint(e.getPoint())).getHeaderValue().toString();
		String rowName = this.getValueAt(rowIndex, 0).toString();
		String tooltip = null;
    	if(System.getenv("username").equalsIgnoreCase("JSKR") || 
    	   System.getenv("username").equalsIgnoreCase("HENO") || 
    	   System.getenv("username").equalsIgnoreCase("CRPE") ||
    	   System.getenv("username").equalsIgnoreCase("MRCR") ||
    	   System.getenv("username").equalsIgnoreCase("RUNE")){   
    		if (colIndex > 1 && this.getTableHeader().getCursor().getType() != Cursor.E_RESIZE_CURSOR && header != "Estimat**" && header != "Krav***"){
        	
    			tooltip = "<html><b>" + rowName + ":</b><br><br>";
    			if(value instanceof Calculation) {
    				Calculation calc = (Calculation)value;
    				String[] tooltips = calc.getToolTipInformation();
    				if(calc.getToolTipInformation() != null){
    					for(String t : tooltips){

    						tooltip += t + "<br>";
    					}
    				tooltip += "</html>";
    				}else{
						tooltip +="0";
    				}
    			}
    		}
    	}
        return tooltip;
    }


	@Override
	public boolean isCellEditable(int rowIndex, int colIndex) {
		return false;
	}
}
