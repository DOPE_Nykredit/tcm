package com.nykredit.kundeservice.tcm.tables;

import javax.swing.table.DefaultTableModel;

import com.nykredit.kundeservice.tcm.DateConstant;
import com.nykredit.kundeservice.tcm.EMonth;
import com.nykredit.kundeservice.tcm.Formatter;
import com.nykredit.kundeservice.tcm.Goal;
import com.nykredit.kundeservice.tcm.ServiceCenterData;
import com.nykredit.kundeservice.tcm.Team;
import com.nykredit.kundeservice.tcm.datatypes.PhoneQueueData.Queue;
import com.nykredit.kundeservice.tcm.datatypes.calculations.HaandteringsGrad;
import com.nykredit.kundeservice.tcm.datatypes.calculations.Tilgaengelighed;

public class KoncernOmstillingTableModel extends DefaultTableModel {

	private static final long serialVersionUID = 1L;
	
	public KoncernOmstillingTableModel(ServiceCenterData centerData, Team team){
		this.addColumn(team.getTeamName(), new Object[]{"Tilgængelighed",
														"Kunder og stabe besvaret indenfor 25 sek."});
		
		this.addColumn("Mål", new Object[]{Formatter.toPercentString(Goal.KOtilgængelighed),
										   Formatter.toPercentString(Goal.KOKunderOgStageBesvaretIndenfor25Sek)});
		
		for(EMonth m : EMonth.getUpToCurrentMonth())
			this.addColumn(m.toString(), new Object[]{centerData.getTilgængelighed(m, team), 
						   							  centerData.getTlfKoeSvarPct(m, Queue.KunderOgStabe)});
		
		for(int i = 1; i <= EMonth.getCurrentMonth().getQuarter(); i++)
			this.addColumn(i + ". kv.", new Object[]{centerData.getTilgængelighed(i, team),
						   							 centerData.getTlfKoeSvarPct(i, Queue.KunderOgStabe)});
		if(DateConstant.completedMonths > 0 || DateConstant.completedMonths == 0){
			Tilgaengelighed tilgængelighed = centerData.getTilgængelighed(team);
			HaandteringsGrad svarProcent = centerData.getTlfKoeSvarPct(Queue.KunderOgStabe); 
	
			this.addColumn("ÅTD*", new Object[]{tilgængelighed, 
					   	   						svarProcent});
			if(DateConstant.completedMonths != 0){

			this.addColumn("Estimat**", new Object[]{tilgængelighed.getEstimat(Goal.KOtilgængelighed),
						   							 svarProcent.getEstimat(Goal.KOKunderOgStageBesvaretIndenfor25Sek)});
		
			this.addColumn("Krav***", new Object[]{tilgængelighed.getKrav(Goal.KOtilgængelighed),
					   	   						   svarProcent.getKrav(Goal.KOKunderOgStageBesvaretIndenfor25Sek)});
			}
		}
	}
}