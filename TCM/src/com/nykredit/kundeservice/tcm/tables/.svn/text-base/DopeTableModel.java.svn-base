package com.nykredit.kundeservice.tcm.tables;

import javax.swing.table.DefaultTableModel;

import com.nykredit.kundeservice.tcm.DateConstant;
import com.nykredit.kundeservice.tcm.EMonth;
import com.nykredit.kundeservice.tcm.Formatter;
import com.nykredit.kundeservice.tcm.Goal;
import com.nykredit.kundeservice.tcm.ServiceCenterData;
import com.nykredit.kundeservice.tcm.datatypes.calculations.HaandteringsGrad;

public class DopeTableModel extends DefaultTableModel {

	private static final long serialVersionUID = 1L;
	
	public DopeTableModel(ServiceCenterData centerData){
		this.addColumn("Drift og Performance", new Object[]{"Kald besvaret indenfor 25 sek",
				  											"Kundeaktiviteter indenfor tidsfrist",
				  											"WFCM - prognose kontra faktisk kald",
				  											"Videnbarometer",
				  											"AHT",
				  											"SMART"});
		
		this.addColumn("M�l", new Object[]{Formatter.toPercentString(Goal.DPKundekaldBesvaretIndenfor25Sek),
										   Formatter.toPercentString(Goal.DPKundeAktiviteterIndenforTidsfrist),
										   Formatter.toPercentString(Goal.DPWFCMPrognose),
										   Formatter.toPercentString(Goal.DPVidenBarometer),
										   Formatter.toPercentString(Goal.DPAhtReduktion),
										   Formatter.toPercentString(Goal.DPSmart)});
		
		for(EMonth m : EMonth.getUpToCurrentMonth())
			this.addColumn(m.toString(), new Object[]{centerData.getTlfKoeSvarPctCenter(m), 
						   							  centerData.getKundeAktiviteterIndenforTidsfristCenter(m),
						   							  centerData.getDpWFCMPrognose(m),
						   							  centerData.getDpVidenbarometer(m),
						   							  centerData.getDpAHTReduktion(m),
						   							  centerData.getDpSmart(m)});
		
		for(int i = 1; i < EMonth.getCurrentMonth().getQuarter(); i++)
			this.addColumn(i + ". kv.", new Object[]{centerData.getTlfKoeSvarPctCenter(i),
						   							 centerData.getKundeAktiviteterIndenforTidsfristCenter(i), 
						   							 centerData.getDpWFCMPrognose(i),
						   							 centerData.getDpVidenbarometer(i),
						   							 centerData.getDpAHTReduktion(i),
						   							 centerData.getDpSmart(i)});
		
		HaandteringsGrad kaldBesvaretIndenfor25Sek = centerData.getTlfKoeSvarPctCenter();
		HaandteringsGrad kundeaktiviteter = centerData.getKundeAktiviteterIndenforTidsfristCenter();
		HaandteringsGrad wfcmPrognose = centerData.getDpWFCMPrognose();
		HaandteringsGrad videnbarometer = centerData.getDpVidenbarometer();
		HaandteringsGrad ahtReduktion = centerData.getDpAHTReduktion();
		HaandteringsGrad smart = centerData.getDpSmart();
		
		if(DateConstant.completedMonths > 0){
			this.addColumn("�TD*", new Object[]{kaldBesvaretIndenfor25Sek, 
					   	   						kundeaktiviteter, 
					   	   						wfcmPrognose,
					   	   						videnbarometer, 
					   	   						ahtReduktion, 
					   	   						smart});
		
			this.addColumn("Estimat**", new Object[]{kaldBesvaretIndenfor25Sek.getEstimat(Goal.DPKundekaldBesvaretIndenfor25Sek), 
						   							 kundeaktiviteter.getEstimat(Goal.DPKundeAktiviteterIndenforTidsfrist),
						   							 wfcmPrognose.getEstimat(Goal.DPWFCMPrognose),
						   							 videnbarometer.getEstimat(Goal.DPVidenBarometer), 
						   							 ahtReduktion.getEstimat(Goal.DPAhtReduktion), 
						   							 smart.getEstimat(Goal.DPSmart)});
		
			this.addColumn("Krav***", new Object[]{kaldBesvaretIndenfor25Sek.getKrav(Goal.DPKundekaldBesvaretIndenfor25Sek), 
						   						   kundeaktiviteter.getKrav(Goal.DPKundeAktiviteterIndenforTidsfrist),
						   						   wfcmPrognose.getKrav(Goal.DPWFCMPrognose),
						   						   videnbarometer.getKrav(Goal.DPVidenBarometer), 
						   						   ahtReduktion.getKrav(Goal.DPAhtReduktion), 
						   						   smart.getKrav(Goal.DPSmart)});
		}
	}
}