package com.nykredit.kundeservice.tcm.tables;

import javax.swing.table.DefaultTableModel;

import com.nykredit.kundeservice.tcm.DateConstant;
import com.nykredit.kundeservice.tcm.EMonth;
import com.nykredit.kundeservice.tcm.ServiceCenterData;

public class RealkreditadministrationTableModel extends DefaultTableModel{

	private static final long serialVersionUID = 1L;

	public RealkreditadministrationTableModel(ServiceCenterData centerData){
		this.addColumn("Realkreditadministration", new Object[]{"Service",
															   "Kvalitet",
															   "Tilg�ngelighed",
															   "Ledelsesm�l - medarbejdertrivsel"});

		this.addColumn("M�l", new Object[]{"95%",
										   "-",
										   "80/25",
										   ">75"});
		
		for(EMonth m : EMonth.getUpToCurrentMonth())
			this.addColumn(m.toString());
		
		for(int i = 1; i <= EMonth.getCurrentMonth().getQuarter(); i++)
			this.addColumn(i + ". kv.");
		
		if(DateConstant.completedMonths > 0 || DateConstant.completedMonths == 0){
			this.addColumn("�TD*");
			if(DateConstant.completedMonths != 0){

			this.addColumn("Estimat**");
			
			this.addColumn("Krav***");
		}}
	}
}