package com.nykredit.kundeservice.tcm.tables;

import javax.swing.table.DefaultTableModel;

import com.nykredit.kundeservice.tcm.DateConstant;
import com.nykredit.kundeservice.tcm.EMonth;
import com.nykredit.kundeservice.tcm.Formatter;
import com.nykredit.kundeservice.tcm.Goal;
import com.nykredit.kundeservice.tcm.ServiceCenterData;
import com.nykredit.kundeservice.tcm.Team;
import com.nykredit.kundeservice.tcm.datatypes.calculations.HaandteringsGrad;
import com.nykredit.kundeservice.tcm.datatypes.calculations.KPI;

public class SupportTableModel extends DefaultTableModel {

	private static final long serialVersionUID = 1L;

	public SupportTableModel(ServiceCenterData centerData, Team team){

		this.addColumn(team.getTeamName(), new Object[]{"Opgaver i support",
			"Kontakt før opsigelser",
		"Produktivitet"});

		this.addColumn("Mål", new Object[]{Formatter.toPercentString(Goal.SUOpgaverIndenforTidsfrist),
				Formatter.toPercentString(Goal.SUKontaktFørOpsigelser),
				Formatter.toNumberString(Goal.SUProduktivitet)});

		for(EMonth m : EMonth.getUpToCurrentMonth())
			this.addColumn(m.toString(), new Object[]{centerData.getAktiviteterIndenforTidsfrist(m, team, true),
				centerData.getKontaktFørOpsigelser(m, team.getId()),
				centerData.getProduktivitet(m, team, false, true)});

		for(int i = 1; i <= EMonth.getCurrentMonth().getQuarter(); i++)
			this.addColumn(i + ". kv.", new Object[]{centerData.getAktiviteterIndenforTidsfrist(i, team, true),
					centerData.getKontaktFørOpsigelser(i, team.getId()),
					centerData.getProduktivitet(i, team, false, true)});

		if(DateConstant.completedMonths > 0 || DateConstant.completedMonths == 0){
			HaandteringsGrad opgaverIndenforTidsfrist = centerData.getAktiviteterIndenforTidsfrist(team, true);
			HaandteringsGrad kontaktFørOpsigelser = centerData.getKontaktFørOpsigelser(team.getId());
			KPI produktivitet = centerData.getProduktivitet(team, false, true);


			this.addColumn("ÅTD*", new Object[]{opgaverIndenforTidsfrist,
					kontaktFørOpsigelser,
					produktivitet});
			if(DateConstant.completedMonths != 0){

				this.addColumn("Estimat**", new Object[]{opgaverIndenforTidsfrist.getEstimat(Goal.SUOpgaverIndenforTidsfrist),
						kontaktFørOpsigelser.getEstimat(Goal.SUKontaktFørOpsigelser),
						produktivitet.getEstimat(Goal.SUProduktivitet)});

				this.addColumn("Krav***", new Object[]{opgaverIndenforTidsfrist.getKrav(Goal.SUOpgaverIndenforTidsfrist),
						kontaktFørOpsigelser.getKrav(Goal.SUKontaktFørOpsigelser),
						produktivitet.getKrav(Goal.SUProduktivitet)});
			}}
	}
}
