package com.nykredit.kundeservice.tcm.tables;

import java.util.ArrayList;

import javax.swing.table.DefaultTableModel;

import com.nykredit.kundeservice.tcm.DateConstant;
import com.nykredit.kundeservice.tcm.EMonth;
import com.nykredit.kundeservice.tcm.Formatter;
import com.nykredit.kundeservice.tcm.Goal;
import com.nykredit.kundeservice.tcm.ServiceCenterData;
import com.nykredit.kundeservice.tcm.datatypes.EmailData.EmailGroups;
import com.nykredit.kundeservice.tcm.datatypes.PhoneQueueData;
import com.nykredit.kundeservice.tcm.datatypes.calculations.HaandteringsGrad;
import com.nykredit.kundeservice.tcm.datatypes.calculations.KPI;
import com.nykredit.kundeservice.tcm.datatypes.calculations.Number;

public class DigitaleServicesDepartmentTableModel extends DefaultTableModel {

	private static final long serialVersionUID = 1L;

	private String tableName = "Afdeling Digitale Services";
	
	public DigitaleServicesDepartmentTableModel(ServiceCenterData centerData,
												ArrayList<Integer> hotlineTeamIds,
												ArrayList<Integer> erhvervTeamIds,
												ArrayList<Integer> mailServiceTeamIds,
												ArrayList<Integer> supportTeamIds){
		
		this.addColumn(this.tableName, new Object[]{"Erhvervskald besvaret indefor 25 sek.",
													"Svarprocent på Webdesk (30 sek.)",
													"Servicemål på mails",
													"Dialogaktiviteter indenfor tidsfrist",
													"Opgaver i support",
													"Produktivitet mailservice",
													"Produktivitet support",
													"Kontakt før opsigelser",
													"Kundetilfredshed"});
		
		this.addColumn("Mål", new Object[]{Formatter.toPercentString(Goal.DSKundekaldBesvaretIndenfor25Sek),
										   Formatter.toPercentString(Goal.DSWebdeskSvarprocent),
										   Formatter.toPercentString(Goal.DSServicemålMails),
										   Formatter.toPercentString(Goal.DSDialogAktiviteterIndenforTidsfrist),
										   Formatter.toPercentString(Goal.DSOpgaverISupport),
										   Formatter.toNumberString(Goal.DSProduktivitetMailservice),
										   Formatter.toNumberString(Goal.DSProduktivitetSupport),
										   Formatter.toPercentString(Goal.DSKontaktFørOpsigelser),
										   Formatter.toPercentString(Goal.DSKundetilfredshed)});
		
		ArrayList<Integer> allTeamIds = new ArrayList<Integer>();
		allTeamIds.addAll(hotlineTeamIds);
		allTeamIds.addAll(erhvervTeamIds);
		allTeamIds.addAll(mailServiceTeamIds);
		allTeamIds.addAll(supportTeamIds);		
		
		ArrayList<Integer> ErhHotTeamIds = new ArrayList<Integer>();
		ErhHotTeamIds.addAll(hotlineTeamIds);
		ErhHotTeamIds.addAll(erhvervTeamIds);
		
		for(EMonth m : EMonth.getUpToCurrentMonth())
			this.addColumn(m.toString(), new Object[]{centerData.getTlfKoeSvarPct(m, PhoneQueueData.Queue.Erhverv),
						   							  centerData.getWebdeskSvarProcent(m),
						   							  centerData.getEmailSvarProcent(m, EmailGroups.Mailservice),
						   							  centerData.getAktiviteterIndenforTidsfrist(m, ErhHotTeamIds, false),
						   							  centerData.getAktiviteterIndenforTidsfrist(m, supportTeamIds, true),
						   							  centerData.getProduktivitet(m, mailServiceTeamIds, true, false),
						   							  centerData.getProduktivitet(m, supportTeamIds, false, true),
						   							  centerData.getKontaktFørOpsigelser(m, supportTeamIds.get(0)),
						   							  centerData.getKundetilfredshed(m, ErhHotTeamIds, true, true)});
		
		for(int i = 1; i <= EMonth.getCurrentMonth().getQuarter(); i++)
			this.addColumn(i + ". kv.", new Object[]{centerData.getTlfKoeSvarPct(i, PhoneQueueData.Queue.Erhverv),
					   	   							 centerData.getWebdeskSvarProcent(i),
					   	   							 centerData.getEmailSvarProcent(i,EmailGroups.Mailservice),
					   	   							 centerData.getAktiviteterIndenforTidsfrist(i, ErhHotTeamIds, false),
					   	   							 centerData.getAktiviteterIndenforTidsfrist(i, supportTeamIds, true),
					   	   							 centerData.getProduktivitet(i, mailServiceTeamIds, true, false),
					   	   							 centerData.getProduktivitet(i, supportTeamIds, false, true),
					   	   							 centerData.getKontaktFørOpsigelser(i, supportTeamIds.get(0)),
					   	   							 centerData.getKundetilfredshed(i, ErhHotTeamIds, true, true)});
		
		if(DateConstant.completedMonths > 0 || DateConstant.completedMonths == 0){
			HaandteringsGrad kundekaldBesvaretIndenfor25Sek = centerData.getTlfKoeSvarPct(PhoneQueueData.Queue.Erhverv);
			HaandteringsGrad webdeskSvarProcent = centerData.getWebdeskSvarProcent();
			HaandteringsGrad servicemålMail = centerData.getEmailSvarProcent(EmailGroups.Mailservice);
			HaandteringsGrad dialogAktiviteterIndenforTidsfrist = centerData.getAktiviteterIndenforTidsfrist(ErhHotTeamIds, false);
			HaandteringsGrad opgaverISupport = centerData.getAktiviteterIndenforTidsfrist(supportTeamIds, true);
			KPI produktivitetMailservice = centerData.getProduktivitet(mailServiceTeamIds, true, false);
			KPI produktivitetSupport = centerData.getProduktivitet(supportTeamIds, false, true);
			HaandteringsGrad kontaktFørOpsigelser = centerData.getKontaktFørOpsigelser(supportTeamIds.get(0));
			HaandteringsGrad kundetilfredshed = centerData.getKundetilfredshed(ErhHotTeamIds, true, true);		
		
			this.addColumn("ÅTD*", new Object[]{kundekaldBesvaretIndenfor25Sek,
						   						webdeskSvarProcent,
						   						servicemålMail,
						   						dialogAktiviteterIndenforTidsfrist,
						   						opgaverISupport,
						   						produktivitetMailservice,
						   						produktivitetSupport,
						   						kontaktFørOpsigelser,
						   						kundetilfredshed});
			if(DateConstant.completedMonths != 0){

			this.addColumn("Estimat**", new Object[]{kundekaldBesvaretIndenfor25Sek.getEstimat(Goal.DSKundekaldBesvaretIndenfor25Sek),
						   							 webdeskSvarProcent.getEstimat(Goal.DSWebdeskSvarprocent),
						   							 servicemålMail.getEstimat(Goal.DSServicemålMails),
						   							 dialogAktiviteterIndenforTidsfrist.getEstimat(Goal.DSDialogAktiviteterIndenforTidsfrist),
						   							 opgaverISupport.getEstimat(Goal.DSOpgaverISupport),
						   							 produktivitetMailservice.getEstimat(Goal.DSProduktivitetMailservice),
						   							 produktivitetSupport.getEstimat(Goal.DSProduktivitetSupport),
						   							 kontaktFørOpsigelser.getEstimat(Goal.DSKontaktFørOpsigelser),
						   							 kundetilfredshed.getEstimat(Goal.DSKundetilfredshed)});
		
			this.addColumn("Krav***", new Object[]{kundekaldBesvaretIndenfor25Sek.getKrav(Goal.DSKundekaldBesvaretIndenfor25Sek),
						   						   webdeskSvarProcent.getKrav(Goal.DSWebdeskSvarprocent),
						   						   servicemålMail.getKrav(Goal.DSServicemålMails),
						   						   dialogAktiviteterIndenforTidsfrist.getKrav(Goal.DSDialogAktiviteterIndenforTidsfrist),
						   						   opgaverISupport.getKrav(Goal.DSOpgaverISupport),
						   						   produktivitetMailservice.getKrav(Goal.DSProduktivitetMailservice),
						   						   produktivitetSupport.getKrav(Goal.DSProduktivitetSupport),
						   						   kontaktFørOpsigelser.getKrav(Goal.DSKontaktFørOpsigelser),
						   						   kundetilfredshed.getKrav(Goal.DSKundetilfredshed)});
			}
		}
	}
}