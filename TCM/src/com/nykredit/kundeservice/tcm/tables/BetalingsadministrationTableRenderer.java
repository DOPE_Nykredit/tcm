package com.nykredit.kundeservice.tcm.tables;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.SwingConstants;

import com.nykredit.kundeservice.swing.NTableRenderer;
import com.nykredit.kundeservice.tcm.DateConstant;
import com.nykredit.kundeservice.tcm.EMonth;
import com.nykredit.kundeservice.tcm.Goal;
import com.nykredit.kundeservice.tcm.RenderFunction;
import com.nykredit.kundeservice.tcm.datatypes.calculations.HaandteringsGrad;

public class BetalingsadministrationTableRenderer extends NTableRenderer {

	private static final long serialVersionUID = 1L;

    @Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column){    
    	Component cell = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    	String header = table.getColumnModel().getColumn(column).getHeaderValue().toString();

		if (column > 0){
			this.setHorizontalAlignment(SwingConstants.CENTER);
		} else {
			this.setHorizontalAlignment(SwingConstants.LEFT);
		}
		
//		if(table.getColumnModel().getColumn(column).getHeaderValue() == "Estimat**" && row == 1){
//			double firstValue = ((HåndteringsGrad)table.getValueAt(row, 2)).getResult();
//			double totalValueAfterJanuary = 0;
//			double averageValueAfterJanuary = 0;
//			double increasePerMonth = 0;
//			double estimate = 
//			
//			for(int i = 3; i < EMonth.getCurrentMonth().ordinal() + 4; i++)
//				totalValueAfterJanuary = ((HåndteringsGrad)table.getValueAt(row, i)).getResult();
//			
//			averageValueAfterJanuary = totalValueAfterJanuary / EMonth.getCurrentMonth().ordinal();
//			increasePerMonth = firstValue - averageValueAfterJanuary;
//		} 
			
		
//		if(column - 1 != DateConstant.getCurrentMonth() & column > 1){
//			switch(row){
//				case 1:
//					RenderFunction.setColoringPct(cell, header, value, Goal.BaDigitalisering);
//					break;
//			}				
//		}
//		
//		addMarking(cell,isSelected,new Color(0,0,128),80);
		
    	return cell;
    }
	
}
