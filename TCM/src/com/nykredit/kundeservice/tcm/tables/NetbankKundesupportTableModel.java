package com.nykredit.kundeservice.tcm.tables;

import javax.swing.table.DefaultTableModel;

import com.nykredit.kundeservice.tcm.DateConstant;
import com.nykredit.kundeservice.tcm.EMonth;
import com.nykredit.kundeservice.tcm.Formatter;
import com.nykredit.kundeservice.tcm.Goal;
import com.nykredit.kundeservice.tcm.ServiceCenterData;
import com.nykredit.kundeservice.tcm.Team;
import com.nykredit.kundeservice.tcm.datatypes.calculations.HaandteringsGrad;
import com.nykredit.kundeservice.tcm.datatypes.calculations.Tilgaengelighed;

public class NetbankKundesupportTableModel extends DefaultTableModel {
	
	private static final long serialVersionUID = 1L;
		
	public NetbankKundesupportTableModel(ServiceCenterData centerData, Team team){
		this.addColumn(team.getTeamName(), new Object[]{"Telefonisk Kundetilfredshed",
			 											"Tilgængelighed",
			 											"Aktiviteter indenfor tidsfrist"});
		
		this.addColumn("Mål", new Object[]{Formatter.toPercentString(Goal.NKTelefoniskKundetilfredshed),
										   Formatter.toPercentString(Goal.NKTilgængelighed),
										   Formatter.toPercentString(Goal.NKAktiviteterIndenforTidsfrist)});
		
		for(EMonth m : EMonth.getUpToCurrentMonth())
			this.addColumn(m.toString(), new Object[]{centerData.getKundetilfredshed(m, team, true, false),
						   							  centerData.getTilgængelighed(m, team), 
						   							  centerData.getAktiviteterIndenforTidsfrist(m, team, false)});
		
		for(int i = 1; i <= EMonth.getCurrentMonth().getQuarter(); i++)
		
			this.addColumn(i + ". kv.", new Object[]{centerData.getKundetilfredshed(i, team, true, false),
						   							 centerData.getTilgængelighed(i, team), 
						   							 centerData.getAktiviteterIndenforTidsfrist(i, team, false)});
	
		if(DateConstant.completedMonths > 0 || DateConstant.completedMonths == 0){
			HaandteringsGrad kundetilfredshed = centerData.getKundetilfredshed(team, true, false);
			Tilgaengelighed tilgængelighed = centerData.getTilgængelighed(team);
			HaandteringsGrad aktiviteterIndenforTidsfrist = centerData.getAktiviteterIndenforTidsfrist(team, false);		
	
			this.addColumn("ÅTD*", new Object[]{kundetilfredshed, 
					   	   						tilgængelighed, 
					   	   						aktiviteterIndenforTidsfrist});
			if(DateConstant.completedMonths != 0){

			this.addColumn("Estimat**", new Object[]{kundetilfredshed.getEstimat(Goal.NKTelefoniskKundetilfredshed),
						   							 tilgængelighed.getEstimat(Goal.NKTilgængelighed),
						   							 aktiviteterIndenforTidsfrist.getEstimat(Goal.NKAktiviteterIndenforTidsfrist)});
		
			this.addColumn("Krav***", new Object[]{kundetilfredshed.getKrav(Goal.NKTelefoniskKundetilfredshed),
					   	   						   tilgængelighed.getKrav(Goal.NKTilgængelighed),
					   	   						   aktiviteterIndenforTidsfrist.getKrav(Goal.NKAktiviteterIndenforTidsfrist)});
			}
		}
	}
}
