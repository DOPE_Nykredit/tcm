package com.nykredit.kundeservice.tcm.tables;

import javax.swing.table.DefaultTableModel;

import com.nykredit.kundeservice.tcm.Formatter;
import com.nykredit.kundeservice.tcm.Goal;
import com.nykredit.kundeservice.tcm.ServiceCenterData;

public class EkspeditionTraeningsTableModel extends DefaultTableModel {

	private static final long serialVersionUID = 1L;

	public EkspeditionTraeningsTableModel(ServiceCenterData centerData){
		this.addColumn("Tr�ningsm�l for Ekspeditionsservice", new Object[]{"Mersalg til basisservice",
																		   "Fagligt minimumsniveau"});
		
		this.addColumn("M�l", new Object[]{Formatter.toNumberString(Goal.ETMerSalgTilBasisService),
										   Formatter.toPercentString(Goal.ETFagligtMinimumsNiveau)});
	}
}
