package com.nykredit.kundeservice.tcm.tables;

import javax.swing.table.DefaultTableModel;

import com.nykredit.kundeservice.tcm.DateConstant;
import com.nykredit.kundeservice.tcm.EMonth;
import com.nykredit.kundeservice.tcm.Formatter;
import com.nykredit.kundeservice.tcm.Goal;
import com.nykredit.kundeservice.tcm.ServiceCenterData;
import com.nykredit.kundeservice.tcm.Team;
import com.nykredit.kundeservice.tcm.datatypes.calculations.AHT;
import com.nykredit.kundeservice.tcm.datatypes.calculations.Number;
import com.nykredit.kundeservice.tcm.datatypes.calculations.Tilgaengelighed;

public class Ekspedition1TableModel extends DefaultTableModel {

	private static final long serialVersionUID = 1L;
		
	public Ekspedition1TableModel(ServiceCenterData centerData, Team team){
		
		this.addColumn(team.getTeamName(), new Object[]{"Tilgængelighed",
														"Gennemførte Salg",
														"Produktivitet"});
		
		this.addColumn("Mål", new Object[]{Formatter.toPercentString(Goal.E1Tilgængelighed),
										   Formatter.toNumberString(Goal.E1AntalGennemførteSalg),
										   Formatter.toTimeString(Goal.E1Produktivitet)});
		
		for(EMonth m : EMonth.getUpToCurrentMonth())
			this.addColumn(m.toString(), new Object[]{centerData.getTilgængelighed(m, team), 
						   							  centerData.getGennemførteSalg(m, team), 
						   							  centerData.getAHT(m, team)});
			
		for(int i = 1; i <= EMonth.getCurrentMonth().getQuarter(); i++)
			this.addColumn(i + ". kv.", new Object[]{centerData.getTilgængelighed(i, team), 
						   							 centerData.getGennemførteSalg(i, team), 
						   							 centerData.getAHT(i, team)});
		
		if(DateConstant.completedMonths > 0 || DateConstant.completedMonths == 0){
			Tilgaengelighed tilgængelighed = centerData.getTilgængelighed(team);
			Number gennemførteSalg = centerData.getGennemførteSalg(team);
			AHT produktivitet = centerData.getAHT(team);
		
			this.addColumn("ÅTD*", new Object[]{tilgængelighed, 
					   	   						gennemførteSalg, 
					   	   						produktivitet});
			if(DateConstant.completedMonths != 0){

			this.addColumn("Estimat**", new Object[]{tilgængelighed.getEstimat(Goal.E1Tilgængelighed), 
						   							 gennemførteSalg.getEstimate(Goal.E1AntalGennemførteSalg), 
						   							 produktivitet.getEstimat(Goal.E1Produktivitet)});
		
			this.addColumn("Krav***", new Object[]{tilgængelighed.getKrav(Goal.E1Tilgængelighed), 
						   						   gennemførteSalg.getKrav(Goal.E1AntalGennemførteSalg), 
						   						   produktivitet.getKrav(Goal.E1Produktivitet)});
			}
		}
	}
}