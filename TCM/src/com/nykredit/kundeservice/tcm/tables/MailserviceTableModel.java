package com.nykredit.kundeservice.tcm.tables;

import javax.swing.table.DefaultTableModel;

import com.nykredit.kundeservice.tcm.DateConstant;
import com.nykredit.kundeservice.tcm.EMonth;
import com.nykredit.kundeservice.tcm.Formatter;
import com.nykredit.kundeservice.tcm.Goal;
import com.nykredit.kundeservice.tcm.ServiceCenterData;
import com.nykredit.kundeservice.tcm.Team;
import com.nykredit.kundeservice.tcm.datatypes.EmailData.EmailGroups;
import com.nykredit.kundeservice.tcm.datatypes.calculations.EmailSvarprocent;
import com.nykredit.kundeservice.tcm.datatypes.calculations.KPI;
import com.nykredit.kundeservice.tcm.datatypes.calculations.Kundetilfredshed;
import com.nykredit.kundeservice.tcm.datatypes.calculations.Number;
import com.nykredit.kundeservice.tcm.datatypes.calculations.WebdeskSvarProcent;

public class MailserviceTableModel extends DefaultTableModel {

	private static final long serialVersionUID = 1L;
		
	public MailserviceTableModel(ServiceCenterData centerData, Team team){
		this.addColumn(team.getTeamName(), new Object[]{"Servicem�l p� mails",
		  	  											"Svarprocent p� webdesk (30 sek.)",
		  	  											"Produktivitet",
			  											"Kundetilfredshed p� mail"});
		
		this.addColumn("M�l", new Object[]{Formatter.toPercentString(Goal.MSServicem�lP�Mails),
				   						   Formatter.toPercentString(Goal.MSWebdeskSvarprocent),
				   						   Goal.MSProduktivitet,
				   						   Formatter.toPercentString(Goal.MSMailKundetilfredshed)});
		
		for(EMonth m : EMonth.getUpToCurrentMonth()){
			this.addColumn(m.toString(), new Object[]{centerData.getEmailSvarProcent(m, EmailGroups.Mailservice),
						   							  centerData.getWebdeskSvarProcent(m),
						   							  centerData.getProduktivitet(m, team, true, false),
						   							  centerData.getKundetilfredshed(m, team, false, true)});
		}
		
		for(int i = 1; i <= EMonth.getCurrentMonth().getQuarter(); i++){
			this.addColumn(i + ". kv.", new Object[]{centerData.getEmailSvarProcent(i, EmailGroups.Mailservice),
						   							 centerData.getWebdeskSvarProcent(i),
						   							 centerData.getProduktivitet(i, team, true, false),
						   							 centerData.getKundetilfredshed(i, team, false, true)});
		}
		
		if(DateConstant.completedMonths > 0 || DateConstant.completedMonths == 0){
			EmailSvarprocent servicem�l = centerData.getEmailSvarProcent(EmailGroups.Mailservice);
			WebdeskSvarProcent webdeskSvarProcent = centerData.getWebdeskSvarProcent();
			KPI produktivetet = centerData.getProduktivitet(team, true, false);
			Kundetilfredshed kundetilfredshed = centerData.getKundetilfredshed(team, false, true); 
		
			this.addColumn("�TD*", new Object[]{servicem�l, 
					   	   						webdeskSvarProcent, 
					   	   						produktivetet, 
					   	   						kundetilfredshed});
			if(DateConstant.completedMonths != 0){

			this.addColumn("Estimat**", new Object[]{servicem�l.getEstimat(Goal.MSServicem�lP�Mails),
						   							 webdeskSvarProcent.getEstimat(Goal.MSWebdeskSvarprocent),
						   							 produktivetet.getEstimat(Goal.MSProduktivitet),
													 kundetilfredshed.getEstimat(Goal.MSMailKundetilfredshed)});
		
			this.addColumn("Krav***", new Object[]{servicem�l.getKrav(Goal.MSServicem�lP�Mails),
					   	   						   webdeskSvarProcent.getKrav(Goal.MSWebdeskSvarprocent),
					   	   						   produktivetet.getKrav(Goal.MSProduktivitet),
					   	   						   kundetilfredshed.getKrav(Goal.MSMailKundetilfredshed)});
			}
		}
	}
}
