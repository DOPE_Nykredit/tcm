package com.nykredit.kundeservice.tcm.tables;

import javax.swing.table.DefaultTableModel;

import com.nykredit.kundeservice.tcm.DateConstant;
import com.nykredit.kundeservice.tcm.EMonth;
import com.nykredit.kundeservice.tcm.Formatter;
import com.nykredit.kundeservice.tcm.Goal;
import com.nykredit.kundeservice.tcm.ServiceCenterData;
import com.nykredit.kundeservice.tcm.datatypes.calculations.HaandteringsGrad;
import com.nykredit.kundeservice.tcm.datatypes.calculations.Number;

public class ProjektTableModel extends DefaultTableModel {

	private static final long serialVersionUID = 1L;
	
	public ProjektTableModel(ServiceCenterData centerData){

		this.addColumn("Projekt", new Object[]{"Projektleverancer",
											   "Spørgsmål omkring telefoniforvaltning",
											   "Medarbejdermøder",
											   "Planproces"});
		
		this.addColumn("Mål", new Object[]{Formatter.toPercentString(Goal.ProjektLeverancer),
										   Formatter.toPercentString(Goal.ProjektTelefoniForvaltning),
										   Formatter.toNumberString(Goal.ProjektMedarbejdermøder),
										   Formatter.toBooleanString(Goal.ProjektPlanproces)});
		
		for(EMonth m : EMonth.getUpToCurrentMonth())
			this.addColumn(m.toString(), new Object[]{centerData.getProjektLeverancer(m),
						   							  centerData.getProjektTelefoniforvaltning(m),
						   							  centerData.getProjektMedarbejderMøder(m),
						   							  centerData.getProjektPlanproces(m)});

		for(int i = 1; i <= EMonth.getCurrentMonth().getQuarter(); i++)
			this.addColumn(i + ". kv.", new Object[]{centerData.getProjektLeverancer(i),
						   							 centerData.getProjektTelefoniforvaltning(i),
						   							 centerData.getProjektMedarbejderMøder(i),
						   							 centerData.getProjektPlanproces(i)});
	
		if(DateConstant.completedMonths > 0 || DateConstant.completedMonths == 0){
			HaandteringsGrad projektLeverancer = centerData.getProjektLeverancer();
			HaandteringsGrad telefoniforvaltning = centerData.getProjektTelefoniforvaltning();
			Number medarbejdermøder = centerData.getProjektMedarbejderMøder();
			Boolean planproces = centerData.getProjektPlanproces();
	
			this.addColumn("ÅTD*", new Object[]{projektLeverancer,
					       						telefoniforvaltning,
					       						medarbejdermøder,
					       						planproces});
			if(DateConstant.completedMonths != 0){

			this.addColumn("Estimat**", new Object[]{projektLeverancer.getEstimat(Goal.ProjektLeverancer),
						   							 telefoniforvaltning.getEstimat(Goal.ProjektTelefoniForvaltning),
						   							 medarbejdermøder.getEstimate(Goal.ProjektMedarbejdermøder),
						   							 planproces});
			
			this.addColumn("Krav***", new Object[]{projektLeverancer.getKrav(Goal.ProjektLeverancer),
						   						   telefoniforvaltning.getKrav(Goal.ProjektTelefoniForvaltning),
						   						   medarbejdermøder.getKrav(Goal.ProjektMedarbejdermøder),
						   						   planproces});
			}
		}
	}
}