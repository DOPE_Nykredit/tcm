package com.nykredit.kundeservice.tcm.tables;

import java.util.ArrayList;

import javax.swing.table.DefaultTableModel;

import com.nykredit.kundeservice.tcm.DateConstant;
import com.nykredit.kundeservice.tcm.EMonth;
import com.nykredit.kundeservice.tcm.Formatter;
import com.nykredit.kundeservice.tcm.Goal;
import com.nykredit.kundeservice.tcm.ServiceCenterData;
import com.nykredit.kundeservice.tcm.datatypes.calculations.HaandteringsGrad;
import com.nykredit.kundeservice.tcm.datatypes.calculations.KPI;
import com.nykredit.kundeservice.tcm.datatypes.calculations.Number;
import com.nykredit.kundeservice.tcm.datatypes.calculations.Tilgaengelighed;

public class BasisServiceDepartmentTableModel extends DefaultTableModel{

	private static final long serialVersionUID = 15L;
	
	private String tableName = "Afdeling Basisservice";
	
	private ArrayList<Integer> basisTeamIds = new ArrayList<Integer>();
	private ArrayList<Integer> forsikringsServiceTeamIds = new ArrayList<Integer>();
	private ArrayList<Integer> koncernOmstillingTeamIds = new ArrayList<Integer>();
	
	public BasisServiceDepartmentTableModel(ServiceCenterData centerData, 
											ArrayList<Integer> basisTeamIds,
											ArrayList<Integer> forsikringTeamIds,
											ArrayList<Integer> koncernOmstillingIds){ 
		if(basisTeamIds != null)
			this.basisTeamIds = basisTeamIds;
		
		if(forsikringTeamIds != null)
			this.forsikringsServiceTeamIds = forsikringTeamIds;
		
		if(koncernOmstillingIds != null)
			this.koncernOmstillingTeamIds = koncernOmstillingIds;
		
		ArrayList<Integer> allTeamIds = new ArrayList<Integer>();
		
		allTeamIds.addAll(this.basisTeamIds);
		allTeamIds.addAll(this.forsikringsServiceTeamIds);
		allTeamIds.addAll(this.koncernOmstillingTeamIds);
		allTeamIds.add(99);
		basisTeamIds.add(99);
		
		this.addColumn(this.tableName, new Object[]{"Telefonisk Kundetilfredshed",
		  	    					   				"Gennemførte Salg",
		  	    					   				"Produktivitet",
		  	    					   				"Tilgængelighed",
		  	    					   				"Forsikringssalg"});

		this.addColumn("Mål", new Object[]{Formatter.toPercentString(Goal.BasisTelefoniskKundetilfredshed),
										   Formatter.toNumberString(Goal.BasisAntalGennemførteSalg),
										   Formatter.toNumberString(Goal.BasisProduktivitet),
										   Formatter.toPercentString(Goal.BasisTilgængelighed),
										   "-"});
		
		for(EMonth m : EMonth.getUpToCurrentMonth())
			this.addColumn(m.toString(), new Object[]{centerData.getKundetilfredshed(m, allTeamIds, true, false),
						   							  centerData.getGennemførteSalg(m, allTeamIds),
						   							  centerData.getProduktivitet(m, basisTeamIds, false, false),
						   							  centerData.getTilgængelighed(m, basisTeamIds),
						   							  centerData.getForsikringsSalg(m, allTeamIds)});
		
		for(int i = 1; i <= EMonth.getCurrentMonth().getQuarter(); i++)
			this.addColumn(i + ". kv.", new Object[]{centerData.getKundetilfredshed(i, allTeamIds, true, false),
					   	   							 centerData.getGennemførteSalg(i, allTeamIds),
					   	   							 centerData.getProduktivitet(i, basisTeamIds, false, false),
					   	   							 centerData.getTilgængelighed(i, basisTeamIds),
					   	   							 centerData.getForsikringsSalg(i, allTeamIds)});

			
			HaandteringsGrad telefoniskKundetilfredshed = centerData.getKundetilfredshed(allTeamIds, true, false);
			Number gennemførteSalg = centerData.getGennemførteSalg(allTeamIds);
			KPI produktivitet = centerData.getProduktivitet(basisTeamIds, false,false);
			Tilgaengelighed tilgængelighed = centerData.getTilgængelighed(basisTeamIds);
	
			this.addColumn("ÅTD*", new Object[]{telefoniskKundetilfredshed,
						   						gennemførteSalg,
						   						produktivitet,
						   						tilgængelighed,
						   						centerData.getForsikringsSalg(allTeamIds)});
			if(DateConstant.completedMonths != 0){
			this.addColumn("Estimat**", new Object[]{telefoniskKundetilfredshed.getEstimat(Goal.BasisTelefoniskKundetilfredshed),
					   	   							 gennemførteSalg.getEstimate(Goal.BasisAntalGennemførteSalg),
					   	   							 produktivitet.getEstimat(Goal.BasisProduktivitet),
					   	   							 tilgængelighed.getEstimat(Goal.BasisTilgængelighed),
					   	   							 "-"});	
		
			this.addColumn("Krav***", new Object[]{telefoniskKundetilfredshed.getKrav(Goal.BasisTelefoniskKundetilfredshed),
				   	   	   						   gennemførteSalg.getKrav(Goal.BasisAntalGennemførteSalg),
				   	   	   						   produktivitet.getKrav(Goal.BasisProduktivitet),
				   	   	   						   tilgængelighed.getKrav(Goal.BasisTilgængelighed),
			
			"-"});
			}
			
		
	}
}