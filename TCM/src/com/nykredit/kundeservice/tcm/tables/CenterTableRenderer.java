package com.nykredit.kundeservice.tcm.tables;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import com.nykredit.kundeservice.swing.NTableRenderer;
import com.nykredit.kundeservice.tcm.DateConstant;
import com.nykredit.kundeservice.tcm.Goal;
import com.nykredit.kundeservice.tcm.RenderFunction;

/**
 * 
 * Renderer used in the centerTable assembled in CenterApplet
 * 
 * @author thra
 */
public class CenterTableRenderer extends NTableRenderer  {
	
	private static final long serialVersionUID = 1L;
	
    @Override
	public Component getTableCellRendererComponent(JTable table, Object value,boolean isSelected, boolean hasFocus, int row, int column){    
    	Component cell = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    	String header = table.getColumnModel().getColumn(column).getHeaderValue().toString();

		if (column > 0){
			this.setHorizontalAlignment(SwingConstants.CENTER);
		} else {
			this.setHorizontalAlignment(SwingConstants.LEFT);
		}
		if(column != 1){
			switch(row){
				case 0:
					RenderFunction.setColoringPct(cell, header, value, Goal.CenterKundekaldBesvaretIndenfor25Sek);
					break;
				case 1:
					RenderFunction.setColoringPct(cell, header, value, Goal.CenterTelefoniskKundetilfredshed);
					break;
				case 2:
					RenderFunction.setColoringPct(cell, header, value, Goal.CenterKundeAktiviteterIndenforTidsfrist);
					break;
				case 3:
					RenderFunction.setColoringInt(cell, header, value, Goal.CenterGennemførteSalg);
					break;
				case 4:
					RenderFunction.setColoringPct(cell, header, value, Goal.CenterProduktivitetsmål);
					break;
				case 5:
					RenderFunction.setColoringPct(cell, header, value, Goal.CenterKvalitet);
			}	
		}
		
		addMarking(cell,isSelected,new Color(0,0,128),80);
		
    	return cell;
    }
}