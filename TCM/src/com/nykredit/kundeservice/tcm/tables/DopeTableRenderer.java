package com.nykredit.kundeservice.tcm.tables;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import com.nykredit.kundeservice.swing.NTableRenderer;
import com.nykredit.kundeservice.tcm.DateConstant;
import com.nykredit.kundeservice.tcm.Goal;
import com.nykredit.kundeservice.tcm.RenderFunction;

public class DopeTableRenderer extends NTableRenderer  {
	
	private static final long serialVersionUID = 1L;

    @Override
	public Component getTableCellRendererComponent(JTable table, Object value,boolean isSelected, boolean hasFocus, int row, int column){    
    	Component cell = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    	String header = table.getColumnModel().getColumn(column).getHeaderValue().toString();

		if (column > 0){this.setHorizontalAlignment(SwingConstants.CENTER);
		}else {this.setHorizontalAlignment(SwingConstants.LEFT);}
    	
		if (row == 0 & column>1)
			if(column != 1)
				RenderFunction.setColoringPct(cell,header,value,Goal.DPKundekaldBesvaretIndenfor25Sek);	

		if (row == 1 & column>1)
			if(column != 1)
				RenderFunction.setColoringPct(cell,header,value,Goal.DPKundeAktiviteterIndenforTidsfrist);
		
		if (row == 2 & column>1)
			if(column != 1)
				RenderFunction.setColoringPctReversed(cell,header,value,Goal.DPWFCMPrognose);
		
		if (row == 3 & column>1)
			if(column != 1)
				RenderFunction.setColoringPct(cell,header,value,Goal.DPVidenBarometer);
		
		if (row == 4 & column>1)
			if(column != 1)
				RenderFunction.setColoringInt(cell,header,value,Goal.DPAhtReduktion);
		
		if (row == 5 & column>1)
			if(column != 1)
				RenderFunction.setColoringInt(cell,header,value,Goal.DPSmart);
		
		addMarking(cell,isSelected,new Color(0,0,128),80);
		
    	return cell;
    }
}