package com.nykredit.kundeservice.tcm.tables;

import javax.swing.table.DefaultTableModel;

import com.nykredit.kundeservice.tcm.DateConstant;
import com.nykredit.kundeservice.tcm.EMonth;
import com.nykredit.kundeservice.tcm.Formatter;
import com.nykredit.kundeservice.tcm.Goal;
import com.nykredit.kundeservice.tcm.ServiceCenterData;
import com.nykredit.kundeservice.tcm.Team;
import com.nykredit.kundeservice.tcm.datatypes.PhoneQueueData.Queue;
import com.nykredit.kundeservice.tcm.datatypes.calculations.HaandteringsGrad;

public class ErhvervsserviceTableModel extends DefaultTableModel {

	private static final long serialVersionUID = 1L;
	
	public ErhvervsserviceTableModel(ServiceCenterData centerData, Team team){
		this.addColumn(team.getTeamName(), new Object[]{"Kundekald besvaret indefor 25 sek.",
														"Aktiviteter indenfor tidsfrist",
														"Telefonisk kundetilfredshed"});
		
		this.addColumn("M�l", new Object[]{Formatter.toPercentString(Goal.ESKundekaldBesvaretIndenfor25Sek),
										   Formatter.toPercentString(Goal.ESAktiviteterIndenforTidsfrist),
										   Formatter.toPercentString(Goal.ESKundetilfredshed)});
		
		for(EMonth m : EMonth.getUpToCurrentMonth()){

			this.addColumn(m.toString(), new Object[]{centerData.getTlfKoeSvarPct(m, Queue.Erhverv),
						   							  centerData.getAktiviteterIndenforTidsfrist(m, team, false),
						   							  centerData.getKundetilfredshed(m, team, true, false)});
		}
		for(int i = 1; i <= EMonth.getCurrentMonth().getQuarter(); i++){
			this.addColumn(i + ". kv.", new Object[]{centerData.getTlfKoeSvarPct(i, Queue.Erhverv), 
						   							 centerData.getAktiviteterIndenforTidsfrist(i, team, false),
						   							 centerData.getKundetilfredshed(i,team, true, false)});
		}
		
		HaandteringsGrad svarProcent = centerData.getTlfKoeSvarPct(Queue.Erhverv);
		HaandteringsGrad aktiviteterIndenforTidsfrist = centerData.getAktiviteterIndenforTidsfrist(team, false); 
		HaandteringsGrad kundetilfredshed = centerData.getKundetilfredshed(team, true, false);
		
		if(DateConstant.completedMonths > 0 || DateConstant.completedMonths == 0){
			this.addColumn("�TD*", new Object[]{svarProcent, 
						   						aktiviteterIndenforTidsfrist,
						   						kundetilfredshed});
			if(DateConstant.completedMonths != 0){

			this.addColumn("Estimat**", new Object[]{svarProcent.getEstimat(Goal.ESKundekaldBesvaretIndenfor25Sek),
						   							 aktiviteterIndenforTidsfrist.getEstimat(Goal.ESAktiviteterIndenforTidsfrist),
						   							 kundetilfredshed.getEstimat(Goal.ESKundetilfredshed)});
		
			this.addColumn("Krav***", new Object[]{svarProcent.getKrav(Goal.ESKundekaldBesvaretIndenfor25Sek),
						   						   aktiviteterIndenforTidsfrist.getKrav(Goal.ESAktiviteterIndenforTidsfrist),
						   						   kundetilfredshed.getKrav(Goal.ESKundetilfredshed)});
			}
		}
	}
}
