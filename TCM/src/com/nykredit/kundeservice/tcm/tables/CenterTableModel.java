package com.nykredit.kundeservice.tcm.tables;

import javax.swing.table.DefaultTableModel;

import com.nykredit.kundeservice.tcm.DateConstant;
import com.nykredit.kundeservice.tcm.EMonth;
import com.nykredit.kundeservice.tcm.Formatter;
import com.nykredit.kundeservice.tcm.Goal;
import com.nykredit.kundeservice.tcm.ServiceCenterData;
import com.nykredit.kundeservice.tcm.datatypes.calculations.HaandteringsGrad;
import com.nykredit.kundeservice.tcm.datatypes.calculations.Number;

/**
 * 
 * Model used to display the table with Goals for the Center
 *
 * @author thra
 */
public class CenterTableModel extends DefaultTableModel{

	private static final long serialVersionUID = 1L;
	
	public CenterTableModel(ServiceCenterData centerData){
		
		this.addColumn("Centermål for Servicecentret", new Object[]{"Kundekald besvaret indefor 25 sek.",
												 					"Telefonisk Kundetilfredshed",
												 					"Kundeaktiviteter indenfor tidsfrist",
												 					"Gennemførte salg",
												 					"Produktivitetsmål",
												 					"Kvalitet"});
		
		this.addColumn("Mål", new Object[]{Formatter.toPercentString(Goal.CenterKundekaldBesvaretIndenfor25Sek),
										   Formatter.toPercentString(Goal.CenterTelefoniskKundetilfredshed),
										   Formatter.toPercentString(Goal.CenterKundeAktiviteterIndenforTidsfrist),
										   Formatter.toNumberString(Goal.CenterGennemførteSalg),
										   Formatter.toPercentString(Goal.CenterProduktivitetsmål),
										   Formatter.toPercentString(Goal.CenterKvalitet)});
		
		for(EMonth m : EMonth.getUpToCurrentMonth()){
			this.addColumn(m.toString(), new Object[]{centerData.getTlfKoeSvarPctCenter(m),
						   							  centerData.getKundetilfredshedCenter(m, true, false),
						   							  centerData.getKundeAktiviteterIndenforTidsfristCenter(m),
						   							  centerData.getGennemførteSalgCenter(m),
						   							  centerData.getServiceCenterProduktivitet(m),
						   							  centerData.getServiceCenterKvalitet(m)});
		}
		
		for(int i = 1; i <= EMonth.getCurrentMonth().getQuarter(); i++)
			this.addColumn(i + ". kv.", new Object[]{centerData.getTlfKoeSvarPctCenter(i),
						   							 centerData.getKundetilfredshedCenter(i, true, false),
						   							 centerData.getKundeAktiviteterIndenforTidsfristCenter(i),
						   							 centerData.getGennemførteSalgCenter(i),
						   							 centerData.getServiceCenterProduktivitet(i),
						   							 centerData.getServiceCenterKvalitet(i)});
		
		if(DateConstant.completedMonths > 0 || DateConstant.completedMonths == 0){
			HaandteringsGrad svarPct = centerData.getTlfKoeSvarPctCenter();
			HaandteringsGrad kundetilfredshed = centerData.getKundetilfredshedCenter(true, false);
			HaandteringsGrad kundeaktiviteterIndenforTidsfrist = centerData.getKundeAktiviteterIndenforTidsfristCenter();
			Number gennemførteSalg = centerData.getGennemførteSalgCenter();
			HaandteringsGrad produktivitetsmål = centerData.getServiceCenterProduktivitet();
			HaandteringsGrad kvalitet = centerData.getServiceCenterKvalitet();
		
			this.addColumn("ÅTD*", new Object[]{svarPct,
						   						kundetilfredshed,
						   						kundeaktiviteterIndenforTidsfrist,
						   						gennemførteSalg,
						   						produktivitetsmål,
						   						kvalitet});
			if(DateConstant.completedMonths != 0){

			this.addColumn("Estimat**",new Object[]{svarPct.getEstimat(Goal.CenterKundekaldBesvaretIndenfor25Sek),
					   	   							kundetilfredshed.getEstimat(Goal.CenterTelefoniskKundetilfredshed),
					   	   							kundeaktiviteterIndenforTidsfrist.getEstimat(Goal.CenterKundeAktiviteterIndenforTidsfrist),
					   	   							gennemførteSalg.getEstimate(Goal.CenterGennemførteSalg),
					   	   							produktivitetsmål.getEstimat(Goal.CenterProduktivitetsmål),
					   	   							kvalitet.getEstimat(Goal.CenterKvalitet)});
		
			this.addColumn("Krav***", new Object[]{svarPct.getKrav(Goal.CenterKundekaldBesvaretIndenfor25Sek),
					   	   						   kundetilfredshed.getKrav(Goal.CenterTelefoniskKundetilfredshed),
					   	   						   kundeaktiviteterIndenforTidsfrist.getKrav(Goal.CenterKundeAktiviteterIndenforTidsfrist),
					   	   						   gennemførteSalg.getKrav(Goal.CenterGennemførteSalg),
					   	   						   produktivitetsmål.getKrav(Goal.CenterProduktivitetsmål),
					   	   						   kvalitet.getKrav(Goal.CenterKvalitet)});
			}
		}
	}
}