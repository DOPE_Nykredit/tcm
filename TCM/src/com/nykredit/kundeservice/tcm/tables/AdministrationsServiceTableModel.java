package com.nykredit.kundeservice.tcm.tables;

import javax.swing.table.DefaultTableModel;

import com.nykredit.kundeservice.tcm.DateConstant;
import com.nykredit.kundeservice.tcm.EMonth;
import com.nykredit.kundeservice.tcm.Goal;
import com.nykredit.kundeservice.tcm.ServiceCenterData;
import com.nykredit.kundeservice.tcm.datatypes.calculations.HaandteringsGrad;

public class AdministrationsServiceTableModel extends DefaultTableModel {

	private static final long serialVersionUID = 1L;

	public AdministrationsServiceTableModel(ServiceCenterData centerData){
		this.addColumn("Afdeling administrationsservice", new Object[]{"Service",
																	   "Kvalitet - Reduktion af fejl og klager",
																	   "Kvalitet - Brugertilfredshed",
																	   "Tilg�ngelighed",
																	   "Produktivitetsforbedring",
																	   "Ledelsesm�l - medarbejdertrivsel"});

		this.addColumn("M�l",new Object[]{"95%",
										  "7,5",
										  "-",
										  "80/25",
										  "5%",
										  ">75"});

		for(EMonth m : EMonth.getUpToCurrentMonth())
			this.addColumn(m.toString(), new Object[]{centerData.getAdministrationServiceService(m)});
		
		for(int i = 1; i <= EMonth.getCurrentMonth().getQuarter(); i++)
			this.addColumn(i + ". kv.", new Object[]{centerData.getAdministrationServiceService(i)});
				
		if(EMonth.getNumberOfCompletedMonths() > 0){
			HaandteringsGrad service = centerData.getAdministrationServiceService(); 
			
			this.addColumn("�TD*", new Object[]{service});
			if(DateConstant.completedMonths != 0){

			this.addColumn("Estimat**", new Object[]{service.getEstimat(Goal.AsService)});
		
			this.addColumn("Krav***", new Object[]{service.getKrav(Goal.AsService)});
			}
		}
	}
}