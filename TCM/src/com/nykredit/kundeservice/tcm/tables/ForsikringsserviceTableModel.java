package com.nykredit.kundeservice.tcm.tables;

import javax.swing.table.DefaultTableModel;

import com.nykredit.kundeservice.tcm.DateConstant;
import com.nykredit.kundeservice.tcm.EMonth;
import com.nykredit.kundeservice.tcm.Formatter;
import com.nykredit.kundeservice.tcm.Goal;
import com.nykredit.kundeservice.tcm.ServiceCenterData;
import com.nykredit.kundeservice.tcm.Team;
import com.nykredit.kundeservice.tcm.datatypes.calculations.HaandteringsGrad;
import com.nykredit.kundeservice.tcm.datatypes.calculations.KPI;
import com.nykredit.kundeservice.tcm.datatypes.calculations.Number;

public class ForsikringsserviceTableModel extends DefaultTableModel {

	private static final long serialVersionUID = 1L;
	
	public ForsikringsserviceTableModel(ServiceCenterData centerData, Team team){
		this.addColumn(team.getTeamName(), new Object[]{"Forsikringssalg",
														"Mersalg",
														"Gennemførte salg",
														"Produktivitet",
														"Telefonisk Kundetilfredshed",
														"Aktiviteter indenfor tidsfrist"});
		
		this.addColumn("Mål", new Object[]{Formatter.toNumberString(Goal.FSForsikringssalg),
								 		   Formatter.toPercentString(Goal.FSMersalg),
								 		   Formatter.toNumberString(Goal.FSAntalAfledteSalg),
										   Formatter.toNumberString(Goal.FSProduktivitet),
										   Formatter.toPercentString(Goal.FSTelefoniskKundetilfredshed),
										   Formatter.toPercentString(Goal.FSAktiviteterIndenforTidsfrist)});
		
		for(EMonth m : EMonth.getUpToCurrentMonth())
			this.addColumn(m.toString(), new Object[]{centerData.getForsikringsSalg(m, team),
						   							  centerData.getMersalg(m, team),
						   							  centerData.getGennemførteSalg(m, team),
						   							  centerData.getProduktivitet(m, team, false, false),
						   							  centerData.getKundetilfredshed(m, team, true, false),
						   							  centerData.getAktiviteterIndenforTidsfrist(m, team, false)});
		
		for(int i = 1; i <= EMonth.getCurrentMonth().getQuarter(); i++)
			this.addColumn(i + ". kv.", new Object[]{centerData.getForsikringsSalg(i, team),
					   	   							 centerData.getMersalg(i, team),
					   	   							 centerData.getGennemførteSalg(i, team),
					   	   							 centerData.getProduktivitet(i, team, false, false),
					   	   							 centerData.getKundetilfredshed(i, team, true, false),
					   	   							 centerData.getAktiviteterIndenforTidsfrist(i, team, false)});

		if(DateConstant.completedMonths > 0 || DateConstant.completedMonths == 0){
			Number forsikringsSalg = centerData.getForsikringsSalg(team);
			HaandteringsGrad mersalg = centerData.getMersalg(team);
			Number afledteSalg = centerData.getGennemførteSalg(team);
			KPI produktivitet = centerData.getProduktivitet(team, false, false);
			HaandteringsGrad telefoniskKundetilfredshed = centerData.getKundetilfredshed(team, true, false);
			HaandteringsGrad aktiviteterIndenforTidsfrist = centerData.getAktiviteterIndenforTidsfrist(team, false);
		
			this.addColumn("ÅTD*", new Object[]{forsikringsSalg,
						   						mersalg,	
						   						afledteSalg,
						   						produktivitet,
						   						telefoniskKundetilfredshed,
						   						aktiviteterIndenforTidsfrist});
		
//			this.addColumn("Estimat**", new Object[]{forsikringsSalg.getEstimate(Goal.FSForsikringssalg),
//						   							 mersalg.getEstimat(Goal.FSMersalg),
//						   							 afledteSalg.getEstimate(Goal.FSAntalAfledteSalg),
//						   							 produktivitet.getEstimat(Goal.FSProduktivitet),
//						   							 telefoniskKundetilfredshed.getEstimat(Goal.FSTelefoniskKundetilfredshed),
//						   							 aktiviteterIndenforTidsfrist.getEstimat(Goal.FSAktiviteterIndenforTidsfrist)});
//		
//			this.addColumn("Krav***", new Object[]{forsikringsSalg.getKrav(Goal.FSForsikringssalg),
//					   	   						   mersalg.getKrav(Goal.FSMersalg),
//					   	   						   afledteSalg.getKrav(Goal.FSAntalAfledteSalg),
//					   	   						   produktivitet.getKrav(Goal.FSProduktivitet),
//					   	   						   telefoniskKundetilfredshed.getKrav(Goal.FSTelefoniskKundetilfredshed),
//					   	   						   aktiviteterIndenforTidsfrist.getKrav(Goal.FSAktiviteterIndenforTidsfrist)});
		}
	}
}
