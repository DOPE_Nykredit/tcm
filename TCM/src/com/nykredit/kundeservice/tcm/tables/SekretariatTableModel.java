package com.nykredit.kundeservice.tcm.tables;

import javax.swing.table.DefaultTableModel;

import com.nykredit.kundeservice.tcm.*;
import com.nykredit.kundeservice.tcm.datatypes.calculations.HaandteringsGrad;
import com.nykredit.kundeservice.tcm.datatypes.calculations.Number;

public class SekretariatTableModel extends DefaultTableModel {

	private static final long serialVersionUID = 1L;
	
	public SekretariatTableModel(ServiceCenterData centerData){

		this.addColumn("Sekretariat og Planl�gning", new Object[]{"Mailbesvarelse",
												 	    		  "Oprettelse af medarbejdere",
												 	    		  "Optimering af processer"});
		
		this.addColumn("M�l",new Object[]{Formatter.toPercentString(Goal.SPMailbesvarelse),
										  Formatter.toPercentString(Goal.SPOprettelseAfMedarbejdere),
										  Formatter.toNumberString(Goal.SPOptimeringAfProcesser)});
		
		for(EMonth m : EMonth.getUpToCurrentMonth())
			this.addColumn(m.toString(), new Object[]{centerData.getSPMailbesvarelse(m), 
						   							  centerData.getSPOprettelseAfMedarbejdere(m),
						   							  centerData.getSPOptimeringAfProcesser(m)});
		
		for(int i = 1; i <= EMonth.getCurrentMonth().getQuarter(); i++)
			this.addColumn(i + ". kv.", new Object[]{centerData.getSPMailbesvarelse(i), 
					       							 centerData.getSPOprettelseAfMedarbejdere(i),
					       							 centerData.getSPOptimeringAfProcesser(i)});
		
		if(DateConstant.completedMonths > 0 || DateConstant.completedMonths == 0){
			HaandteringsGrad besvarelseAfMails = centerData.getSPMailbesvarelse();
			HaandteringsGrad oprettelseAfMedarbejdere = centerData.getSPOprettelseAfMedarbejdere();
			Number optimering = centerData.getSPOptimeringAfProcesser();
		
			this.addColumn("�TD*", new Object[]{besvarelseAfMails, 
				       	   						oprettelseAfMedarbejdere,
				       	   						optimering});
			if(DateConstant.completedMonths != 0){

			this.addColumn("Estimat**", new Object[]{besvarelseAfMails.getEstimat(Goal.SPMailbesvarelse),
					   	   							 oprettelseAfMedarbejdere.getEstimat(Goal.SPOprettelseAfMedarbejdere),
					   	   							 null});
		
			this.addColumn("Krav***", new Object[]{besvarelseAfMails.getKrav(Goal.SPMailbesvarelse), 
					   	   						   oprettelseAfMedarbejdere.getKrav(Goal.SPOprettelseAfMedarbejdere),
					   	   						   null});
			}
		}
	}
}