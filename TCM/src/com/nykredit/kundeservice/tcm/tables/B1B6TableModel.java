package com.nykredit.kundeservice.tcm.tables;

import javax.swing.table.DefaultTableModel;

import com.nykredit.kundeservice.tcm.DateConstant;
import com.nykredit.kundeservice.tcm.EMonth;
import com.nykredit.kundeservice.tcm.Formatter;
import com.nykredit.kundeservice.tcm.Goal;
import com.nykredit.kundeservice.tcm.ServiceCenterData;
import com.nykredit.kundeservice.tcm.Team;
import com.nykredit.kundeservice.tcm.datatypes.calculations.HaandteringsGrad;
import com.nykredit.kundeservice.tcm.datatypes.calculations.KPI;
import com.nykredit.kundeservice.tcm.datatypes.calculations.Number;
import com.nykredit.kundeservice.tcm.datatypes.calculations.Tilgaengelighed;

public class B1B6TableModel extends DefaultTableModel {

	private static final long serialVersionUID = 1L;
		
	public B1B6TableModel(ServiceCenterData serviceCenterData, Team team){
		this.addColumn(team.getTeamName(), new Object[]{"Telefonisk Kundetilfredshed",
									  			  	    "Gennemførte salg",
									  			  	    "Produktivitet",
												  		"Tilgængelighed",
												  		"Forsikringssalg"});
		
		this.addColumn("Mål", new Object[]{Formatter.toPercentString(Goal.B1B6TelefoniskKundetilfredshed),
										   Formatter.toNumberString(Goal.B1B6AntalGennemførteSalg),
										   Formatter.toNumberString(Goal.B1B6Produktivitet),
										   Formatter.toPercentString(Goal.B1B6Tilgængelighed),
										   "-"});
		
		for(EMonth m : EMonth.getUpToCurrentMonth())
			this.addColumn(m.toString(), new Object[]{serviceCenterData.getKundetilfredshed(m, team, true, false),
						   							  serviceCenterData.getGennemførteSalg(m, team),
						   							  serviceCenterData.getProduktivitet(m, team, true, false),
						   							  serviceCenterData.getTilgængelighed(m, team),
						   							  serviceCenterData.getForsikringsSalg(m, team)});
			
		for(int i = 1; i <= EMonth.getCurrentMonth().getQuarter(); i++){
			this.addColumn(i + ". kv.", new Object[]{serviceCenterData.getKundetilfredshed(i, team, true, false),
					   	   							 serviceCenterData.getGennemførteSalg(i, team),
					   	   							 serviceCenterData.getProduktivitet(i, team, true , false),
					   	   							 serviceCenterData.getTilgængelighed(i, team),
					   	   							 serviceCenterData.getForsikringsSalg(i, team)});
		}
		if(DateConstant.completedMonths > 0 || DateConstant.completedMonths == 0){
			HaandteringsGrad telefoniskKundetilfredshed = serviceCenterData.getKundetilfredshed(team, true, false);
			Number afledtSalg = serviceCenterData.getGennemførteSalg(team);
			KPI produktivitet = serviceCenterData.getProduktivitet(team, true, false);
			Tilgaengelighed tilgængelighed = serviceCenterData.getTilgængelighed(team); 
			Number forsikringssalg = serviceCenterData.getForsikringsSalg(team);
		
			this.addColumn("ÅTD*", new Object[]{telefoniskKundetilfredshed,
					   	   						afledtSalg,
					   	   						produktivitet,
					   	   						tilgængelighed,
					   	   						forsikringssalg});
			if(DateConstant.completedMonths != 0){
			this.addColumn("Estimat**", new Object[]{telefoniskKundetilfredshed.getEstimat(Goal.B1B6TelefoniskKundetilfredshed),
						   							 afledtSalg.getEstimate(Goal.B1B6AntalGennemførteSalg),
						   							 produktivitet.getEstimat(Goal.B1B6Produktivitet),
						   							 tilgængelighed.getEstimat(Goal.B1B6Tilgængelighed),
						   							 "-"});
		
			this.addColumn("Krav***", new Object[]{telefoniskKundetilfredshed.getKrav(Goal.B1B6TelefoniskKundetilfredshed),
						   						   afledtSalg.getKrav(Goal.B1B6AntalGennemførteSalg),
						   						   produktivitet.getKrav(Goal.B1B6Produktivitet),
						   						   tilgængelighed.getKrav(Goal.B1B6Tilgængelighed),
						   						   "-"});
			}
		}
	}
	
}