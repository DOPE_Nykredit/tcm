package com.nykredit.kundeservice.tcm.tables;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.SwingConstants;

import com.nykredit.kundeservice.swing.NTableRenderer;
import com.nykredit.kundeservice.tcm.DateConstant;
import com.nykredit.kundeservice.tcm.Goal;
import com.nykredit.kundeservice.tcm.RenderFunction;

public class DigitaleServicesDepartmentTableRenderer extends NTableRenderer{

	private static final long serialVersionUID = 1L;
	
    @Override
	public Component getTableCellRendererComponent(JTable table, Object value,boolean isSelected, boolean hasFocus, int row, int column){    
    	Component cell = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    	String header = table.getColumnModel().getColumn(column).getHeaderValue().toString();

		if (column > 0)
			this.setHorizontalAlignment(SwingConstants.CENTER);
		else
			this.setHorizontalAlignment(SwingConstants.LEFT);
		
		if(column != 1){
			switch(row){
				case 0:
					RenderFunction.setColoringPct(cell, header, value, Goal.DSKundekaldBesvaretIndenfor25Sek);
					break;
				case 1:
					RenderFunction.setColoringPct(cell, header, value, Goal.DSWebdeskSvarprocent);
					break;
				case 2:
					RenderFunction.setColoringPct(cell, header, value, Goal.DSServicemålMails);
					break;
				case 3:
					RenderFunction.setColoringPct(cell, header, value, Goal.DSDialogAktiviteterIndenforTidsfrist);
					break;
				case 4:
					RenderFunction.setColoringPct(cell, header, value, Goal.DSOpgaverISupport);
					break;
				case 5:
					RenderFunction.setColoringIndex(cell, header, value, Goal.DSProduktivitetMailservice);
					break;
				case 6:
					RenderFunction.setColoringIndex(cell, header, value, Goal.DSProduktivitetSupport);
					break;
				case 7:
					RenderFunction.setColoringPct(cell, header, value, Goal.DSKontaktFørOpsigelser);
					break;
				case 8:
					RenderFunction.setColoringPct(cell, header, value, Goal.DSKundetilfredshed);
					break;
			}
		}
		addMarking(cell,isSelected,new Color(0,0,128),80);
		
    	return cell;
    }
}