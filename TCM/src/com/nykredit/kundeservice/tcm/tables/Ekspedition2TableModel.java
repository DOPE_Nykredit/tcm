package com.nykredit.kundeservice.tcm.tables;

import javax.swing.table.DefaultTableModel;

import com.nykredit.kundeservice.tcm.DateConstant;
import com.nykredit.kundeservice.tcm.EMonth;
import com.nykredit.kundeservice.tcm.Formatter;
import com.nykredit.kundeservice.tcm.Goal;
import com.nykredit.kundeservice.tcm.ServiceCenterData;
import com.nykredit.kundeservice.tcm.Team;
import com.nykredit.kundeservice.tcm.datatypes.calculations.AHT;
import com.nykredit.kundeservice.tcm.datatypes.calculations.Number;
import com.nykredit.kundeservice.tcm.datatypes.calculations.Tilgaengelighed;

public class Ekspedition2TableModel extends DefaultTableModel {

	private static final long serialVersionUID = 1L;
	
	public Ekspedition2TableModel(ServiceCenterData centerData, Team team){
		
		this.addColumn(team.getTeamName(), new Object[]{"Tilgængelighed",
														"Gennemførte salg",
														"Produktivitet"});
		
		this.addColumn("Mål", new Object[]{Formatter.toPercentString(Goal.E2Tilgængelighed),
										   Formatter.toNumberString(Goal.E2AntalGennemførteSalg),
										   Formatter.toTimeString(Goal.E2Produktivitet)});
		
		for(EMonth m : EMonth.getUpToCurrentMonth())
			this.addColumn(m.toString(), new Object[]{centerData.getTilgængelighed(m, team), 
						   							  centerData.getGennemførteSalg(m, team), 
						   							  centerData.getAHT(m, team)});
			
		for(int i = 1; i <= EMonth.getCurrentMonth().getQuarter(); i++)
			this.addColumn(i + ". kv.", new Object[]{centerData.getTilgængelighed(i, team),  
						   							 centerData.getGennemførteSalg(i, team), 
						   							 centerData.getAHT(i, team)});
	
		if(DateConstant.completedMonths > 0 || DateConstant.completedMonths == 0){
			Tilgaengelighed tilgængelighed = centerData.getTilgængelighed(team);
			Number gennemførteSalg = centerData.getGennemførteSalg(team);
			AHT produkvititet = centerData.getAHT(team);
	
			this.addColumn("ÅTD*", new Object[]{tilgængelighed, 
						   						gennemførteSalg, 
						   						produkvititet});
			if(DateConstant.completedMonths != 0){

			this.addColumn("Estimat**", new Object[]{tilgængelighed.getEstimat(Goal.E2Tilgængelighed), 
						   							 gennemførteSalg.getEstimate(Goal.E2AntalGennemførteSalg), 
						   							 produkvititet.getEstimat(Goal.E2Produktivitet)});
		
			this.addColumn("Krav***", new Object[]{tilgængelighed.getKrav(Goal.E2Tilgængelighed), 
						   						   gennemførteSalg.getKrav(Goal.E2AntalGennemførteSalg), 
						   						   produkvititet.getKrav(Goal.E2Produktivitet)});
			}
		}
	}
}