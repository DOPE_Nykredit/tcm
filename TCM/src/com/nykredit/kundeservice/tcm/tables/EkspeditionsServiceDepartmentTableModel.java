package com.nykredit.kundeservice.tcm.tables;

import java.util.ArrayList;

import javax.swing.table.DefaultTableModel;

import com.nykredit.kundeservice.tcm.DateConstant;
import com.nykredit.kundeservice.tcm.EMonth;
import com.nykredit.kundeservice.tcm.Formatter;
import com.nykredit.kundeservice.tcm.Goal;
import com.nykredit.kundeservice.tcm.ServiceCenterData;
import com.nykredit.kundeservice.tcm.datatypes.calculations.AHT;
import com.nykredit.kundeservice.tcm.datatypes.calculations.HaandteringsGrad;
import com.nykredit.kundeservice.tcm.datatypes.calculations.Number;
import com.nykredit.kundeservice.tcm.datatypes.calculations.Tilgaengelighed;

public class EkspeditionsServiceDepartmentTableModel extends DefaultTableModel{

	private static final long serialVersionUID = 1L;
	
	private String tableName = "Afdeling Ekspeditionsservice";
	
	public EkspeditionsServiceDepartmentTableModel(ServiceCenterData centerData, 
												   ArrayList<Integer> e1TeamIds,
												   ArrayList<Integer> e2TeamIds){
		ArrayList<Integer> teamIds = new ArrayList<Integer>();
		
		if(e1TeamIds != null)
			teamIds.addAll(e1TeamIds);
		
		if(e2TeamIds != null)
			teamIds.addAll(e2TeamIds);
		
		this.addColumn(this.tableName, new Object[]{"Tilgængelighed",
													"Gennemførte salg",
													"Kvalitet",
													"Telefonisk Kundetilfredshed",
													"Produktivitet"});
		
		this.addColumn("Mål", new Object[]{Formatter.toPercentString(Goal.EKTilgængelighed),
										   Formatter.toNumberString(Goal.EKAntalGennemførteSalg),
										   Formatter.toPercentString(Goal.EKKvalitet),
										   Formatter.toPercentString(Goal.EKTelefoniskKundetilfredshed),
										   Formatter.toTimeString(Goal.EKProduktivitet)});
		
		for(EMonth m : EMonth.getUpToCurrentMonth())
			this.addColumn(m.toString(), new Object[]{centerData.getTilgængelighed(m, teamIds), 
						   							  centerData.getGennemførteSalg(m, teamIds), 
						   							  centerData.getEkspeditionsServiceKvalitet(m), 
						   							  centerData.getKundetilfredshed(m, teamIds, true, false), 
						   							  centerData.getAHT(m, teamIds)});
		
		for(int i = 1; i <= EMonth.getCurrentMonth().getQuarter(); i++)
			this.addColumn(i + ". kv.", new Object[]{centerData.getTilgængelighed(i, teamIds), 
						   							 centerData.getGennemførteSalg(i, teamIds),
						   							 centerData.getEkspeditionsServiceKvalitet(i),
						   							 centerData.getKundetilfredshed(i, teamIds, true, false),
						   							 centerData.getAHT(i, teamIds)});
		
		if(DateConstant.completedMonths > 0 || DateConstant.completedMonths == 0){
			Tilgaengelighed tilgængelighed = centerData.getTilgængelighed(teamIds);
			Number afledteSalg = centerData.getGennemførteSalg(teamIds);
			HaandteringsGrad kvalitet = centerData.getEkspeditionsServiceKvalitet();
			HaandteringsGrad kundetilfredshed = centerData.getKundetilfredshed(teamIds, true, false);
			AHT produktivitet = centerData.getAHT(teamIds);
		
			this.addColumn("ÅTD*", new Object[]{tilgængelighed, 
					 	   						afledteSalg, 
					 	   						kvalitet, 
					 	   						kundetilfredshed, 
					 	   						produktivitet});
			if(DateConstant.completedMonths != 0){

			this.addColumn("Estimat**", new Object[]{tilgængelighed.getEstimat(Goal.EKTilgængelighed), 
						   							 afledteSalg.getEstimate(Goal.EKAntalGennemførteSalg), 
						   							 kvalitet.getEstimat(Goal.EKKvalitet), 
						   							 kundetilfredshed.getEstimat(Goal.EKTelefoniskKundetilfredshed), 
						   							 produktivitet.getEstimat(Goal.EKProduktivitet)});
		
			this.addColumn("Krav***", new Object[]{tilgængelighed.getKrav(Goal.EKTilgængelighed),
						   						   afledteSalg.getKrav(Goal.EKAntalGennemførteSalg),
						   						   kvalitet.getKrav(Goal.EKKvalitet),
						   						   kundetilfredshed.getKrav(Goal.EKTelefoniskKundetilfredshed),
						   						   produktivitet.getKrav(Goal.EKProduktivitet)});
			}
		}
	}
}