package com.nykredit.kundeservice.tcm.tables;

import javax.swing.table.DefaultTableModel;

import com.nykredit.kundeservice.tcm.DateConstant;
import com.nykredit.kundeservice.tcm.EMonth;
import com.nykredit.kundeservice.tcm.ServiceCenterData;
import com.nykredit.kundeservice.tcm.datatypes.calculations.Number;

public class KundeAktiviteterTableModel extends DefaultTableModel {

	private static final long serialVersionUID = 1L;

	public KundeAktiviteterTableModel(ServiceCenterData centerData){
		
		this.addColumn("Kundeaktiviteter", new Object[]{"Gennemførelse af konkurrencer",
														"Omsætning af NYK kampagner i SC",
														"Kundetilfredshedsmåling",
														"Online kvalitet",
														"Samarbejde med centre"});
		
		this.addColumn("Mål", new Object[]{"1 stk./kvartal",
										   "2 uger/kvartal",
										   "1 stk./kvartal",
										   "10 stk./år",
										   "1 pr. område"});
		
		for(EMonth m : EMonth.getUpToCurrentMonth())
			this.addColumn(m.toString(), new Object[]{centerData.getKaGennemførelseAfKonkurrence(m),
						   							  centerData.getKaOmsætningAfKampagner(m),
						   							  centerData.getKaKundetilfredshedsmåling(m),
						   							  centerData.getKaOnlineKvalitet(m)});
		
		for(int i = 1; i <= EMonth.getCurrentMonth().getQuarter(); i++)
			this.addColumn(i + ". kv.", new Object[]{centerData.getKaGennemførelseAfKonkurrence(i),
						   							 centerData.getKaOmsætningAfKampagner(i),
						   							 centerData.getKaKundetilfredshedsmåling(i),
						   							 centerData.getKaOnlineKvalitet(i)});
		
		if(DateConstant.completedMonths > 0 || DateConstant.completedMonths == 0){
			Number konkurrence = centerData.getKaGennemførelseAfKonkurrence();
			Number kampagne = centerData.getKaOmsætningAfKampagner();
			Number måling = centerData.getKaKundetilfredshedsmåling();
			Number kvalitet = centerData.getKaOnlineKvalitet();
		
			this.addColumn("ÅTD*", new Object[]{konkurrence,
					   	   						kampagne,
					   	   						måling,
					   	   						kvalitet});
		}
	}
}