package com.nykredit.kundeservice.tcm.tables;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import com.nykredit.kundeservice.swing.NTableRenderer;

public class ProjektTableRenderer extends NTableRenderer  {
	
	private static final long serialVersionUID = 1L;

    @Override
	public Component getTableCellRendererComponent(JTable table, Object value,boolean isSelected, boolean hasFocus, int row, int column){    
    	Component cell = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
//    	String header = table.getColumnModel().getColumn(column).getHeaderValue().toString();

		if (column > 0){this.setHorizontalAlignment(SwingConstants.CENTER);
		}else {this.setHorizontalAlignment(SwingConstants.LEFT);}
    	
//		if (row == 0 & column>1)
//			if(column-1 != DateConstant.getCurrentMonth())
//				RenderFunction.setColoringPct(cell, header, value, Goal.ProjektLeverancer);	
//
//		if (row == 1 & column>1)
//			if(column-1 != DateConstant.getCurrentMonth())
//				RenderFunction.setColoringPct(cell, header, value, Goal.ProjektTelefoniForvaltning);
//
//		if (row == 2 & column>1)
//			if(column-1 != DateConstant.getCurrentMonth())
//				RenderFunction.setColoringInt(cell, header, value, Goal.ProjektMedarbejdermøder);
//		
//		if (row == 3 & column>1)
//			if(column-1 != DateConstant.getCurrentMonth())
//				RenderFunction.setColoringBoolean(cell, header, value);
		
		addMarking(cell,isSelected,new Color(0,0,128),80);
		
    	return cell;
    }
}