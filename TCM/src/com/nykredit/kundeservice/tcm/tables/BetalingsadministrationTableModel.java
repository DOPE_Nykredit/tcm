package com.nykredit.kundeservice.tcm.tables;

import javax.swing.table.DefaultTableModel;

import com.nykredit.kundeservice.tcm.DateConstant;
import com.nykredit.kundeservice.tcm.EMonth;
import com.nykredit.kundeservice.tcm.Goal;
import com.nykredit.kundeservice.tcm.ServiceCenterData;
import com.nykredit.kundeservice.tcm.datatypes.calculations.HaandteringsGrad;

public class BetalingsadministrationTableModel extends DefaultTableModel {

	private static final long serialVersionUID = 1L;

	public BetalingsadministrationTableModel(ServiceCenterData centerData) {
		this.addColumn("Betalingsadministration", new Object[] { "Service",
				"Digitalisering", "Kvalitet",
				"Telefonisk tilg�ngelighed i afdelingen",
				"Tilg�ngelighed - formularer og mails",
				"Ledelsesm�l - medarbejdertrivsel" });

		this.addColumn("M�l", new Object[] { "95%", "85%", "-", "80/25", "-",
				">75" });

		for (EMonth m : EMonth.getUpToCurrentMonth())
			this.addColumn(
					m.toString(),
					new Object[] {
							"",
							centerData
									.getBetalingsadministrationDigitalisering(m) });

		for (int i = 1; i <= EMonth.getCurrentMonth().getQuarter(); i++)
			this.addColumn(
					i + ". kv.",
					new Object[] {
							"",
							centerData
									.getBetalingsadministrationDigitalisering(i) });

		if (DateConstant.completedMonths > 0
				|| DateConstant.completedMonths == 0) {
			HaandteringsGrad digitalisering = centerData
					.getBetalingsadministrationDigitalisering();

			this.addColumn("�TD*", new Object[] { "", digitalisering });
			if (DateConstant.completedMonths != 0) {

				this.addColumn(
						"Estimat**",
						new Object[] {
								"",
								digitalisering
										.getEstimat(Goal.BaDigitalisering) });

				this.addColumn(
						"Krav***",
						new Object[] { "",
								digitalisering.getKrav(Goal.BaDigitalisering) });
			}
		}
	}
}
