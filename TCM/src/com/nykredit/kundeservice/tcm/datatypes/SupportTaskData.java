package com.nykredit.kundeservice.tcm.datatypes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nykredit.kundeservice.data.CTIRConnection;
import com.nykredit.kundeservice.tcm.DateConstant;
import com.nykredit.kundeservice.tcm.EMonth;

public class SupportTaskData {

	private EMonth month;
	
	private int totalTasks;
	private int completedWithinDeadline;
	
	public EMonth getMonth(){
		return this.month;
	}
	
	public int getCompletedWithinDeadline(){
		return this.completedWithinDeadline;
	}
	public int getTotalTasks(){
		return this.totalTasks;
	}
	
	public SupportTaskData(EMonth month, int totalTasks, int completedWithinDeadline){
		this.month = month;
		this.totalTasks = totalTasks;
		this.completedWithinDeadline = completedWithinDeadline;
	}
	
	public static ArrayList<SupportTaskData> getSupportTaskData(CTIRConnection conn) throws SQLException{
		ArrayList<SupportTaskData> supportTaskData = new ArrayList<SupportTaskData>();
		String sqlQuery = "SELECT " +
						      "TO_CHAR(COMPLETED_DATE, 'MM') AS MONTH, " +
						      "COUNT(*) AS PRODUCERET, " +
						      "COUNT(CASE WHEN COMPLETED_DATE <= DEADLINE THEN 1 END) AS INDENFOR_DEADLINE " +
						  "FROM " +     
						      "KS_DRIFT.SOP_TASK_DATA " +
						  "WHERE " +    
						      "DELETED = 0 AND " +
						  	  "COMPLETED_DATE >= '" + DateConstant.startDate + "' AND " + 
						  	  "COMPLETED_DATE <= '" + DateConstant.endDate + "' AND " +
						  	  "TASK_ID NOT IN (3, 4, 5, 20, 23, 25, 26, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 65, 67, 70, 83, 84, 85, 87, 88, 99) " +
						  "GROUP BY " + 
						  	  "TO_CHAR(COMPLETED_DATE, 'MM')";
		
		ResultSet rs = conn.Hent_tabel(sqlQuery);
		
		while(rs.next())
			supportTaskData.add(new SupportTaskData(EMonth.getFromMonthNumber(rs.getInt("MONTH")),
													rs.getInt("PRODUCERET"),
			 									 	rs.getInt("INDENFOR_DEADLINE")));
		
		return supportTaskData;
	}
}