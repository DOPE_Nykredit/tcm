package com.nykredit.kundeservice.tcm.datatypes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nykredit.kundeservice.data.CTIRConnection;
import com.nykredit.kundeservice.tcm.DateConstant;
import com.nykredit.kundeservice.tcm.EMonth;

public class DialogActivityData {

	private EMonth month;
	private int teamId;
	
	private int incomingEmails = 0;
	private int completedWithinDeadline = 0;
	private int exceededDeadline = 0;
	private int additionalSalesToBasis = 0;
	private int contactBeforeCancelation = 0;
	private int contactBeforeCancelationWithinDeadline = 0;
	
	public EMonth getMonth(){
		return this.month;
	}
	public int getTeamId(){
		return this.teamId;
	}
	
	public int getIncomingEmails(){
		return this.incomingEmails;
	}
	public int getCompletedWithinDeadline(){
		return this.completedWithinDeadline;
	}
	public int getExceededDeadline(){
		return this.exceededDeadline;
	}
	public int getAdditionalSalesToBasis(){
		return this.additionalSalesToBasis;
	}
	public int getContactBeforeCancelation(){
		return this.contactBeforeCancelation;
	}
	public int getContactBeforeCancelationWithinDeadline(){
		return this.contactBeforeCancelationWithinDeadline;
	}
	
	public int getTotalActivities(){
		return (this.completedWithinDeadline + this.exceededDeadline);
	}

	public DialogActivityData(EMonth month, 
							  int teamId,
							  int incomingEmails,
							  int completedWithingDeadline, 
							  int exceededDeadline,
							  int additionalSalesToBais,
							  int contactBeforeCancelation,
							  int contactBeforeCancelationWithinDeadline){
		this.month = month;
		this.teamId = teamId;
		this.incomingEmails = incomingEmails;
		this.completedWithinDeadline = completedWithingDeadline;
		this.exceededDeadline = exceededDeadline;
		this.contactBeforeCancelation = contactBeforeCancelation;
		this.contactBeforeCancelationWithinDeadline = contactBeforeCancelationWithinDeadline;
	}	
	
	public static ArrayList<DialogActivityData> getDialogActivityData(CTIRConnection conn) throws SQLException{
		ArrayList<DialogActivityData> dialogActivityData = new ArrayList<DialogActivityData>();
		
		String sqlQuery = "SELECT " +
						  	  "TO_CHAR(DATO,'MM') AS MONTH, " +
						  	  "TEAM_ID, " +
						  	  "COUNT(CASE WHEN \"Aktivitetstype\" = 'Email - Indg�ende' THEN 1 END) AS EMAIL_INDG�ENDE, " +
						  	  "COUNT(CASE WHEN \"Faktisk afsluttet dato\" > \"Senest d\" AND \"Aktivitetstype\" NOT IN('Opf�lgningservice','Email - Indg�ende' ) THEN 1 END) AS AKTIVITETER_OVER, " +
						  	  "COUNT(CASE WHEN \"Faktisk afsluttet dato\" <= \"Senest d\" AND (\"Faktisk afslutning\" - \"Oprettet dato\" > (2 / 24) OR \"Oprettet af Initialer\" <> \"Ansvarlig Initialer\") AND \"Aktivitetstype\" NOT IN ('Opf�lgningservice', 'Email - Indg�ende') THEN 1 END) AS AKTIVITETER_INDEN, " +
						  	  "COUNT(CASE WHEN \"Aktivitetstype\" = 'Udg�ende kald' AND \"Delaktivitet\" = 'Henvisning eksisterende kunde' THEN 1 END) AS MERSALG_TIL_BASIS, " +
						  	  "COUNT(CASE WHEN \"Delaktivitet\" = 'Kontakt f�r opsigelse' THEN 1 END) AS KONTAKT_F�R_OPSIGELSE, " +
						  	  "COUNT(CASE WHEN \"Delaktivitet\" = 'Kontakt f�r opsigelse' AND \"Faktisk afsluttet dato\" <= \"Senest d\" THEN 1 END) AS KONTAKT_F�R_OPSIGELSE_IND " +
						  "FROM " +
						  	  "KS_DRIFT.DIAN_AKT_AFS_KS " +
						  "INNER JOIN " +
						  	  "KS_DRIFT.V_TEAM_DATO " +
						  "ON " +
						  	  "\"Faktisk afsluttet dato\" = DATO AND \"Ansvarlig Initialer\" = INITIALER " +
						  "WHERE " +
						  	  "DATO >= '" + DateConstant.startDate + "' AND DATO <= '" + DateConstant.endDate + "' " +
						  "GROUP BY " +
						  	  "TO_CHAR(DATO,'MM'), " +
						  	  "TEAM_ID";
		
		ResultSet rs = conn.Hent_tabel(sqlQuery);
	
		while(rs.next())
			dialogActivityData.add(new DialogActivityData(EMonth.getFromMonthNumber(rs.getInt("MONTH")), 
														  rs.getInt("TEAM_ID"), 
														  rs.getInt("EMAIL_INDG�ENDE"),
														  rs.getInt("AKTIVITETER_INDEN"), 
														  rs.getInt("AKTIVITETER_OVER"),
														  rs.getInt("MERSALG_TIL_BASIS"),
														  rs.getInt("KONTAKT_F�R_OPSIGELSE"),
														  rs.getInt("KONTAKT_F�R_OPSIGELSE_IND")));
		
		return dialogActivityData;
	}
}
