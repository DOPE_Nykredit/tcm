package com.nykredit.kundeservice.tcm.datatypes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nykredit.kundeservice.data.CTIRConnection;
import com.nykredit.kundeservice.tcm.DateConstant;
import com.nykredit.kundeservice.tcm.EMonth;

public class WebdeskData {

	private EMonth month;
	
	private int calls = 0;
	private int answered = 0;
	private int answeredWithin30Sec;
	private int answeredByMailservice = 0;
	private int answeredByBasis = 0;
	private int answeredByBasis1 = 0;
	private int answeredByBasis2 = 0;
	private int answeredByBasis3 = 0;
	private int answeredByBasis4 = 0;
	private int answeredByBasis5 = 0;
	private int answeredByBasis6 = 0;

	public EMonth getMonth(){
		return this.month;
	}
		
	public int getCalls(){
		return this.calls;
	}
	public int getAnswered(){
		return this.answered;
	}
	public int getAnsweredWithin30Sec(){
		return this.answeredWithin30Sec;
	}
	public int getAnsweredByMailservice(){
		return this.answeredByMailservice;
	}
	public int getAnsweredByBasis(){
		return this.answeredByBasis;
	}
	public int getAnsweredByBasis1(){
		return this.answeredByBasis1;
	}
	public int getAnsweredByBasis2(){
		return this.answeredByBasis2;
	}
	public int getAnsweredByBasis3(){
		return this.answeredByBasis3;
	}
	public int getAnsweredByBasis4(){
		return this.answeredByBasis4;
	}
	public int getAnsweredByBasis5(){
		return this.answeredByBasis5;
	}
	public int getAnsweredByBasis6(){
		return this.answeredByBasis6;
	}
	
	public WebdeskData(EMonth month, int calls, int answered, int answeredWithin30Sec, int answeredByMailservice, int answeredByBasis, int answeredByBasis1,
			int answeredByBasis2, int answeredByBasis3, int answeredByBasis4, int answeredByBasis5,int answeredByBasis6){
		this.month = month;
		this.answeredByBasis = answeredByBasis;
		this.calls = calls;
		this.answered = answered;
		this.answeredWithin30Sec = answeredWithin30Sec;
		this.answeredByMailservice = answeredByMailservice;
		this.answeredByBasis1 = answeredByBasis1;
		this.answeredByBasis2 = answeredByBasis2;
		this.answeredByBasis3 = answeredByBasis3;
		this.answeredByBasis4 = answeredByBasis4;
		this.answeredByBasis5 = answeredByBasis5;
		this.answeredByBasis6 = answeredByBasis6;

	}
		
	public static ArrayList<WebdeskData> getWebdeskData(CTIRConnection conn) throws SQLException{
		ArrayList<WebdeskData> webdeskData = new ArrayList<WebdeskData>();
		
		String sqlQuery = "SELECT " + 
						  	  "TO_CHAR(DATETIME ,'MM') AS MONTH, " +
						  	  "COUNT(*) AS CALLS, " +
						  	  "COUNT(CASE WHEN INITIALS is not null THEN 1 END) AS ANSWERED, " +
						  	  "COUNT(CASE WHEN INITIALS is not null AND RESPONSE_TIME BETWEEN 0 AND 30 THEN 1 END) AS ANSWERED_WITHIN_30_SEC, " +
						  	  "COUNT(CASE WHEN vt.TEAM IN('B1') THEN 1 END) AS ANSWERED_BY_BASIS1, "+
						  	  "COUNT(CASE WHEN vt.TEAM IN('B2') THEN 1 END) AS ANSWERED_BY_BASIS2, "+
						  	  "COUNT(CASE WHEN vt.TEAM IN('B3') THEN 1 END) AS ANSWERED_BY_BASIS3, "+
						  	  "COUNT(CASE WHEN vt.TEAM IN('B4') THEN 1 END) AS ANSWERED_BY_BASIS4, "+
						  	  "COUNT(CASE WHEN vt.TEAM IN('B5') THEN 1 END) AS ANSWERED_BY_BASIS5, "+
						  	  "COUNT(CASE WHEN vt.TEAM IN('B6') THEN 1 END) AS ANSWERED_BY_BASIS6, "+
						  	  "COUNT(CASE WHEN vt.TEAM IN('B1','B2','B3','B4','B5','B6') THEN 1 END) AS ANSWERED_BY_BASIS, "+
						  	  "COUNT(CASE WHEN vt.TEAM = 'MS' THEN 1 END) AS ANSWERED_BY_MAILSERVICE " +
						  "FROM " + 
						  	  "KS_DRIFT.V_WEBDESK_SERVICECENTER web " + 
						  "LEFT JOIN " + 
						  	  "KS_DRIFT.V_TEAM_DATO vt ON web.INITIALS = vt.INITIALER AND trunc(web.DATETIME) = vt.DATO " +
						  "WHERE " + 
						  	  "DATETIME BETWEEN '" + DateConstant.startDate + "' AND '" + DateConstant.endDate + "' " + 
						  "GROUP BY " + 
						  	  "TO_CHAR(DATETIME, 'MM')"; 
		
		ResultSet rs = conn.Hent_tabel(sqlQuery);
		System.out.println("sql "+sqlQuery);
		while(rs.next())
			webdeskData.add(new WebdeskData(EMonth.getFromMonthNumber(rs.getInt("MONTH")),
			    							rs.getInt("CALLS"),
											rs.getInt("ANSWERED"),
											rs.getInt("ANSWERED_WITHIN_30_SEC"),
											rs.getInt("ANSWERED_BY_MAILSERVICE"),
											rs.getInt("ANSWERED_BY_BASIS"),
											rs.getInt("ANSWERED_BY_BASIS1"),
											rs.getInt("ANSWERED_BY_BASIS2"),
											rs.getInt("ANSWERED_BY_BASIS3"),
											rs.getInt("ANSWERED_BY_BASIS4"),
											rs.getInt("ANSWERED_BY_BASIS5"),
											rs.getInt("ANSWERED_BY_BASIS6")

											
					
					
					));
		
		return webdeskData;
	}
}
