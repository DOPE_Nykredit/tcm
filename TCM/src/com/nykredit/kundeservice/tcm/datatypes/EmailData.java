package com.nykredit.kundeservice.tcm.datatypes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nykredit.kundeservice.data.CTIRConnection;
import com.nykredit.kundeservice.tcm.DateConstant;
import com.nykredit.kundeservice.tcm.EMonth;

public class EmailData {

	private EMonth month;
	
	public enum EmailGroups{
		Kampagne,
		Mailservice
	}
	
//	private int activitiesCompletedWithinDeadline = 0;
//	private int activitiesExceededDeadline = 0;
	private int TotalEmailsCompletedWithinDeadline = 0;
	private int TotalEmailsExceededDeadline = 0;
	private int campaignEmailsCompletedWithinDeadline = 0;
	private int campaignEmailsExceededDeadline = 0;
	private int mailServiceEmailsReceived = 0;
	private int mailServiceEmailsCompletedWithinDeadline = 0;
	
	public EMonth getMonth(){
		return this.month;
	}
	
//	public int getActivitiesCompletedWithinDeadline(){
//		return this.activitiesCompletedWithinDeadline;
//	}
//	public int getActivitiesExceededDeadline(){
//		return this.activitiesExceededDeadline;
//	}
//	public int getTotalActivities(){
//		return this.activitiesCompletedWithinDeadline + this.activitiesExceededDeadline;
//	}
	
	public int getTotalEmailsCompletedWithinDeadline(){
		return this.TotalEmailsCompletedWithinDeadline;
	}
	public int getTotalEmailsExceededDeadline(){
		return this.TotalEmailsExceededDeadline;
	}
	public int getTotalEmailsReceived(){
		return this.TotalEmailsCompletedWithinDeadline + this.TotalEmailsExceededDeadline;
	}
	
	public int getCampaignEmailsCompletedWithinDeadline(){
		return this.campaignEmailsCompletedWithinDeadline;
	}
	public int getCampaignEmailsExceededDeadline(){
		return this.campaignEmailsExceededDeadline;
	}
	public int getTotalCampaignEmailsReceived(){
		return this.campaignEmailsCompletedWithinDeadline + this.campaignEmailsExceededDeadline;
	}
	
	public int getMailServiceEmailsCompletedWithinDeadline(){
		return this.mailServiceEmailsCompletedWithinDeadline;
	}
	public int getMailServiceEmailsReceived(){
		return this.mailServiceEmailsReceived;
	}
	
	public EmailData(EMonth month,
//					 int activitiesCompletedWithinDeadline,
//					 int activitiesExceededDeadline,
					 int TotalEmailsCompletedWithinDeadline,
					 int TotalEmailsExceededDeadline,
					 int campaignEmailsCompletedWithinDeadline,
					 int campaignEmailsExceededDeadline,
					 int mailServiceEmailsReceived,
					 int mailServiceEmailsCompletedWithinDeadline){
		this.month = month;
//		this.activitiesCompletedWithinDeadline = activitiesCompletedWithinDeadline;
//		this.activitiesExceededDeadline = activitiesExceededDeadline;
		this.TotalEmailsCompletedWithinDeadline = TotalEmailsCompletedWithinDeadline;
		this.TotalEmailsExceededDeadline = TotalEmailsExceededDeadline;
		this.campaignEmailsCompletedWithinDeadline = campaignEmailsCompletedWithinDeadline;
		this.campaignEmailsExceededDeadline = campaignEmailsExceededDeadline;
		this.mailServiceEmailsCompletedWithinDeadline = mailServiceEmailsCompletedWithinDeadline;
		this.mailServiceEmailsReceived = mailServiceEmailsReceived;
	}
	
	public static ArrayList<EmailData> getEmailData(CTIRConnection conn) throws SQLException{
		ArrayList<EmailData> emailData = new ArrayList<EmailData>();
		
		String sqlQuery = "SELECT " +
						  	"TO_CHAR(DATO,'MM') AS MONTH, " +
						  	"SUM(AKT_INDENFOR) AS AKT_INDENFOR, " +
						  	"SUM(AKT_OVER) AS AKT_OVER, " +
						  	"SUM(B9_IND+BEH_IND)-SUM(B9_FEJL+BEH_FEJL) AS EMAIL_INDENFOR, " + 
						  	"SUM(B9_FEJL+BEH_FEJL) AS EMAIL_OVERSKREDET, " + 
						  	"SUM(KAMP_IND)-SUM(KAMP_FEJL) AS KAMPAGNE_INDENFOR, " +
						  	"SUM(KAMP_FEJL) AS KAMPAGNE_OVERSKREDET, " +
						  	"SUM(B9_IND) AS B9INDG�ENDE, " +
						  	"SUM(B9_IND)-SUM(B9_FEJL) AS B9INDENFORTIDSFRIST " +			
						  "FROM " +
						  	"KS_DRIFT.EMAIL_STAT " +
						  "WHERE " +
						  	"DATO>='" + DateConstant.startDate + "' AND " +
						  	"DATO<='" + DateConstant.endDate + "' " +
						  "GROUP BY " +
						  	"TO_CHAR(DATO,'MM') " +
						  "ORDER BY " +
						  	"MONTH";

		ResultSet rs = conn.Hent_tabel(sqlQuery);
		
		while(rs.next())
			emailData.add(new EmailData(EMonth.getFromMonthNumber(rs.getInt("MONTH")),
//										rs.getInt("AKT_INDENFOR"), 
//										rs.getInt("AKT_OVER"), 
										rs.getInt("EMAIL_INDENFOR"), 
										rs.getInt("EMAIL_OVERSKREDET"), 
										rs.getInt("KAMPAGNE_INDENFOR"), 
										rs.getInt("KAMPAGNE_OVERSKREDET"), 
										rs.getInt("B9INDG�ENDE"), 
									    rs.getInt("B9INDENFORTIDSFRIST")));
		
		return emailData;
	}
}
