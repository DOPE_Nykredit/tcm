package com.nykredit.kundeservice.tcm.datatypes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nykredit.kundeservice.data.CTIRConnection;
import com.nykredit.kundeservice.tcm.DateConstant;
import com.nykredit.kundeservice.tcm.EMonth;

public class CustomerSatisfactionData {

	private EMonth month;
	private int teamId;
	
	private int respondents = 0;
	private int verySatisfied = 0;
	
	public EMonth getMonth(){
		return this.month;
	}
	public int getTeamId(){
		return this.teamId;
	}
	
	public int getRespondents(){
		return this.respondents;
	}
	public int getVerySatisfied(){
		return this.verySatisfied;
	}
	
	public CustomerSatisfactionData(EMonth month, int teamId, int respondents, int verySatisfied){
		this.month = month;
		this.teamId = teamId;
		this.respondents = respondents;
		this.verySatisfied = verySatisfied;
	}
	
	public static ArrayList<CustomerSatisfactionData> getCustomerSatisfactionData(CTIRConnection conn) throws SQLException{
		ArrayList<CustomerSatisfactionData> customerSatisfactionData = new ArrayList<CustomerSatisfactionData>();
		
		String sqlQuery = "SELECT " +
							  "TO_CHAR(DATETIME,'MM') AS MONTH, " +
							  "vt.TEAM_ID, " +
							  "COUNT(*) AS RESPONDENTS, " + 
							  "COUNT(CASE WHEN gra.GRADE = 5 THEN 1 END) AS VERYSATISFIED " +
						  "FROM " +
						  	  "KS_DRIFT.CUST_SATISFACTION_RESPONSE res " +
						  "INNER JOIN " +
						  	  "KS_DRIFT.CUST_SATISFACTION_GRADE gra " +
						  "ON " +
						  	  "res.ID = gra.RESPONSE_ID " +
						  "INNER JOIN " +
						  	  "KS_DRIFT.V_TEAM_DATO vt " +
						  "ON " +
						  	  "res.FIRST_INITIALS = vt.INITIALER AND trunc(res.DATETIME) = vt.DATO " +
						  "WHERE " +
						  	  "vt.DATO BETWEEN '" + DateConstant.startDate + "' AND '" + DateConstant.endDate + "' AND " +
						  	  "gra.QUESTION_ID = 48 " +
						  "GROUP BY " +
						  	  "TO_CHAR(DATETIME,'MM'), " +
						  	  "vt.TEAM_ID";
		
		ResultSet rs = conn.Hent_tabel(sqlQuery);
		
		while(rs.next())
			customerSatisfactionData.add(new CustomerSatisfactionData(EMonth.getFromMonthNumber(rs.getInt("MONTH")),
																	  rs.getInt("TEAM_ID"),
																	  rs.getInt("RESPONDENTS"),
																	  rs.getInt("VERYSATISFIED")));
		
		return customerSatisfactionData;
	}
}