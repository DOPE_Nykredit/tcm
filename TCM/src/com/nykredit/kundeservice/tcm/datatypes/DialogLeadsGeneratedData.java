package com.nykredit.kundeservice.tcm.datatypes;

import com.nykredit.kundeservice.data.CTIRConnection;
import com.nykredit.kundeservice.tcm.DateConstant;
import com.nykredit.kundeservice.tcm.EMonth;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DialogLeadsGeneratedData {

	private EMonth month;
	private int teamId;
	
	private int leadsGenerated = 0;
	private int campaignLeads = 0;
	
	public EMonth getMonth(){
		return this.month;
	}
	public int getTeamId(){
		return this.teamId;
	}
	
	public int getLeadsGenerated(){
		return this.leadsGenerated;
	}
	public int getCampaignLeads(){
		return this.campaignLeads;
	}
	
	public DialogLeadsGeneratedData(EMonth month, int teamId, int leadsGenerated, int campaignLeads){
		this.month = month;
		this.teamId = teamId;
		this.leadsGenerated = leadsGenerated;
		this.campaignLeads = campaignLeads;
	}
	
	public static ArrayList<DialogLeadsGeneratedData> getDialogLeadsGeneratedData(CTIRConnection conn) throws SQLException{
		ArrayList<DialogLeadsGeneratedData> dialogLeadsGeneratedData = new ArrayList<DialogLeadsGeneratedData>();
		
		String sqlQuery = "SELECT " + 
						  	"TO_CHAR(DATO, 'MM') AS MONTH, " +
						  	"TEAM_ID, " +
						  	"COUNT(CASE WHEN \"Produkt\" IN ('Henvisning til Privat', 'Henvisning til Salgscentret', 'Henvisning til Erhverv', 'Henvisning til Direkte', 'Henvisning til Retail') THEN 1 END) AS HENVISNINGER, " +
						  	"COUNT(CASE WHEN \"Responsform\" = 'Internet/E-mail' THEN 1 END) AS KAMPAGNEPOTENTIALER " +
						  "FROM " +     
						  	"KS_DRIFT.DIAN_POT_OPR " +
						  "INNER JOIN " + 
						  	"KS_DRIFT.V_TEAM_DATO " + 
						  "ON " + 
						  	"trunc(\"Oprettet dato\") = DATO AND \"Oprettet af Initialer\" = INITIALER " +
						  "WHERE " +
						  	"DATO >= '" + DateConstant.startDate + "' AND DATO <= '" + DateConstant.endDate + "' " +
						  "GROUP BY " + 
						  	"TO_CHAR(DATO,'MM'), TEAM_ID " +
						  "ORDER BY " +
						  	"MONTH, " +
						  	"TEAM_ID";
		
		
		ResultSet rs = conn.Hent_tabel(sqlQuery);
		System.out.println(sqlQuery);
		while(rs.next())
			dialogLeadsGeneratedData.add(new DialogLeadsGeneratedData(EMonth.getFromMonthNumber(rs.getInt("MONTH")),
																	  rs.getInt("TEAM_ID"),
																	  rs.getInt("HENVISNINGER"),
																	  rs.getInt("KAMPAGNEPOTENTIALER")));

		return dialogLeadsGeneratedData;
	}
}
