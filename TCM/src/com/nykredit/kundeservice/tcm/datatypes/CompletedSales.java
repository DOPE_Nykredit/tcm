package com.nykredit.kundeservice.tcm.datatypes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nykredit.kundeservice.data.CTIRConnection;
import com.nykredit.kundeservice.tcm.DateConstant;
import com.nykredit.kundeservice.tcm.EMonth;

public class CompletedSales {

	private EMonth month;
	private int teamId;
	
	private int completedSales;
	
	public EMonth getMonth(){
		return this.month;
	}
	public int getTeamId(){
		return this.teamId;
	}
	
	public int getCompletedSales(){
		return this.completedSales;
	}
	
	public CompletedSales(EMonth month, int teamId, int completedSales){
		this.month = month;
		this.teamId = teamId;
		
		this.completedSales = completedSales;
	}
	
	public static ArrayList<CompletedSales> getCompletedSales(CTIRConnection conn) throws SQLException{
		ArrayList<CompletedSales> completedSales = new ArrayList<CompletedSales>();
		
		String sqlQuery = "SELECT " + 
				  			  "TO_CHAR(OPRETTET_DATO_TID, 'MM') AS MONTH, " +
				  			  "vt.TEAM_ID, " +
				  			  "COUNT(*) AS GENNEMFØRTSALG " +
				  		  "FROM (" + 
				  		  	  "SELECT " +  
				  		  	  	  "TRUNC(opr.\"Oprettet dato/tid\") AS OPRETTET_DATO_TID, " +  
				  		  	  	  "opr.\"Oprettet af Initialer\" AS OPRETTET_AF, " +
				  		  	  	  "opr.\"Produkt\" AS HENVISNING, " + 
				  		  	  	  "opr.\"CPR-/CVR-nummer\" AS KUNDE_ID " +
				  		  	  "FROM " + 
				  		  	  	  "KS_DRIFT.DIAN_POT_07 pot " +
				  		  	  "RIGHT OUTER JOIN " + 
				  		  	  	  "KS_DRIFT.DIAN_POT_OPR opr ON pot.\"CPR-/CVR-nummer\" = opr.\"CPR-/CVR-nummer\" " +
				  		  	  "RIGHT OUTER JOIN " + 
				  		  	  	  "KS_DRIFT.V_TEAM_DATO vt ON vt.DATO = opr.\"Oprettet dato\" AND vt.INITIALER = opr.\"Oprettet af Initialer\" " + 
				  		  	  "WHERE " + 
				  		  	  	  "opr.\"Produkt\" in ('Henvisning til Direkte','Henvisning til Privat','Henvisning til Retail','Henvisning til Salgscentret') AND " + 
				  		  	  	  "pot.\"Oprettet af Region\" not in ('Kundeservice','Servicecenter') AND " + 
				  		  	  	  "pot.\"Produkt\" not in ('Henvisning til Direkte','Henvisning til Privat','Henvisning til Retail','Henvisning til Salgscentret') AND " +
				  		  	  	  "opr.\"Oprettet dato\" BETWEEN '2013-01-01' AND '2013-03-31' AND " +
				  		  	  	  "pot.\"Dato salgstrin\" - opr.\"Oprettet dato\" BETWEEN 0 AND 45 " +
				  		  	  "GROUP BY " +
				  		  	  	  "TRUNC(opr.\"Oprettet dato/tid\"), " +
				  		  	  	  "opr.\"Oprettet af Initialer\", " +
				  		  	  	  "opr.\"Produkt\", " +
				  		  	  	  "opr.\"CPR-/CVR-nummer\") pot " +
				  		  "INNER JOIN " +
				  		  	  "KS_DRIFT.V_TEAM_DATO vt ON vt.DATO = trunc(pot.OPRETTET_DATO_TID) AND vt.INITIALER = pot.OPRETTET_AF " +
				  		  "GROUP BY " +
				  		  	  "TO_CHAR(OPRETTET_DATO_TID, 'MM'), " +
				  		  	  "vt.TEAM_ID";
		
		ResultSet rs = conn.Hent_tabel(sqlQuery);
		
		while(rs.next())
			completedSales.add(new CompletedSales(EMonth.getFromMonthNumber(rs.getInt("MONTH")),
										   	 	  rs.getInt("TEAM_ID"),
										   	 	  rs.getInt("GENNEMFØRTSALG")));
		
		sqlQuery = "SELECT " +
					   "TO_CHAR(\"Oprettet dato\", 'MM') AS MONTH, " +
					   "vt.TEAM_ID, " +
					   "COUNT(*) AS GENNEMFØRTSALG " +
				   "FROM " +
				   	   "KS_DRIFT.DIAN_POT_07 pot " + 
				   "INNER JOIN " +
				   	   "KS_DRIFT.V_TEAM_DATO vt ON pot.\"Oprettet af Initialer\" = vt.INITIALER AND trunc(pot.\"Oprettet dato\") = vt.DATO " +
				   "WHERE " +
				   	   "\"Ansvarlig Region\" <> 'Servicecenter' AND " +
				   	   "NVL(\"Responsform\", 'X') <> 'Internet/E-mail' AND " +
				   	   "\"Dato salgstrin\" - \"Oprettet dato\" <= 45 AND " +
				   	   " \"Oprettet dato\" >= '2013-04-01' " +
				   	   "  AND \"Produkt\" Not in ('Mødeservice') "+
				   	   "AND \"Oprettet dato\" < '" + DateConstant.endDate + "' " +
				   "GROUP BY " +
				   	   "TO_CHAR(\"Oprettet dato\", 'MM'), vt.TEAM_ID";
		System.out.println("2"+sqlQuery);
		rs = conn.Hent_tabel(sqlQuery);
		
		while(rs.next())
			completedSales.add(new CompletedSales(EMonth.getFromMonthNumber(rs.getInt("MONTH")),
			   	 	  														rs.getInt("TEAM_ID"),
			   	 	  														rs.getInt("GENNEMFØRTSALG")));
		
		return completedSales;
	}
}