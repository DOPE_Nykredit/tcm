package com.nykredit.kundeservice.tcm.datatypes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nykredit.kundeservice.data.CTIRConnection;
import com.nykredit.kundeservice.tcm.DateConstant;
import com.nykredit.kundeservice.tcm.EMonth;

public class CTIData {

	private EMonth month;
	private int teamId;
	private double email = 0;
	private int indgaendeKald = 0;
	private double klar = 0;
	private double korrigeretLogin = 0;
	private double ovrigTid = 0;
	private double taler = 0;
	private double webdesk = 0;
	private double m�de = 0;
	private double uddannelse = 0;
	
	public EMonth getMonth(){
		return this.month;
	}
	public int getTeamId(){
		return this.teamId;
	}
	public double getEmail() {
		return this.email;
	}
	public int getIndgaendeKald() {
		return this.indgaendeKald;
	}
	public double getKlar() {
		return this.klar;
	}
	public double getKorrigeretLogin() {
		return this.korrigeretLogin;
	}
	public double getOvrigTid() {
		return this.ovrigTid;
	}
	public double getTaler() {
		return this.taler;
	}
	public double getWebdesk() {
		return this.webdesk;
	}
	public double getM�de(){
		return m�de;
	}
	public double getUddannelse(){
		return this.uddannelse;
	}
	
	public CTIData(EMonth month,
				   int teamId,
				   double email, 
				   int indgaendeKald, 
				   double klar,
				   double korrigeretLogin,
				   double ovrigTid,
				   double taler,
				   double webdesk,
				   double m�de,
				   double uddannelse){
		this.month = month;
		this.teamId = teamId;
		this.email = email;
		this.indgaendeKald = indgaendeKald;
		this.klar = klar;
		this.korrigeretLogin = korrigeretLogin;
		this.ovrigTid = ovrigTid;
		this.taler = taler;
		this.webdesk = webdesk;
		this.m�de = m�de;
		this.uddannelse = uddannelse;
	}
	
	public static ArrayList<CTIData> getCTIData(CTIRConnection conn) throws SQLException{
		ArrayList<CTIData> ctiData = new ArrayList<CTIData>();
		
		String sqlQuery = "SELECT " +
						  	"TO_CHAR(DATO,'MM') AS MONTH, " +
						  	"TEAM_ID, " +
						  	"SUM(\"Ind kald\") AS INDKALD, " +
						  	"SUM(\"Korrigeret\") AS KORRIGERET, " +
						  	"SUM(\"Klar\") AS KLAR, " +
						  	"SUM(\"Taler\") AS TALER, " +
						  	"SUM(\"�vrig tid\") AS �VRIGTID, " +
						  	"SUM(\"Webdesk\") AS WEBDESK, " +
						  	"SUM(\"E-mail\") AS EMAIL, " +
						  	"SUM(\"M�de\") AS M�DE, " +
						  	"SUM(\"Uddannelse\") AS UDDANNELSE " +
						  "FROM	" +
						  	"KS_DRIFT.PERO_AGENT_CBR CBR " +
						  "INNER JOIN " +
						  	"KS_DRIFT.V_TEAM_DATO " +
						  "ON " +
						  	"\"Initialer\"=INITIALER AND " +
						  	"\"Dato\"=DATO " +
						  "WHERE " +
						  	"DATO>='" + DateConstant.startDate + "' AND	" +
						  	"DATO<='" + DateConstant.endDate + "' " +
						  "GROUP BY	" +
						  	"TO_CHAR(DATO,'MM'), " +
						  	"TEAM_ID " +
						  "ORDER BY " +
						  	"MONTH,	" +
						  	"TEAM_ID";
		
		ResultSet rs = conn.Hent_tabel(sqlQuery);
		
		while(rs.next())
			ctiData.add(new CTIData(EMonth.getFromMonthNumber(rs.getInt("MONTH")), 
									rs.getInt("TEAM_ID"), 
									rs.getDouble("EMAIL"), 
									rs.getInt("INDKALD"), 
									rs.getDouble("KLAR"), 
									rs.getDouble("KORRIGERET"), 
									rs.getDouble("�VRIGTID"), 
									rs.getDouble("TALER"), 
									rs.getDouble("WEBDESK"),
									rs.getDouble("M�DE"),
									rs.getDouble("UDDANNELSE")));
		
		return ctiData;
	}

}