package com.nykredit.kundeservice.tcm.datatypes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nykredit.kundeservice.data.CTIRConnection;
import com.nykredit.kundeservice.tcm.DateConstant;
import com.nykredit.kundeservice.tcm.EMonth;

public class PhoneQueueData {
	
	public enum Queue{
		KunderOgStabe,
		Erhverv,
		Hotline,
		Andre
	}
	
	private EMonth month;
	
	private int kunderOgStabeCalls = 0;
	private int kunderOgStabeCallsAnsweredWithin25Sec = 0;
	private int erhvervCalls = 0;
	private int erhvervCallsAnsweredWithin25Sec = 0;
	private int hotlineCalls = 0;
	private int hotlineCallsAnsweredWithin25Sec = 0;
	private int otherCalls = 0;
	private int otherCallsAnsweredWithin25Sec = 0;
	
	public EMonth getMonth(){
		return this.month;
	}
	
	public int getKunderOgStabeCalls(){
		return this.kunderOgStabeCalls;
	}
	public int getKundeOgStabeCallsAnsweredWithin25Sec(){
		return this.kunderOgStabeCallsAnsweredWithin25Sec;
	}
	public int getErhvervCalls(){
		return this.erhvervCalls;
	}
	public int getErhvervCallsAnsweredWithin25Sec(){
		return this.erhvervCallsAnsweredWithin25Sec;
	}
	public int getHotlineCalls(){
		return this.hotlineCalls;
	}
	public int getHotlineCallsAnsweredWithin25Sec(){
		return this.hotlineCallsAnsweredWithin25Sec;
	}
	public int getOtherCalls(){
		return this.otherCalls;
	}
	public int getOtherCallsAnsweredWithin25Sec(){
		return this.otherCallsAnsweredWithin25Sec;
	}
	
	public int getTotalCalls(){
		return (this.kunderOgStabeCalls + this.erhvervCalls + this.hotlineCalls + this.otherCalls);
	}
	public int getTotalCallsAnsweredWithin25Sec(){
		return (this.kunderOgStabeCallsAnsweredWithin25Sec +
				this.erhvervCallsAnsweredWithin25Sec + 
				this.hotlineCallsAnsweredWithin25Sec +
				this.otherCallsAnsweredWithin25Sec);
	}
	
	public PhoneQueueData(EMonth month,
						  int kunderOgStabeCalls, 
						  int kunderOgStabeCallsAnsweredWithin25Sec,
						  int erhvervCalls,
						  int erhvervCallsAnsweredWithin25Sec,
						  int hotlineCalls,
						  int hotlineCallsAnsweredWithin25Sec,
						  int otherCalls,
						  int otherCallsAnsweredWithin25Sec){
		this.month = month;
		this.kunderOgStabeCalls = kunderOgStabeCalls;
		this.kunderOgStabeCallsAnsweredWithin25Sec = kunderOgStabeCallsAnsweredWithin25Sec;
		this.erhvervCalls = erhvervCalls;
		this.erhvervCallsAnsweredWithin25Sec = erhvervCallsAnsweredWithin25Sec;
		this.hotlineCalls = hotlineCalls;
		this.hotlineCallsAnsweredWithin25Sec = hotlineCallsAnsweredWithin25Sec;
		this.otherCalls = otherCalls;
		this.otherCallsAnsweredWithin25Sec = otherCallsAnsweredWithin25Sec;
	}
	
	public static ArrayList<PhoneQueueData> getPhoneQueueData(CTIRConnection conn) throws SQLException{
		ArrayList<PhoneQueueData> phoneQueueData = new ArrayList<PhoneQueueData>();
		
		String sqlQuery = "SELECT " +
						  	"TO_CHAR(TIDSPUNKT,'MM') AS MONTH, " +
						  	"SUM(CASE WHEN QUEUE = 'Hotline' THEN ANTAL_KALD END) AS HOTLINE_KALD, " +
						  	"SUM(CASE WHEN QUEUE = 'Hotline' THEN BESVARET_25_SEK END) AS HOTLINE_BESV_25_SEK, " + 
						  	"SUM(CASE WHEN QUEUE IN ('Kunder', 'Stabe') THEN ANTAL_KALD END) AS KUNDER_STABE_KALD, " +
						  	"SUM(CASE WHEN QUEUE IN ('Kunder', 'Stabe') THEN BESVARET_25_SEK END) AS KUNDER_STABE_BESV_25_SEK, " +
						  	"SUM(CASE WHEN QUEUE IN ('Erhverv', 'ErhvervOverflow', 'ErhvervStorkunder') THEN ANTAL_KALD END) AS ERHVERV_KALD, " +
						  	"SUM(CASE WHEN QUEUE IN ('Erhverv', 'ErhvervOverflow', 'ErhvervStorkunder') THEN BESVARET_25_SEK END) AS ERHVERV_BESV_25_SEK, " +
						  	"SUM(CASE WHEN QUEUE NOT IN('Hotline', 'Kunder', 'Stabe', 'Erhverv', 'ErhvervOverflow', 'ErhvervStorkunder') THEN ANTAL_KALD END) AS ANDRE_KALD, " + 
						  	"SUM(CASE WHEN QUEUE NOT IN('Hotline', 'Kunder', 'Stabe', 'Erhverv', 'ErhvervOverflow', 'ErhvervStorkunder') THEN BESVARET_25_SEK END) AS ANDRE_BESVARET_25_SEK " + 
						  "FROM " +     
						  	"KS_DRIFT.PERO_NKM_K�_OVERSIGT " +
						  "WHERE " +    
						  	"TIDSPUNKT >= '" + DateConstant.startDate + "' AND " +      
						  	"TIDSPUNKT <= '" + DateConstant.endDate + "' " +
						  "GROUP BY " + 
						  	"TO_CHAR(TIDSPUNKT,'MM') " +
						  "ORDER BY " + 
						  	"MONTH";
		
		ResultSet rs = conn.Hent_tabel(sqlQuery);

		while(rs.next())
			phoneQueueData.add(new PhoneQueueData(EMonth.getFromMonthNumber(rs.getInt("MONTH")),
												  rs.getInt("KUNDER_STABE_KALD"), 
												  rs.getInt("KUNDER_STABE_BESV_25_SEK"), 
												  rs.getInt("ERHVERV_KALD"), 
												  rs.getInt("ERHVERV_BESV_25_SEK"), 
												  rs.getInt("HOTLINE_KALD"), 
												  rs.getInt("HOTLINE_BESV_25_SEK"), 
												  rs.getInt("ANDRE_KALD"), 
												  rs.getInt("ANDRE_BESVARET_25_SEK")));

		
		return phoneQueueData;
	}
}
