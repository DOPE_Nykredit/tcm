package com.nykredit.kundeservice.tcm.datatypes.calculations;

import java.util.ArrayList;

import com.nykredit.kundeservice.tcm.Formatter;

public class TelefonSvarprocent extends HaandteringsGrad {

	private Integer besvaretIndenfor25Sek;
	private Integer total;
	
	@Override
	public Integer getOpfyldt() {
		return this.besvaretIndenfor25Sek;
	}

	@Override
	public Integer getTotal() {
		return this.total;
	}

	@Override
	public String[] getToolTipInformation() {
		ArrayList<String> toolTipInformation = new ArrayList<String>();
		
		if(this.total != null)
			toolTipInformation.add("<b>Telefon k�</b><br>Besvaret (25 sek): " + Formatter.toNumberString(this.besvaretIndenfor25Sek) + "<br>Antal kald: " + Formatter.toNumberString(this.total));
		
		if(toolTipInformation.isEmpty())
			return null;
		else
			return toolTipInformation.toArray(new String[0]);
	}
	
	public TelefonSvarprocent(Integer besvaretIndenfor25Sek, Integer total){
		if(besvaretIndenfor25Sek == null ^ total == null)
			throw new IllegalArgumentException("Both besvaretIndenfor25Sek and total must contain value or be null.");
		
		this.besvaretIndenfor25Sek = besvaretIndenfor25Sek;
		this.total = total;
	}
}
