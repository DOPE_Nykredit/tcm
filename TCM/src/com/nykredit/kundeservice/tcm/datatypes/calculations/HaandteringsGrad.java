package com.nykredit.kundeservice.tcm.datatypes.calculations;

import com.nykredit.kundeservice.tcm.DateConstant;
import com.nykredit.kundeservice.tcm.EMonth;
import com.nykredit.kundeservice.tcm.Formatter;

public abstract class HaandteringsGrad extends Calculation {

	public abstract Integer getOpfyldt();
	public abstract Integer getTotal();
		
//	protected HåndteringsGrad(int håndteretIndenforTidsfrist, int totalAntal){
//		this.håndteretIndenforTidsfrist = håndteretIndenforTidsfrist;
//		this.totalAntal = totalAntal;
//	}
	
	public double getResult(){
		return this.calculateHåndteringsGrad();
	}
	
	protected Double calculateHåndteringsGrad(){
		if(this.getTotal() == null)
			return null;
		else
			if(this.getTotal() == 0)
				return 0.0;
			else
				return ((double) this.getOpfyldt() / (double) this.getTotal());
	}

	@Override
	public String getFormattedResult() {
		Double håndteringsGrad = this.calculateHåndteringsGrad();
		
		if(håndteringsGrad != null)
			return Formatter.toPercentString(this.getResult());
		else
			return "-";
	}
	
	public String getEstimat(double mål){
		if(this.getTotal() == null)
			return "-";
		else if(this.getTotal() == 0)
			return "0%";
		else{
			double estimeretÅrligTotalAntal = ((double) this.getTotal() / (double)EMonth.getNumberOfCompletedMonths()) * 12; 
			double estimeretResterendeÅrligAntal = estimeretÅrligTotalAntal - this.getTotal();
			double restOfYearNuminator = estimeretResterendeÅrligAntal * mål;
			return Formatter.toPercentString((this.getOpfyldt() + restOfYearNuminator) / estimeretÅrligTotalAntal);
		}
	}
	public String getKrav(double mål){
		double budgetDenominator;
		if(DateConstant.completedMonths == 0){
			budgetDenominator = (this.getTotal() / 13) * 12;

		}else{
			budgetDenominator = (this.getTotal() / DateConstant.completedMonths) * 12;
			
		}
		double budgetNuminator = budgetDenominator * mål;
		double restOfYearDenominator = budgetDenominator - this.getTotal();
		double demandNuminator = budgetNuminator - this.getOpfyldt();
		
		return Formatter.toPercentString(demandNuminator/restOfYearDenominator);
	}
}
