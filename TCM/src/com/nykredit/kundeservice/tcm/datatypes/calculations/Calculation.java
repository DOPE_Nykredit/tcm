package com.nykredit.kundeservice.tcm.datatypes.calculations;

public abstract class Calculation {

	public abstract String getFormattedResult();

	@Override
	public String toString() {
		return this.getFormattedResult();
	}
	
	public abstract String[] getToolTipInformation();
	
}
