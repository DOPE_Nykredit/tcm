package com.nykredit.kundeservice.tcm.datatypes.calculations;

import java.util.ArrayList;

import com.nykredit.kundeservice.tcm.Formatter;

public class OpgaverIndenforTidsfrist extends HaandteringsGrad {

	private Integer dialogAktiviteterIndenforTidsfrist;
	private Integer dialogAktiviteterTotal;
	
	private Integer supportOpgaverIndenforTidsfrist;
	private Integer supportOpgaverTotal;
	
	private Integer kampagneMailsIndenforTidsfrist;
	private Integer kampagneMailsTotal;

	@Override
	public Integer getOpfyldt() {
		Integer indenforTidsfrist = null;
		
		if(this.dialogAktiviteterIndenforTidsfrist != null)
			indenforTidsfrist = this.dialogAktiviteterIndenforTidsfrist;

		if(this.supportOpgaverIndenforTidsfrist != null)
			if(indenforTidsfrist == null)
				indenforTidsfrist = this.supportOpgaverIndenforTidsfrist;
			else
				indenforTidsfrist += this.supportOpgaverIndenforTidsfrist;
		
		if(this.kampagneMailsIndenforTidsfrist != null)
			if(indenforTidsfrist == null)
				indenforTidsfrist = this.kampagneMailsIndenforTidsfrist;
			else
				indenforTidsfrist += this.kampagneMailsIndenforTidsfrist;
		
		return indenforTidsfrist;
	}


	@Override
	public Integer getTotal() {
		Integer total = null;
		
		if(this.dialogAktiviteterTotal != null)
			total = this.dialogAktiviteterTotal;
		
		if(this.supportOpgaverTotal != null)
			if(total == null)
				total = this.supportOpgaverTotal;
			else
				total += this.supportOpgaverTotal;
		
		if(this.kampagneMailsTotal != null)
			if(total == null)
				total = this.kampagneMailsTotal;
			else
				total += this.kampagneMailsTotal;
		
		return total;
	}

	@Override
	public String[] getToolTipInformation() {
		ArrayList<String> toolTipInformation = new ArrayList<String>();
		
		if(this.dialogAktiviteterTotal != null)
			toolTipInformation.add("<b>Dialog aktiviteter</b><br>Indenfor tidsfrist: " + Formatter.toNumberString(this.dialogAktiviteterIndenforTidsfrist) + "<br>Lukkede aktiviteter: " + Formatter.toNumberString(this.dialogAktiviteterTotal));
			
		if(this.supportOpgaverTotal != null)
			toolTipInformation.add("<b>Support opgaver</b><br>Indenfor tidsfrist: " + Formatter.toNumberString(this.supportOpgaverIndenforTidsfrist) + "<br>Afsluttede: " + Formatter.toNumberString(this.supportOpgaverTotal));
		
		if(this.kampagneMailsTotal != null)
			toolTipInformation.add("<b>Kampagnemails</b><br>Indenfor tidsfrist: " + Formatter.toNumberString(this.kampagneMailsIndenforTidsfrist) + "<br>Afsluttede: " + Formatter.toNumberString(this.kampagneMailsTotal));		
		
		if(toolTipInformation.isEmpty())
			return null;
		else
			return toolTipInformation.toArray(new String[0]);
	}
	
	public OpgaverIndenforTidsfrist(Integer dialogAktiviteterIndenforTidsfrist,
									Integer dialogAktiviteterTotal){
		this(dialogAktiviteterIndenforTidsfrist, dialogAktiviteterTotal, 0, 0, 0, 0);
	}
	
	public OpgaverIndenforTidsfrist(Integer dialogAktiviteterIndenforTidsfrist,
									Integer dialogAktiviteterTotal,
									Integer supportOpgaverIndenforTidsfrist,
									Integer supportOpgaverTotal,
									Integer kampagneMailsIndenforTidsfrist,
									Integer kampagneMailsTotal){
		if(dialogAktiviteterIndenforTidsfrist == null ^ dialogAktiviteterTotal == null)
			throw new IllegalArgumentException("Both dialogAktiviteterIndenforTidsfrist and dialogAktiviteterTotal must contain value or be null.");
		
		if(supportOpgaverIndenforTidsfrist == null ^supportOpgaverTotal == null)
			throw new IllegalArgumentException("Both supportOpgaverIndenforTidsfrist and supportOpgaverTotal must contain value or be null.");
		
		if(kampagneMailsIndenforTidsfrist == null ^kampagneMailsTotal == null)
			throw new IllegalArgumentException("Both kampagneMailsIndenforTidsfrist and kampagneMailsTotal must contain value or be null.");		
		
		this.dialogAktiviteterIndenforTidsfrist = dialogAktiviteterIndenforTidsfrist;
		this.dialogAktiviteterTotal = dialogAktiviteterTotal;
		
		this.supportOpgaverIndenforTidsfrist = supportOpgaverIndenforTidsfrist;
		this.supportOpgaverTotal = supportOpgaverTotal;
		
		this.kampagneMailsIndenforTidsfrist = kampagneMailsIndenforTidsfrist;
		this.kampagneMailsTotal = kampagneMailsTotal;
	}
}