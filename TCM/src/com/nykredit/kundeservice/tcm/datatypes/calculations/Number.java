package com.nykredit.kundeservice.tcm.datatypes.calculations;

import com.nykredit.kundeservice.tcm.DateConstant;
import com.nykredit.kundeservice.tcm.Formatter;

public class Number extends Calculation {

	private Integer number;
	
	@Override
	public String getFormattedResult() {
		return Formatter.toNumberString(this.number);
	}

	public String getEstimate(int m�l){
		if(DateConstant.remainingMonths == 12){
			return Formatter.toNumberString((int)Math.round(this.number + ((double)m�l / 12) * 0));
		}else{
			return Formatter.toNumberString((int)Math.round(this.number + ((double)m�l / 12) * DateConstant.remainingMonths));
			
		}
	}
	public String getKrav(int m�l){
		return Formatter.toNumberString((int)(Math.round((double)(m�l - this.number) / (double)DateConstant.remainingMonths)));
	}
	
	@Override
	public String[] getToolTipInformation() {
		return null;
	}
	
	public Number(Integer number){
		this.number = number;
	}
}