package com.nykredit.kundeservice.tcm.datatypes.calculations;

import java.util.ArrayList;

import com.nykredit.kundeservice.tcm.Formatter;

public class EmailSvarprocent extends HaandteringsGrad {

	private Integer afsluttetIndenforTidsfrist;
	private Integer modtaget;
	
	@Override
	public Integer getOpfyldt() {
		return this.afsluttetIndenforTidsfrist;
	}

	@Override
	public Integer getTotal() {
		return this.modtaget;
	}
	
	@Override
	public String[] getToolTipInformation() {
		ArrayList<String> toolTipInformation = new ArrayList<String>();
		
		if(this.modtaget != null)
			toolTipInformation.add("<b>Dialog emails</b><br>Afsluttet indenfor tidsfrist: " + Formatter.toNumberString(this.afsluttetIndenforTidsfrist) + "<br>Modtaget: " + Formatter.toNumberString(this.modtaget));
		
		if(toolTipInformation.isEmpty())
			return null;
		else
			return toolTipInformation.toArray(new String[0]);
	}
	
	public EmailSvarprocent(Integer afsluttetIndenforTidsfrist, Integer modtaget){
		if(afsluttetIndenforTidsfrist == null ^ modtaget == null)
			throw new IllegalArgumentException("Both afsluttetIndenforTidsfrist and modtaget must contain value or be null.");
		
		this.afsluttetIndenforTidsfrist = afsluttetIndenforTidsfrist;
		this.modtaget = modtaget;
	}
}