package com.nykredit.kundeservice.tcm.datatypes.calculations;

import java.util.ArrayList;

import com.nykredit.kundeservice.tcm.DateConstant;
import com.nykredit.kundeservice.tcm.Formatter;

public class AHT extends Calculation {
		
	protected Integer handlingSeconds;
	protected Integer calls;
	
	public int getHandlingSeconds(){
		return this.handlingSeconds;
	}
	public int getCalls(){
		return this.calls;
	}
	
	public AHT(int handlingSeconds, int calls){
		this.handlingSeconds = handlingSeconds;
		this.calls = calls;
	}
	
	public int getResult(){
		if(this.calls == 0)
			return 0;
		else
			return this.handlingSeconds / this.calls;
	}
	
	public String getEstimat(int goal){
		if(this.calls == null)
			return "-";
		if(this.calls == 0)
			return "00:00";
		else{
			int ahtSeconds = this.handlingSeconds / this.calls;
			
			return Formatter.toTimeString((ahtSeconds * DateConstant.completedMonths + goal * DateConstant.remainingMonths) / 12);
		}
	}
	public String getKrav(int goal){
		return "-";
	}
	
	@Override
	public String toString() {
		return this.getFormattedResult();
	}
	@Override
	public String getFormattedResult() {
		return Formatter.toTimeString(this.getResult());
	}
	@Override
	public String[] getToolTipInformation() {
		ArrayList<String> toolTipInformation = new ArrayList<String>();
		
		if(this.calls != null)
			toolTipInformation.add("<b>AHT:</b><br>Minutter: " + Formatter.toNumberString(this.handlingSeconds / 60) + "<br>Kald: " + Formatter.toNumberString(this.calls));
		
		if(toolTipInformation.isEmpty())
			return null;
		else
			return toolTipInformation.toArray(new String[0]);
	}
}
