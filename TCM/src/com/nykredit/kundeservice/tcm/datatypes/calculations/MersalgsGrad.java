package com.nykredit.kundeservice.tcm.datatypes.calculations;

import java.util.ArrayList;

import com.nykredit.kundeservice.tcm.Formatter;

public class MersalgsGrad extends HaandteringsGrad {

	private Integer mersalg;
	private Integer forsikringsSalg;
	
	@Override
	public Integer getOpfyldt() {
		return this.mersalg;
	}

	@Override
	public Integer getTotal() {
		return this.forsikringsSalg;
	}

	@Override
	public String[] getToolTipInformation() {
		ArrayList<String> toolTipInformation = new ArrayList<String>();
		
		if(this.forsikringsSalg != null)
			toolTipInformation.add("<b>Dialog salg:</b><br>Mersalg: " + Formatter.toNumberString(this.mersalg) + "<br>Forsikringssalg: " + Formatter.toNumberString(this.forsikringsSalg));
		
		if(toolTipInformation.isEmpty())
			return null;
		else
			return toolTipInformation.toArray(new String[0]);
	}
	
	public MersalgsGrad(Integer mersalg, Integer forskringsSalg){
		if(this.mersalg == null ^ this.forsikringsSalg == null)
			throw new IllegalArgumentException("Both mersalg and forsikringssalg must contain value or be null.");
		
		this.mersalg = mersalg;
		this.forsikringsSalg = forskringsSalg;
	}
}
