package com.nykredit.kundeservice.tcm.datatypes.calculations;

import java.util.ArrayList;

import com.nykredit.kundeservice.tcm.Formatter;

public class Kundetilfredshed extends HaandteringsGrad {

	private Integer telefoniskMegetTilfreds;
	private Integer telefoniskSvar;
	
	private Integer emailTilfreds;
	private Integer emailSvar;

	@Override
	public Integer getOpfyldt() {
		Integer indenforTidsfrist = null;
		
		if(this.telefoniskMegetTilfreds != null)
			indenforTidsfrist = this.telefoniskMegetTilfreds;

		if(this.emailTilfreds != null)
			if(indenforTidsfrist == null)
				indenforTidsfrist = this.emailTilfreds;
			else
				indenforTidsfrist += this.emailTilfreds;
		
		return indenforTidsfrist;
	}


	@Override
	public Integer getTotal() {
		Integer total = null;
		
		if(this.telefoniskSvar != null)
			total = this.telefoniskSvar;
		
		if(this.emailSvar != null)
			if(total == null)
				total = this.emailSvar;
			else
				total += this.emailSvar;
		
		return total;
	}

	@Override
	public String[] getToolTipInformation() {
		ArrayList<String> toolTipInformation = new ArrayList<String>();
		
		if(this.telefoniskSvar != null)
			toolTipInformation.add("<b>Telefonisk kundetilfredshed:</b><br>Meget tilfredse: " + Formatter.toNumberString(this.telefoniskMegetTilfreds) + "<br>Antal besvarelser: " + Formatter.toNumberString(this.telefoniskSvar));
			
		if(this.emailSvar != null)
			toolTipInformation.add("<b>Email kundetilfredshed</b><br>Tilfredse: " +this.emailTilfreds + "<br>Antal besvarelser: " +this.emailSvar);
		
		if(toolTipInformation.isEmpty())
			return null;
		else
			return toolTipInformation.toArray(new String[0]);
	}
	
	public Kundetilfredshed(Integer telefoniskMegetTilfreds,
							Integer telefoniskSvar,
							Integer emailTilfreds,
							Integer emailSvar){
		if(telefoniskMegetTilfreds == null ^ telefoniskSvar == null)
			throw new IllegalArgumentException("Both telefoniskMegetTilfreds and telefoniskSvar must contain value or be null.");
		
		if(emailTilfreds == null ^emailSvar == null)
			throw new IllegalArgumentException("Both emailTilfreds and emailSvar must contain value or be null.");
		
		this.telefoniskMegetTilfreds = telefoniskMegetTilfreds;
		this.telefoniskSvar = telefoniskSvar;
		
		this.emailTilfreds = emailTilfreds;
		this.emailSvar = emailSvar;
	}
}