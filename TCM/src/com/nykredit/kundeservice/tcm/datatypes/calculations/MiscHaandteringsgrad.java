package com.nykredit.kundeservice.tcm.datatypes.calculations;

public class MiscHaandteringsgrad extends HaandteringsGrad {

	private Integer succes;
	private Integer antal;
	
	@Override
	public Integer getOpfyldt() {
		return this.succes;
	}

	@Override
	public Integer getTotal() {
		return this.antal;
	}

	@Override
	public String[] getToolTipInformation() {
		return null;
	}
	
	public MiscHaandteringsgrad(Integer succes, Integer antal){
		if(succes == null ^ antal == null)
			throw new IllegalArgumentException("Both succes and antal must contain value or be null.");
		
		this.succes = succes;
		this.antal = antal;
	}
}