package com.nykredit.kundeservice.tcm.datatypes.calculations;

import java.util.ArrayList;

import com.nykredit.kundeservice.tcm.Formatter;

public class WebdeskSvarProcent extends HaandteringsGrad {

	private Integer besvaretIndenfor30Sek;
	private Integer total;
	
	@Override
	public Integer getOpfyldt() {
		return this.besvaretIndenfor30Sek;
	}

	@Override
	public Integer getTotal() {
		return this.total;
	}

	@Override
	public String[] getToolTipInformation() {
		ArrayList<String> toolTipInformation = new ArrayList<String>();
		
		if(this.total != null)
			toolTipInformation.add("<b>Webdesk:</b><br>Besvaret (30 sek): " + Formatter.toNumberString(this.besvaretIndenfor30Sek) + "<br>Antal kald: " + Formatter.toNumberString(this.total));
		
		if(toolTipInformation.isEmpty())
			return null;
		else
			return toolTipInformation.toArray(new String[0]);
	}
	
	public WebdeskSvarProcent(Integer besvaretIndenfor30Sek, Integer total){
		if(besvaretIndenfor30Sek == null ^ total == null)
			throw new IllegalArgumentException("Both besvaretIndenfor30Sek and total must contain value or be null.");
		
		this.besvaretIndenfor30Sek = besvaretIndenfor30Sek;
		this.total = total;
	}
}
