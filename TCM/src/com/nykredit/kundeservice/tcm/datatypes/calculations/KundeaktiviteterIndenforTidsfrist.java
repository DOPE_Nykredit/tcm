package com.nykredit.kundeservice.tcm.datatypes.calculations;

import java.util.ArrayList;

import com.nykredit.kundeservice.tcm.Formatter;

public class KundeaktiviteterIndenforTidsfrist extends HaandteringsGrad {

	private Integer aktiviteterIndenforTidsfrist;
	private Integer aktiviteterTotal;
	
	private Integer emailsIndenforTidsfrist;
	private Integer emailsTotal;
	
	private Integer supportOpgaverIndenforTidsfrist;
	private Integer supportOpgaverTotal;
	
	@Override
	public Integer getOpfyldt() {
		Integer aktiviteterIndenforTidsfrist = null;
		
		if(this.aktiviteterIndenforTidsfrist != null)
			aktiviteterIndenforTidsfrist = this.aktiviteterIndenforTidsfrist;
		
		if(this.emailsIndenforTidsfrist != null)
			if(aktiviteterIndenforTidsfrist == null)
				aktiviteterIndenforTidsfrist = (int)(this.emailsIndenforTidsfrist * (.95 / .9));
			else
				aktiviteterIndenforTidsfrist += (int)(this.emailsIndenforTidsfrist * (.95 / .9));
		
//		if(this.kampagneIndenforTidsfrist != null)
//			if(aktiviteterIndenforTidsfrist == null)
//				aktiviteterIndenforTidsfrist = (int)(this.kampagneIndenforTidsfrist * (.95/.98));
//			else
//				aktiviteterIndenforTidsfrist += (int)(this.kampagneIndenforTidsfrist * (.95/.98));
		
		if(this.supportOpgaverIndenforTidsfrist != null)
			if(aktiviteterIndenforTidsfrist == null)
				aktiviteterIndenforTidsfrist = this.supportOpgaverIndenforTidsfrist;
			else
				aktiviteterIndenforTidsfrist += this.supportOpgaverIndenforTidsfrist;

		return aktiviteterIndenforTidsfrist;
	}

	@Override
	public Integer getTotal() {
		Integer total = null;
		
		if(this.aktiviteterTotal != null)
			total = this.aktiviteterTotal;
		
		if(this.emailsTotal != null)
			if(total == null)
				total = this.emailsTotal;
			else
				total += this.emailsTotal;
		
//		if(this.kampagneTotal != null)
//			if(total == null)
//				total = this.kampagneTotal;
//			else
//				total += this.kampagneTotal;
		
		if(this.supportOpgaverTotal != null)
			if(total == null)
				total = this.supportOpgaverTotal;
			else
				total += this.supportOpgaverTotal;
		
		return total;
	}
	
	@Override
	public String[] getToolTipInformation() {
		ArrayList<String> toolTipInformation = new ArrayList<String>();
		
		if(this.aktiviteterTotal != null)
			toolTipInformation.add("<b>Dialog aktiviteter</b><br>Indenfor tidsfrist: " + Formatter.toNumberString(this.aktiviteterIndenforTidsfrist) + "<br>Afsluttede: " + Formatter.toNumberString(this.aktiviteterTotal));
			
		if(this.emailsTotal != null)
			toolTipInformation.add("<b>Emails</b><br>Indenfor tidsfrist: " + Formatter.toNumberString(this.emailsIndenforTidsfrist) + "<br>Afsluttede: " + Formatter.toNumberString(this.emailsTotal));

//		if(this.kampagneTotal != null)
//			toolTipInformation.add("<b>Kampagnemails</b><br>Indenfor tidsfrist: " + Formatter.toNumberString(this.kampagneIndenforTidsfrist) + "<br>Afsluttede: " + Formatter.toNumberString(this.kampagneTotal));
		
		if(this.supportOpgaverTotal != null)
			toolTipInformation.add("<b>Opgaver i support</b><br>Indenfor tidsfrist: " + Formatter.toNumberString(this.supportOpgaverIndenforTidsfrist) + "<br>Afsluttede: " + Formatter.toNumberString(this.supportOpgaverTotal));
		
		if(toolTipInformation.isEmpty())
			return null;
		else
			return toolTipInformation.toArray(new String[0]);
	}
	
	public KundeaktiviteterIndenforTidsfrist(Integer aktiviteterIndenforTidsfrist,
											 Integer aktiviteterTotal,
											 Integer emailsIndenforTidsfrist,
											 Integer emailsTotal,
//											 Integer kampagneIndenforTidsfrist,
//											 Integer kampagneTotal,
											 Integer supportOpgaverIndenforTidsfrist,
											 Integer supportOpgaverTotal){
		if(aktiviteterIndenforTidsfrist == null ^ aktiviteterTotal == null)
			throw new IllegalArgumentException("Both aktiviteterIndenforTidsfrist and aktiviteterTotal must contain value or be null.");
		
		if(emailsIndenforTidsfrist == null ^ emailsTotal == null)
			throw new IllegalArgumentException("Both emailsIndenforTidsfrist and emailsTotal must contain value or be null.");
		
//		if(kampagneIndenforTidsfrist == null ^ kampagneTotal == null)
//			throw new IllegalArgumentException("Both kampagneIndenforTidsfrist and kampagneTotal must contain value or be null.");
		
		if(supportOpgaverIndenforTidsfrist == null ^ supportOpgaverTotal == null)
			throw new IllegalArgumentException("Both supportOpgaverIndenforTidsfrist and supportOpgaverTotal must contain value or be null.");
		
		this.aktiviteterIndenforTidsfrist = aktiviteterIndenforTidsfrist;
		this.aktiviteterTotal = aktiviteterTotal;
		
		this.emailsIndenforTidsfrist = emailsIndenforTidsfrist;
		this.emailsTotal = emailsTotal;
		
//		this.kampagneIndenforTidsfrist = kampagneIndenforTidsfrist;
//		this.kampagneTotal = kampagneTotal;
		
		this.supportOpgaverIndenforTidsfrist = supportOpgaverIndenforTidsfrist;
		this.supportOpgaverTotal = supportOpgaverTotal;
	}

}
