package com.nykredit.kundeservice.tcm.datatypes.calculations;

import java.util.ArrayList;

import com.nykredit.kundeservice.tcm.Formatter;

public class DialogSvarProcent extends HaandteringsGrad {

	private Integer besvaretIndenfor30Sek;
	private Integer total;
	
	@Override
	public Integer getOpfyldt() {
		return this.besvaretIndenfor30Sek;
	}

	@Override
	public Integer getTotal() {
		return this.total;
	}

	@Override
	public String[] getToolTipInformation() {
		ArrayList<String> toolTipInformation = new ArrayList<String>();
		
		if(this.total != null)
			toolTipInformation.add("<b>Dialog:</b><br>Besvaret: " + Formatter.toNumberString(this.besvaretIndenfor30Sek) + "<br>Antal kald: " + Formatter.toNumberString(this.total));

		if(toolTipInformation.isEmpty())
			return null;
		else
			return toolTipInformation.toArray(new String[0]);
	}
	
	public DialogSvarProcent(Integer besvaretIndenfor30Sek, Integer total){
		this.besvaretIndenfor30Sek = besvaretIndenfor30Sek;
		this.total = total;
	}
}