package com.nykredit.kundeservice.tcm.datatypes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nykredit.kundeservice.data.CTIRConnection;
import com.nykredit.kundeservice.tcm.DateConstant;
import com.nykredit.kundeservice.tcm.EMonth;

public class CallbackData {

	private EMonth month;
	private int teamId;
	
	private int callbackCount;
	
	public EMonth getMonth(){
		return this.month;
	}
	public int getTeamId(){
		return this.teamId;
	}
	
	public int getCallbackCount(){
		return this.callbackCount;
	}
	
	public CallbackData(EMonth month, int teamId, int callbackCount){
		this.month = month;
		this.teamId = teamId;
		
		this.callbackCount = callbackCount;
	}
	
	public static ArrayList<CallbackData> getAllCallbacks(CTIRConnection conn) throws SQLException{
		ArrayList<CallbackData> callbacks = new ArrayList<CallbackData>();
		
		String sqlQuery = "SELECT " +
							  "TO_CHAR(vt.DATO, 'MM') AS MONTH, " +
				  			  "vt.TEAM_ID, " +
				  			  "COUNT(*) AS CALLBACKS " +
				  		  "FROM " +
				  		  	  "KS_DRIFT.NYK_SIEBEL_CALLBACK_AGENT_H_V cb " +
				  		  "INNER JOIN " +
				  		  	  "KS_DRIFT.V_TEAM_DATO vt ON cb.INITIALS = vt.INITIALER AND trunc(LAST_UPD) = vt.DATO " +
				  		  "WHERE " +
				  		  	  "DATO >= '" + DateConstant.startDate + "' AND DATO <= '" + DateConstant.endDate + "' " +
				  		  "GROUP BY " +
					  	  	  "TO_CHAR(vt.DATO, 'MM'), " +
					  	  	  "vt.TEAM_ID";
		
		ResultSet rs = conn.Hent_tabel(sqlQuery);
		
		while(rs.next())
			callbacks.add(new CallbackData(EMonth.getFromMonthNumber(rs.getInt("MONTH")), 
								  		   rs.getInt("TEAM_ID"),
								  		   rs.getInt("CALLBACKS")));
		
		return callbacks;
	}
}
