package com.nykredit.kundeservice.tcm.datatypes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nykredit.kundeservice.data.CTIRConnection;
import com.nykredit.kundeservice.tcm.DateConstant;
import com.nykredit.kundeservice.tcm.EMonth;

public class DialogLeadsRealizedData {

	private EMonth month;
	private int teamId;
	
	private int insuranceSold = 0;
	private int additionalInsuranceSales = 0;
	
	public EMonth getMonth(){
		return this.month;
	}
	public int getTeamId(){
		return this.teamId;
	}
	
	public int getInsuranceSold(){
		return this.insuranceSold;
	}
	public int getAdditionalInsuranceSales(){
		return this.additionalInsuranceSales;
	}	
	
	public DialogLeadsRealizedData(EMonth month, int teamId, int insuranceSold, int additionalInsuranceSales){
		this.month = month;
		this.teamId = teamId;
		
		this.insuranceSold = insuranceSold;
		this.additionalInsuranceSales = additionalInsuranceSales;
	}
	
	public static ArrayList<DialogLeadsRealizedData> getDialogLeadsRealizedData(CTIRConnection conn) throws SQLException{
		ArrayList<DialogLeadsRealizedData> dialogLeadsRealizedData = new ArrayList<DialogLeadsRealizedData>();
		
		String sqlQuery = "SELECT " +   
						  	"TO_CHAR(DATO, 'MM') AS MONTH, " +
						  	"TEAM_ID, " +
							"SUM(CASE WHEN \"Produktlinje\" = 'Forsikring' THEN \"Bruttobeløb\" END) AS FORSIKRINGSSALG, " +
							"SUM(CASE WHEN \"Produktlinje\" = 'Forsikring' AND \"Salgsanledning\" = 'Mersalg' THEN \"Bruttobeløb\" END) AS MERSALG " +
						  "FROM " +     
						  	"KS_DRIFT.DIAN_POT_07 " + 
						  "INNER JOIN " + 
						  	"KS_DRIFT.V_TEAM_DATO " + 
						  "ON " + 
						   	"\"Dato salgstrin\" = DATO AND \"Ansvarlig Initialer\" = INITIALER " +
						  "WHERE " +    
						   	"DATO >= '" + DateConstant.startDate + "' AND " +
						   	"DATO <= '" + DateConstant.endDate + "' " +
						  "GROUP BY " + 
						  	"TO_CHAR(DATO,'MM'), " +
						  	"TEAM_ID";
		
		ResultSet rs = conn.Hent_tabel(sqlQuery);
		
		while(rs.next())
			dialogLeadsRealizedData.add(new DialogLeadsRealizedData(EMonth.getFromMonthNumber(rs.getInt("MONTH")), 
																 	rs.getInt("TEAM_ID"), 
																 	rs.getInt("FORSIKRINGSSALG"),
																 	rs.getInt("MERSALG")));
		
		return dialogLeadsRealizedData;
	}
	
}
