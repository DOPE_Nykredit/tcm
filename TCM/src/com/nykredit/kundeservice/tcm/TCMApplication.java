package com.nykredit.kundeservice.tcm;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.table.DefaultTableModel;

import com.nykredit.kundeservice.data.CTIRConnection;
import com.nykredit.kundeservice.swing.NTableRenderer;
import com.nykredit.kundeservice.tcm.Team.TeamTableType;
import com.nykredit.kundeservice.tcm.tables.*;

public class TCMApplication implements ActionListener{

	private JFrame frame;
	
	private ArrayList<Team> teams;
	private ServiceCenterData centerData = new ServiceCenterData();
	
	JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
			run();
	}

	public static void run(){
		try {
			TCMApplication window = new TCMApplication();
			window.frame.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
	/**
	 * Create the application.
	 */
	public TCMApplication() {
		initialize();
		
		this.frame.setTitle("Centermål 2013");
		
		CTIRConnection conn = new CTIRConnection();
		System.out.println("Hej "+conn.LogWho);
		
		try {
			conn.Connect();
			
			this.teams = Team.getTeams(conn);
			this.centerData.loadData(conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		this.frame.setContentPane(this.tabbedPane);
		
		ArrayList<String> basisTabDepartments = new ArrayList<String>();
		basisTabDepartments.add("Basis");
		
		this.tabbedPane.addTab("Servicecenter", new JScrollPane(this.getServiceCenterTablePanel()));		
		this.tabbedPane.addTab("Basisservice", new JScrollPane(this.getTeamsTablePanel("Basis", basisTabDepartments)));
		this.tabbedPane.addTab("Digitale Services", new JScrollPane(this.getTeamsTablePanel("Digitale")));
		this.tabbedPane.addTab("Ekspeditionsservice", new JScrollPane(this.getTeamsTablePanel("Ekspedition")));
		this.tabbedPane.addTab("Administrationsservice", new JScrollPane(this.getAdministrationsTablePanel()));
		this.tabbedPane.addTab("Stabe", new JScrollPane(this.getStabeTablePanel()));
	}
	
	private JPanel getServiceCenterTablePanel(){
		JPanel tablePanel = new JPanel(new GridBagLayout());
		
		tablePanel.add(this.getCenterMålTableInScrollPane(),
					   this.getDefaultGridbagConstraints(1));
		
		tablePanel.add(this.getDescriptionPanel(),
					   this.getDefaultGridbagConstraints(2));	
				
		return tablePanel;
	}
	private JPanel getTeamsTablePanel(String departmentName){
		JPanel tablePanel = new JPanel(new GridBagLayout());
		
		ArrayList<Team> teams = Team.getTeamsByDepName(this.teams, departmentName);
		
		int gridY = 1;
		
		tablePanel.add(this.getDepartmentGoalTableInScrollPane(departmentName),
					   this.getDefaultGridbagConstraints(gridY));
		
		gridY += 1;
				
		for(Team t : teams){
			tablePanel.add(this.getTeamGoalTableInScrollPane(t),
						   this.getDefaultGridbagConstraints(gridY));
			
			gridY += 1;
		}
		
		tablePanel.add(this.getDescriptionPanel(),
					   this.getDefaultGridbagConstraints(gridY));
		
		return tablePanel;
	}
	private JPanel getTeamsTablePanel(String mainDepartmentName, ArrayList<String> includedDepartmentNames){
		JPanel tablePanel = new JPanel(new GridBagLayout());

		ArrayList<Team> teams = new ArrayList<Team>();
		
		for(String n : includedDepartmentNames)
			teams.addAll(Team.getTeamsByDepName(this.teams, n));
				
		int gridY = 1;
		
		tablePanel.add(this.getDepartmentGoalTableInScrollPane(mainDepartmentName),
					   this.getDefaultGridbagConstraints(gridY));
		
		gridY += 1;
				
		for(Team t : teams){
			tablePanel.add(this.getTeamGoalTableInScrollPane(t),
						   this.getDefaultGridbagConstraints(gridY));
			
			gridY += 1;
		}
		
		tablePanel.add(this.getDescriptionPanel(),
					   this.getDefaultGridbagConstraints(gridY));
		
		return tablePanel;
	}	
	private JPanel getStabeTablePanel(){
		JPanel tablePanel = new JPanel(new GridBagLayout());
		
		tablePanel.add(this.getDopeTableInScrollPane(),
					   this.getDefaultGridbagConstraints(1));
		
		tablePanel.add(this.getKundeaktiviteterTableInScrollPane(),
					   this.getDefaultGridbagConstraints(2));
		
		tablePanel.add(this.getSekretariatOgPlanlaegningTalbeInScrollPane(),
					   this.getDefaultGridbagConstraints(3));
		
		tablePanel.add(this.getProjektTableInScrollPane(),
					   this.getDefaultGridbagConstraints(4));
		
		return tablePanel;
	}
	
	private JPanel getAdministrationsTablePanel(){
		JPanel tablePanel = new JPanel(new GridBagLayout());
		
		TCMTable table = new TCMTable(new AdministrationsServiceTableModel(this.centerData), new AdministrationsServiceTableRenderer()); 
		JScrollPane tablePane = new JScrollPane(table);
				
		tablePane.setBorder(BorderFactory.createRaisedBevelBorder());
		tablePane.setPreferredSize(new Dimension((int)table.getPreferredSize().getWidth(),(int)table.getPreferredSize().getHeight()+30));
		tablePane.setMinimumSize(new Dimension((int)table.getPreferredSize().getWidth(),(int)table.getPreferredSize().getHeight()+30));
		
		tablePanel.add(tablePane, this.getDefaultGridbagConstraints(0));
		
		table = new TCMTable(new BetalingsadministrationTableModel(this.centerData), new BetalingsadministrationTableRenderer());
		tablePane = new JScrollPane(table);
		
		tablePane.setBorder(BorderFactory.createLoweredBevelBorder());
		tablePane.setPreferredSize(new Dimension((int)table.getPreferredSize().getWidth(),(int)table.getPreferredSize().getHeight()+30));
		tablePane.setMinimumSize(new Dimension((int)table.getPreferredSize().getWidth(),(int)table.getPreferredSize().getHeight()+30));
		
		tablePanel.add(tablePane, this.getDefaultGridbagConstraints(1));

		table = new TCMTable(new BankadministrationTableModel(this.centerData), new AdministrationsServiceTableRenderer());
		tablePane = new JScrollPane(table);
		
		tablePane.setBorder(BorderFactory.createLoweredBevelBorder());
		tablePane.setPreferredSize(new Dimension((int)table.getPreferredSize().getWidth(),(int)table.getPreferredSize().getHeight()+30));
		tablePane.setMinimumSize(new Dimension((int)table.getPreferredSize().getWidth(),(int)table.getPreferredSize().getHeight()+30));
		
		tablePanel.add(tablePane, this.getDefaultGridbagConstraints(2));
		
		table = new TCMTable(new RealkreditadministrationTableModel(this.centerData), new AdministrationsServiceTableRenderer());
		tablePane = new JScrollPane(table);
		
		tablePane.setBorder(BorderFactory.createLoweredBevelBorder());
		tablePane.setPreferredSize(new Dimension((int)table.getPreferredSize().getWidth(),(int)table.getPreferredSize().getHeight()+30));
		tablePane.setMinimumSize(new Dimension((int)table.getPreferredSize().getWidth(),(int)table.getPreferredSize().getHeight()+30));
		
		tablePanel.add(tablePane, this.getDefaultGridbagConstraints(3));

		table = new TCMTable(new AdministrationsOptimeringTableModel(this.centerData), new AdministrationsServiceTableRenderer());
		tablePane = new JScrollPane(table);
		
		tablePane.setBorder(BorderFactory.createLoweredBevelBorder());
		tablePane.setPreferredSize(new Dimension((int)table.getPreferredSize().getWidth(),(int)table.getPreferredSize().getHeight()+30));
		tablePane.setMinimumSize(new Dimension((int)table.getPreferredSize().getWidth(),(int)table.getPreferredSize().getHeight()+30));
		
		tablePanel.add(tablePane, this.getDefaultGridbagConstraints(4));
		
		tablePanel.add(this.getDescriptionPanel(),
					   this.getDefaultGridbagConstraints(5));
		
		return tablePanel;
	}
	
	private JScrollPane getCenterMålTableInScrollPane(){
		TCMTable table = new TCMTable(new CenterTableModel(this.centerData), new CenterTableRenderer()); 
		JScrollPane tablePane = new JScrollPane(table);
		
		tablePane.setBorder(BorderFactory.createRaisedBevelBorder());
		tablePane.setPreferredSize(new Dimension((int)table.getPreferredSize().getWidth(),(int)table.getPreferredSize().getHeight()+30));
		tablePane.setMinimumSize(new Dimension((int)table.getPreferredSize().getWidth(),(int)table.getPreferredSize().getHeight()+30));
		
		return tablePane;
	}
//	private JScrollPane getTræningsMålTableInScrollPane(){
//		TCMTable table = new TCMTable(new TraeningsTableModel(this.centerData), new TraeningsTableRenderer());
//		JScrollPane tablePane = new JScrollPane(table);
//
//		tablePane.setBorder(BorderFactory.createRaisedBevelBorder());
//		tablePane.setPreferredSize(new Dimension((int)table.getPreferredSize().getWidth(),(int)table.getPreferredSize().getHeight()+30));
//		tablePane.setMinimumSize(new Dimension((int)table.getPreferredSize().getWidth(),(int)table.getPreferredSize().getHeight()+30));
//	
//		return tablePane;
//	}
	private JScrollPane getDopeTableInScrollPane(){
		TCMTable table = new TCMTable(new DopeTableModel(centerData), new DopeTableRenderer());
		JScrollPane tablePane = new JScrollPane(table);
		
		tablePane.setPreferredSize(new Dimension((int)table.getPreferredSize().getWidth(),(int)table.getPreferredSize().getHeight()+30));
		tablePane.setMinimumSize(new Dimension((int)table.getPreferredSize().getWidth(),(int)table.getPreferredSize().getHeight()+30));
		
		return tablePane;
	}
	private JScrollPane getKundeaktiviteterTableInScrollPane(){
		TCMTable table = new TCMTable(new KundeAktiviteterTableModel(centerData), new KundeAktiviteterTableRenderer());
		JScrollPane tablePane = new JScrollPane(table);
		
		tablePane.setPreferredSize(new Dimension((int)table.getPreferredSize().getWidth(),(int)table.getPreferredSize().getHeight()+30));
		tablePane.setMinimumSize(new Dimension((int)table.getPreferredSize().getWidth(),(int)table.getPreferredSize().getHeight()+30));
		
		return tablePane;		
	}
	private JScrollPane getSekretariatOgPlanlaegningTalbeInScrollPane(){
		TCMTable table = new TCMTable(new SekretariatTableModel(this.centerData), new SekretariatTableRenderer());
		JScrollPane scrollPane = new JScrollPane(table);
		
		scrollPane.setPreferredSize(new Dimension((int)table.getPreferredSize().getWidth(),(int)table.getPreferredSize().getHeight()+30));
		scrollPane.setMinimumSize(new Dimension((int)table.getPreferredSize().getWidth(),(int)table.getPreferredSize().getHeight()+30));
		
		return scrollPane;
	}
	private JScrollPane getProjektTableInScrollPane(){
		TCMTable table = new TCMTable(new ProjektTableModel(this.centerData), new ProjektTableRenderer());
		JScrollPane scrollPane = new JScrollPane(table);
		
		scrollPane.setPreferredSize(new Dimension((int)table.getPreferredSize().getWidth(),(int)table.getPreferredSize().getHeight()+30));
		scrollPane.setMinimumSize(new Dimension((int)table.getPreferredSize().getWidth(),(int)table.getPreferredSize().getHeight()+30));
		
		return scrollPane;		
	}
	private JScrollPane getTeamGoalTableInScrollPane(Team team){
		DefaultTableModel tableModel = null;
		NTableRenderer renderer = null;
		
		switch(team.getTableType()){
			case Basis:
				tableModel = new B1B6TableModel(this.centerData, team);
				renderer = new B1B6TableRenderer();
				break;
			case Forsikring:
				tableModel = new ForsikringsserviceTableModel(this.centerData, team);
				renderer = new ForsikringsserviceTableRenderer();
				break;
			case KoncernOmstilling:
				tableModel = new KoncernOmstillingTableModel(this.centerData, team);
				renderer = new KoncernOmstillingTableRenderer();
				break;
			case ErhvervsService:
				tableModel = new ErhvervsserviceTableModel(this.centerData, team);
				renderer = new ErhvervsserviceTableRenderer();
				break;
			case NetbankKundesupport:
				tableModel = new NetbankKundesupportTableModel(this.centerData, team);
				renderer = new NetbankKundesupportTableRenderer();
				break;
			case Support:
				tableModel = new SupportTableModel(this.centerData, team);
				renderer = new SupportTableRenderer();
				break;
			case MailService:
				tableModel = new MailserviceTableModel(this.centerData, team);
				renderer = new MailserviceTableRenderer();
				break;
			case EkspeditionsService1:
				tableModel = new Ekspedition1TableModel(this.centerData, team);
				renderer = new Ekspedition1TableRenderer();
				break;
			case EkspeditionsService2:
				tableModel = new Ekspedition2TableModel(this.centerData, team);
				renderer = new Ekspedition2TableRenderer();
				break;
			default:
				return null;
		}
		
		TCMTable teamGoalTable = new TCMTable(tableModel, renderer);
		
		JScrollPane teamGoalTablePanel = new JScrollPane(teamGoalTable);
		
		teamGoalTablePanel.setBorder(BorderFactory.createLoweredBevelBorder());
		teamGoalTablePanel.setPreferredSize(new Dimension((int)teamGoalTable.getPreferredSize().getWidth(),(int)teamGoalTable.getPreferredSize().getHeight()+30));
		teamGoalTablePanel.setMinimumSize(new Dimension((int)teamGoalTable.getPreferredSize().getWidth(),(int)teamGoalTable.getPreferredSize().getHeight()+30));
		
		return teamGoalTablePanel;
	}
	private JScrollPane getDepartmentGoalTableInScrollPane(String departmentName){
		DefaultTableModel tableModel = null;
		NTableRenderer renderer = null;
		
		if(departmentName.equalsIgnoreCase("Basis")){
			tableModel = new BasisServiceDepartmentTableModel(this.centerData,
														      Team.getTeamIds(this.teams, TeamTableType.Basis),
														      Team.getTeamIds(this.teams, TeamTableType.Forsikring),
														      Team.getTeamIds(this.teams, TeamTableType.KoncernOmstilling));
			renderer = new BasisServiceDepartmentTableRenderer();
		}else if(departmentName.equalsIgnoreCase("Digitale")){
			tableModel = new DigitaleServicesDepartmentTableModel(this.centerData,
																  Team.getTeamIds(this.teams, TeamTableType.NetbankKundesupport),
																  Team.getTeamIds(this.teams, TeamTableType.ErhvervsService),
																  Team.getTeamIds(this.teams, TeamTableType.MailService),
																  Team.getTeamIds(this.teams, TeamTableType.Support));
			renderer = new DigitaleServicesDepartmentTableRenderer();
		}else if(departmentName.equalsIgnoreCase("Ekspedition")){
			tableModel = new EkspeditionsServiceDepartmentTableModel(this.centerData,
																	 Team.getTeamIds(this.teams, TeamTableType.EkspeditionsService1),
																	 Team.getTeamIds(this.teams, TeamTableType.EkspeditionsService2));
			renderer = new EkspeditionsServiceDepartmentTableRenderer();
		}else if(departmentName.equalsIgnoreCase("Stab")){
			
		}
		
		TCMTable departmentTable = new TCMTable(tableModel, renderer);
			
		JScrollPane departmentGoalTablePanel = new JScrollPane(departmentTable);
		
		departmentGoalTablePanel.setBorder(BorderFactory.createRaisedBevelBorder());
		departmentGoalTablePanel.setPreferredSize(new Dimension((int)departmentTable.getPreferredSize().getWidth(),(int)departmentTable.getPreferredSize().getHeight()+30));
		departmentGoalTablePanel.setMinimumSize(new Dimension((int)departmentTable.getPreferredSize().getWidth(),(int)departmentTable.getPreferredSize().getHeight()+30));
		
		return departmentGoalTablePanel;
	}
//	private JScrollPane getKundeAktiviteterGoalTableInScrollPane(){
//		TCMTable kundeAktiviteterTable = new TCMTable(new KundeAktiviteterTableModel(this.centerData), new KundeAktiviteterTableRenderer());
//		
//		JScrollPane tablePanel = new JScrollPane(kundeAktiviteterTable);
//		
//		tablePanel.setBorder(BorderFactory.createLoweredBevelBorder());
//		tablePanel.setPreferredSize(new Dimension((int)kundeAktiviteterTable.getPreferredSize().getWidth(),(int)kundeAktiviteterTable.getPreferredSize().getHeight()+30));
//		tablePanel.setMinimumSize(new Dimension((int)kundeAktiviteterTable.getPreferredSize().getWidth(),(int)kundeAktiviteterTable.getPreferredSize().getHeight()+30));
//		
//		return tablePanel;
//	}
	
	private JPanel getDescriptionPanel(){
		JPanel descriptionPanel = new JPanel(new GridBagLayout());
		
		descriptionPanel.add(new JLabel("* Ekskl. indeværende måned"), this.getDefaultGridbagConstraints(1));
		descriptionPanel.add(new JLabel("** Realiseret År Til Dato + Budget for resten af året"), this.getDefaultGridbagConstraints(2));
		descriptionPanel.add(new JLabel("*** Målopfyldelsen pr. måned resten af året for at nå målet"), this.getDefaultGridbagConstraints(3));
		
		JButton copyTables = new JButton("Kopier som billede");
		copyTables.addActionListener(this);
		JButton update = new JButton("Opdatér");
		update.addActionListener(new ActionListener() {
        	 
            public void actionPerformed(ActionEvent e)
            {
                //Execute when button is pressed
            	                
                run();
                frame.setVisible(false);
            }
        });     
		
		descriptionPanel.add(copyTables, this.getDefaultGridbagConstraints(5));
		descriptionPanel.add(update, this.getDefaultGridbagConstraints(6));

		return descriptionPanel;
	}
	private GridBagConstraints getDefaultGridbagConstraints(int gridY){
		return new GridBagConstraints(0, 
									  gridY, 
									  1, 
									  1, 
									  0, 
									  0, 
									  GridBagConstraints.WEST, 
									  GridBagConstraints.NONE, 
									  new Insets(3, 1, 0, 1), 
									  0, 
									  0);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(50, 25, 1000, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource().getClass() == JButton.class){
			JPanel tablesPanel = (JPanel)((JButton)e.getSource()).getParent().getParent();
			
			int topTableX = tablesPanel.getComponent(0).getX();
			int topTableY = tablesPanel.getComponent(0).getY();
			
			int height = tablesPanel.getComponent(tablesPanel.getComponentCount() - 2).getY() +
						 tablesPanel.getComponent(tablesPanel.getComponentCount() - 2).getHeight() -
						 topTableY;
			
			int width = tablesPanel.getComponent(0).getWidth();
			
	        BufferedImage img = new BufferedImage(tablesPanel.getWidth(), tablesPanel.getHeight(), BufferedImage.TYPE_INT_RGB);
	        
	        Graphics g = img.getGraphics();
	        
	        tablesPanel.paint(g);
	        
	        img = img.getSubimage(topTableX, topTableY, width, height);
	        
	        ImageSelection imgSel = new ImageSelection(img);
	        imgSel.copyImageToClipboard();
		}
	}
	
    private class ImageSelection implements Transferable {    
    	private Image image;       
    	
    	public void copyImageToClipboard(){        
    		ImageSelection imageSelection = new ImageSelection(image);        
    		Toolkit toolkit = Toolkit.getDefaultToolkit();        
    		toolkit.getSystemClipboard().setContents(imageSelection, null);    
    	}       
    	
    	public ImageSelection(Image image) {        
    		this.image = image;    
    	}
    	
    	public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException {        
    		if (flavor.equals(DataFlavor.imageFlavor) == false){            
    			throw new UnsupportedFlavorException(flavor);        
    		}        
    		
    		return image;    
    	}       
    	
    	public boolean isDataFlavorSupported(DataFlavor flavor){        
    		return flavor.equals(DataFlavor.imageFlavor);    
    	}       
    	
    	public DataFlavor[] getTransferDataFlavors(){        
    		return new DataFlavor[] {            
    			DataFlavor.imageFlavor        
    		};    
    	}
    }
}
