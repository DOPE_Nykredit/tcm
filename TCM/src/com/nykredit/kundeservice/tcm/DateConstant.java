package com.nykredit.kundeservice.tcm;

import java.util.Calendar;

public class DateConstant {
	
	/**
	 * The actual year the program is set to display
	 */
	public static int year = 2013;
	/**
	 * start date of the year the program is set to display
	 */
	public static String startDate = year+"-01-01";
	/**
	 *  end date of the year the program is set to display
	 */
	public static String endDate = DateConstant.getEndDate();	
	/**
	 * the current month of the year taken from the system
	 */
	//public static int currentMonth = DateConstant.getCurrentMonth();
	public static int getCurrentMonth(){
		Calendar c = Calendar.getInstance();
			return c.get(Calendar.MONTH) +1;
			
		
	}
	/**
	 * the current year taken from the system
	 */
	public static int currentYear = Calendar.getInstance().get(Calendar.YEAR);
	/**
	 *  completed month lagged 1 from currentMonth
	 */
	public static int completedMonths = DateConstant.getCurrentMonth() - 1;
	/**
	 *  number of remaining months based on completed months
	 */
	public static int remainingMonths = 12-completedMonths;
	
	private static String getEndDate(){
		Calendar c = Calendar.getInstance();
		
		c.add(Calendar.DAY_OF_MONTH, -1);
		if(c.get(Calendar.YEAR) == 2013){
			return "2013-" + (c.get(Calendar.MONTH) + 1) + "-" + (c.get(Calendar.DAY_OF_MONTH));
		}else{
			return "2014-01-01";
		}
	}
	/**
	 * 
	 * current number of completed quarters based on DateConstant.year
	 * 
	 * @return double completedQuarter
	 */
	public static int completedQuarter(){
		int completedQuarter = 0;
		if(DateConstant.completedMonths>2 | DateConstant.year<DateConstant.currentYear)	completedQuarter = 1;
		else if(DateConstant.completedMonths>5 | DateConstant.year<DateConstant.currentYear) completedQuarter = 2;
		else if(DateConstant.completedMonths>8 | DateConstant.year<DateConstant.currentYear) completedQuarter = 3;
		else if(DateConstant.currentYear>DateConstant.year) completedQuarter = 4;
		return completedQuarter;
	}

}
