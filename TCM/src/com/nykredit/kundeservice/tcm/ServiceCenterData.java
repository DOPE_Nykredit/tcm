package com.nykredit.kundeservice.tcm;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;

import com.nykredit.kundeservice.data.*;
import com.nykredit.kundeservice.tcm.datatypes.CBR;
import com.nykredit.kundeservice.tcm.datatypes.CTIData;
import com.nykredit.kundeservice.tcm.datatypes.CallbackData;
import com.nykredit.kundeservice.tcm.datatypes.CompletedSales;
import com.nykredit.kundeservice.tcm.datatypes.CustomerSatisfactionData;
import com.nykredit.kundeservice.tcm.datatypes.DialogActivityData;
import com.nykredit.kundeservice.tcm.datatypes.DialogLeadsGeneratedData;
import com.nykredit.kundeservice.tcm.datatypes.DialogLeadsRealizedData;
import com.nykredit.kundeservice.tcm.datatypes.EmailData;
import com.nykredit.kundeservice.tcm.datatypes.TCMMiscStats;
import com.nykredit.kundeservice.tcm.datatypes.EmailData.EmailGroups;
import com.nykredit.kundeservice.tcm.datatypes.PhoneQueueData;
import com.nykredit.kundeservice.tcm.datatypes.PhoneQueueData.Queue;
import com.nykredit.kundeservice.tcm.datatypes.SupportTaskData;
import com.nykredit.kundeservice.tcm.datatypes.WebdeskData;
import com.nykredit.kundeservice.tcm.datatypes.calculations.AHT;
import com.nykredit.kundeservice.tcm.datatypes.calculations.EmailSvarprocent;
import com.nykredit.kundeservice.tcm.datatypes.calculations.KPI;
import com.nykredit.kundeservice.tcm.datatypes.calculations.HaandteringsGrad;
import com.nykredit.kundeservice.tcm.datatypes.calculations.KundeaktiviteterIndenforTidsfrist;
import com.nykredit.kundeservice.tcm.datatypes.calculations.Kundetilfredshed;
import com.nykredit.kundeservice.tcm.datatypes.calculations.MersalgsGrad;
import com.nykredit.kundeservice.tcm.datatypes.calculations.MiscHaandteringsgrad;
import com.nykredit.kundeservice.tcm.datatypes.calculations.Number;
import com.nykredit.kundeservice.tcm.datatypes.calculations.OpgaverIndenforTidsfrist;
import com.nykredit.kundeservice.tcm.datatypes.calculations.TelefonSvarprocent;
import com.nykredit.kundeservice.tcm.datatypes.calculations.Tilgaengelighed;
import com.nykredit.kundeservice.tcm.datatypes.calculations.WebdeskSvarProcent;

public class ServiceCenterData {

	private ArrayList<CTIData> ctiData = new ArrayList<CTIData>();
	private ArrayList<PhoneQueueData> phoneQueueData = new ArrayList<PhoneQueueData>();
	private ArrayList<WebdeskData> webdeskData = new ArrayList<WebdeskData>();
	private ArrayList<EmailData> emailData = new ArrayList<EmailData>();
	private ArrayList<SupportTaskData> supportTaskData = new ArrayList<SupportTaskData>();
	private ArrayList<CustomerSatisfactionData> customerSatisfactionData = new ArrayList<CustomerSatisfactionData>();
	private ArrayList<DialogActivityData> dialogActivityData = new ArrayList<DialogActivityData>();
	private ArrayList<DialogLeadsGeneratedData> dialogLeadsGenerated = new ArrayList<DialogLeadsGeneratedData>();
	private ArrayList<DialogLeadsRealizedData> dialogLeadsReadlizedData = new ArrayList<DialogLeadsRealizedData>();
	private ArrayList<CompletedSales> completedSales = new ArrayList<CompletedSales>();
	private ArrayList<CBR> cbrData = new ArrayList<CBR>();
	private ArrayList<CallbackData> callbackData = new ArrayList<CallbackData>();
	private ArrayList<TCMMiscStats> miscStats = new ArrayList<TCMMiscStats>();

	public ServiceCenterData(){
	}

	public void loadData(CTIRConnection conn) throws SQLException {
		if(conn == null)
			throw new IllegalArgumentException("Conn cannot be null.");

		this.ctiData = CTIData.getCTIData(conn);
		this.phoneQueueData = PhoneQueueData.getPhoneQueueData(conn);
		this.webdeskData = WebdeskData.getWebdeskData(conn);
		this.emailData = EmailData.getEmailData(conn);
		this.supportTaskData = SupportTaskData.getSupportTaskData(conn);
		this.customerSatisfactionData = CustomerSatisfactionData.getCustomerSatisfactionData(conn);
		this.dialogActivityData = DialogActivityData.getDialogActivityData(conn);
		this.dialogLeadsGenerated = DialogLeadsGeneratedData.getDialogLeadsGeneratedData(conn);
		this.dialogLeadsReadlizedData = DialogLeadsRealizedData.getDialogLeadsRealizedData(conn);
		this.completedSales = CompletedSales.getCompletedSales(conn);
		this.miscStats = TCMMiscStats.getTCMMiscStats(conn);
		this.cbrData = CBR.getCBR(conn);
		this.callbackData = CallbackData.getAllCallbacks(conn);
	}

	public Tilgaengelighed getTilgængelighed(EMonth month, Team team){
		double klar = 0;
		double taler = 0;
		double øvrigTid = 0;
		double webdesk = 0;
		double korrigeretLogin = 0;
		double email = 0;
		double møde = 0;
		double uddannelse = 0;

		for(CTIData c : this.ctiData){
			if(c.getMonth() == month && c.getTeamId() == team.getId()){
				klar = c.getKlar();
				taler = c.getTaler();
				øvrigTid = c.getOvrigTid();
				webdesk = c.getWebdesk();
				korrigeretLogin = c.getKorrigeretLogin();
				email = c.getEmail();
				møde = c.getMøde();
				uddannelse = c.getUddannelse();

				break;
			}
		}

		return new Tilgaengelighed(klar, taler, øvrigTid, webdesk, korrigeretLogin, email, møde, uddannelse);
	}
	public Tilgaengelighed getTilgængelighed(int quarter, Team team){
		double klar = 0;
		double taler = 0;
		double øvrigTid = 0;
		double webdesk = 0;
		double korrigeretLogin = 0;
		double email = 0;
		double møde = 0;
		double uddannelse = 0;

		for(CTIData c : this.ctiData){
			if(c.getMonth().getQuarter() == quarter && c.getTeamId() == team.getId()){
				klar += c.getKlar();
				taler += c.getTaler();
				øvrigTid += c.getOvrigTid();
				webdesk += c.getWebdesk();
				korrigeretLogin += c.getKorrigeretLogin();
				email += c.getEmail();
				møde += c.getMøde();
				uddannelse += c.getUddannelse();
			}
		}

		return new Tilgaengelighed(klar, taler, øvrigTid, webdesk, korrigeretLogin, email, møde, uddannelse);
	}
	public Tilgaengelighed getTilgængelighed(Team team){
		double klar = 0;
		double taler = 0;
		double øvrigTid = 0;
		double webdesk = 0;
		double korrigeretLogin = 0;
		double email = 0;
		double møde = 0;
		double uddannelse = 0;

		for(CTIData c : this.ctiData)
			if(c.getMonth().before(EMonth.getCurrentMonth()) && c.getTeamId() == team.getId()){
				klar += c.getKlar();
				taler += c.getTaler();
				øvrigTid += c.getOvrigTid();
				webdesk += c.getWebdesk();
				korrigeretLogin += c.getKorrigeretLogin();
				email += c.getEmail();
				møde += c.getMøde();
				uddannelse += c.getUddannelse();
			}

		return new Tilgaengelighed(klar, taler, øvrigTid, webdesk, korrigeretLogin, email, møde, uddannelse);
	}
	public Tilgaengelighed getTilgængelighed(EMonth month, ArrayList<Integer> teamIds){
		double klar = 0;
		double taler = 0;
		double øvrigTid = 0;
		double webdesk = 0;
		double korrigeretLogin = 0;
		double email = 0;
		double møde = 0;
		double uddannelse = 0;

		for(CTIData c : this.ctiData){
			if(c.getMonth() == month && teamIds.contains(c.getTeamId())){
				klar += c.getKlar();
				taler += c.getTaler();
				øvrigTid += c.getOvrigTid();
				webdesk += c.getWebdesk();
				korrigeretLogin += c.getKorrigeretLogin();
				email += c.getEmail();
				møde += c.getMøde();
				uddannelse += c.getUddannelse();
			}
		}

		return new Tilgaengelighed(klar, taler, øvrigTid, webdesk, korrigeretLogin, email, møde, uddannelse);
	}
	public Tilgaengelighed getTilgængelighed(int quarter, ArrayList<Integer> teamIds){
		double klar = 0;
		double taler = 0;
		double øvrigTid = 0;
		double webdesk = 0;
		double korrigeretLogin = 0;
		double email = 0;
		double møde = 0;
		double uddannelse = 0;

		for(CTIData c : this.ctiData){
			if(c.getMonth().getQuarter() == quarter && teamIds.contains(c.getTeamId())){
				klar += c.getKlar();
				taler += c.getTaler();
				øvrigTid += c.getOvrigTid();
				webdesk += c.getWebdesk();
				korrigeretLogin += c.getKorrigeretLogin();
				email += c.getEmail();
				møde += c.getMøde();
				uddannelse += c.getUddannelse();
			}
		}

		return new Tilgaengelighed(klar, taler, øvrigTid, webdesk, korrigeretLogin, email, møde, uddannelse);
	}
	public Tilgaengelighed getTilgængelighed(ArrayList<Integer> teamIds){
		double klar = 0;
		double taler = 0;
		double øvrigTid = 0;
		double webdesk = 0;
		double korrigeretLogin = 0;
		double email = 0;
		double møde = 0;
		double uddannelse = 0;

		for(CTIData c : this.ctiData)
			if(teamIds.contains(c.getTeamId())){
				klar += c.getKlar();
				taler += c.getTaler();
				øvrigTid += c.getOvrigTid();
				webdesk += c.getWebdesk();
				korrigeretLogin += c.getKorrigeretLogin();
				email += c.getEmail();
				møde += c.getMøde();
				uddannelse += c.getUddannelse();
			}

		return new Tilgaengelighed(klar, taler, øvrigTid, webdesk, korrigeretLogin, email, møde, uddannelse);		
	}

	public KPI getProduktivitet(EMonth month, Team team, boolean medWebdesk, boolean medSupportOpgaver){
		Integer indgåendeKald = null;
		Double korrigeretLogin = null;
		Integer totalAktiviteter = null;
		Integer emails = null;
		Integer webdeskKald = null;
		Integer supportOpgaver = null;
		Integer kampagnePotentialer = null;
		Integer callbacks = null;

		for(CTIData c : this.ctiData)
			if(c.getMonth() == month && c.getTeamId() == team.getId()){
				if(indgåendeKald == null)
					indgåendeKald = c.getIndgaendeKald();
				else
					indgåendeKald += c.getIndgaendeKald();

				if(korrigeretLogin == null)
					korrigeretLogin = c.getKorrigeretLogin();
				else
					korrigeretLogin += c.getKorrigeretLogin();
			}

		for(DialogActivityData d : this.dialogActivityData)
			if(d.getMonth() == month && d.getTeamId() == team.getId()){
				if(totalAktiviteter == null)
					totalAktiviteter = d.getTotalActivities();
				else
					totalAktiviteter += d.getTotalActivities();

				if(medSupportOpgaver == false)
					if(emails == null)
						emails = d.getIncomingEmails();
					else
						emails += d.getIncomingEmails();
			}
		if(medWebdesk){
			if(team.toString().contains("Basis")){
				if(team.toString().equalsIgnoreCase("Basis 1")){
					for(WebdeskData w : this.webdeskData){
						if(w.getMonth() == month){
							if(webdeskKald == null){
								webdeskKald = w.getAnsweredByBasis1();
							}else{
								webdeskKald += w.getAnsweredByBasis1();
							}
						}
					}					
				}else if (team.toString().equalsIgnoreCase("Basis 2")) {
					for(WebdeskData w : this.webdeskData){
						if(w.getMonth() == month){
							if(webdeskKald == null){
								webdeskKald = w.getAnsweredByBasis2();
							}else{
								webdeskKald += w.getAnsweredByBasis2();
							}
						}
					}

				}else if (team.toString().equalsIgnoreCase("Basis 3")) {
					for(WebdeskData w : this.webdeskData){
						if(w.getMonth() == month){
							if(webdeskKald == null){
								webdeskKald = w.getAnsweredByBasis3();
							}else{
								webdeskKald += w.getAnsweredByBasis3();
							}
						}
					}

				}else if (team.toString().equalsIgnoreCase("Basis 4")) {
					for(WebdeskData w : this.webdeskData){
						if(w.getMonth() == month){
							if(webdeskKald == null){
								webdeskKald = w.getAnsweredByBasis4();
							}else{
								webdeskKald += w.getAnsweredByBasis4();
							}
						}
					}

				}else if (team.toString().equalsIgnoreCase("Basis 5")) {
					for(WebdeskData w : this.webdeskData){
						if(w.getMonth() == month){
							if(webdeskKald == null){
								webdeskKald = w.getAnsweredByBasis5();
							}else{
								webdeskKald += w.getAnsweredByBasis5();
							}
						}
					}

				}else if (team.toString().equalsIgnoreCase("Basis 6")) {
					for(WebdeskData w : this.webdeskData){
						if(w.getMonth() == month){
							if(webdeskKald == null){
								webdeskKald = w.getAnsweredByBasis6();
							}else{
								webdeskKald += w.getAnsweredByBasis6();
							}
						}
					}
				}else{
					for(WebdeskData w : this.webdeskData){
						if(w.getMonth() == month){
							if(webdeskKald == null){
								webdeskKald = w.getAnsweredByBasis();
							}else{
								webdeskKald += w.getAnsweredByBasis();
							}
						}
					}
				}
			}else{
				for(WebdeskData w : this.webdeskData)
					if(w.getMonth() == month)
						if(webdeskKald == null)
							webdeskKald = w.getAnsweredByMailservice();
						else
							webdeskKald += w.getAnsweredByMailservice();
			}
		}
		if(medSupportOpgaver){
			for(SupportTaskData s : this.supportTaskData)
				if(s.getMonth() == month)
					if(supportOpgaver == null)
						supportOpgaver = s.getTotalTasks();
					else
						supportOpgaver += s.getTotalTasks();

			if(medSupportOpgaver)
				for(DialogLeadsGeneratedData d: this.dialogLeadsGenerated)
					if(d.getMonth() == month)
						if(kampagnePotentialer == null)
							kampagnePotentialer = d.getCampaignLeads();
						else
							kampagnePotentialer += d.getCampaignLeads();
		}

		for(CallbackData c : this.callbackData){
			if(c.getMonth() == month && c.getTeamId() == team.getId())
				if(callbacks == null){

					callbacks = c.getCallbackCount();
				}else{
					callbacks += c.getCallbackCount();
				}
		}		
		return new KPI(indgåendeKald, 
				totalAktiviteter, 
				emails, 
				webdeskKald, 
				supportOpgaver, 
				kampagnePotentialer,
				callbacks,
				korrigeretLogin);
	}
	public KPI getProduktivitet(int quarter, Team team, boolean medWebdesk, boolean medSupportOpgaver){
		Integer indgåendeKald = null;
		Double korrigeretLogin = null;
		Integer totalAktiviteter = null;
		Integer emails = null;
		Integer webdeskKald = null;
		Integer supportOpgaver = null;
		Integer kampagnePotentialer = null;
		Integer callbacks = null;

		for(CTIData c : this.ctiData)
			if(c.getMonth().getQuarter() == quarter && c.getTeamId() == team.getId()){
				if(indgåendeKald == null)
					indgåendeKald = c.getIndgaendeKald();
				else
					indgåendeKald += c.getIndgaendeKald();

				if(korrigeretLogin == null)
					korrigeretLogin = c.getKorrigeretLogin();
				else
					korrigeretLogin += c.getKorrigeretLogin();
			}

		for(DialogActivityData d : this.dialogActivityData)
			if(d.getMonth().getQuarter() == quarter && d.getTeamId() == team.getId()){
				if(totalAktiviteter == null)
					totalAktiviteter = d.getTotalActivities();
				else
					totalAktiviteter += d.getTotalActivities();

				if(medSupportOpgaver == false)
					if(emails == null)
						emails = d.getIncomingEmails();
					else
						emails += d.getIncomingEmails();
			}


		if(medWebdesk){
			if(team.toString().contains("Basis")){
				if(team.toString().equalsIgnoreCase("Basis 1")){
					for(WebdeskData w : this.webdeskData){
						if(w.getMonth().getQuarter() == quarter){
							if(webdeskKald == null){
								webdeskKald = w.getAnsweredByBasis1();
							}else{
								webdeskKald += w.getAnsweredByBasis1();
							}
						}
					}					
				}else if (team.toString().equalsIgnoreCase("Basis 2")) {
					for(WebdeskData w : this.webdeskData){
						if(w.getMonth().getQuarter() == quarter){
							if(webdeskKald == null){
								webdeskKald = w.getAnsweredByBasis2();
							}else{
								webdeskKald += w.getAnsweredByBasis2();
							}
						}
					}

				}else if (team.toString().equalsIgnoreCase("Basis 3")) {
					for(WebdeskData w : this.webdeskData){
						if(w.getMonth().getQuarter() == quarter){
							if(webdeskKald == null){
								webdeskKald = w.getAnsweredByBasis3();
							}else{
								webdeskKald += w.getAnsweredByBasis3();
							}
						}
					}

				}else if (team.toString().equalsIgnoreCase("Basis 4")) {
					for(WebdeskData w : this.webdeskData){
						if(w.getMonth().getQuarter() == quarter){
							if(webdeskKald == null){
								webdeskKald = w.getAnsweredByBasis4();
							}else{
								webdeskKald += w.getAnsweredByBasis4();
							}
						}
					}

				}else if (team.toString().equalsIgnoreCase("Basis 5")) {
					for(WebdeskData w : this.webdeskData){
						if(w.getMonth().getQuarter() == quarter){
							if(webdeskKald == null){
								webdeskKald = w.getAnsweredByBasis5();
							}else{
								webdeskKald += w.getAnsweredByBasis5();
							}
						}
					}

				}else if (team.toString().equalsIgnoreCase("Basis 6")) {
					for(WebdeskData w : this.webdeskData){
						if(w.getMonth().getQuarter() == quarter){
							if(webdeskKald == null){
								webdeskKald = w.getAnsweredByBasis6();
							}else{
								webdeskKald += w.getAnsweredByBasis6();
							}
						}
					}
				}else{
					for(WebdeskData w : this.webdeskData){
						if(w.getMonth().getQuarter() == quarter){
							if(webdeskKald == null){
								webdeskKald = w.getAnsweredByBasis();
							}else{
								webdeskKald += w.getAnsweredByBasis();
							}
						}
					}
				}
			}else{
				for(WebdeskData w : this.webdeskData)
					if(w.getMonth().getQuarter() == quarter){
						if(webdeskKald == null)
							webdeskKald = w.getAnsweredByMailservice();
						else
							webdeskKald += w.getAnsweredByMailservice();
					}
			}
		}
		if(medSupportOpgaver){
			for(SupportTaskData s : this.supportTaskData)
				if(s.getMonth().getQuarter() == quarter)
					if(supportOpgaver == null)
						supportOpgaver = s.getTotalTasks();
					else
						supportOpgaver += s.getTotalTasks();

			for(DialogLeadsGeneratedData d: this.dialogLeadsGenerated)
				if(d.getMonth().getQuarter() == quarter)
					if(kampagnePotentialer == null)
						kampagnePotentialer = d.getCampaignLeads();
					else
						kampagnePotentialer += d.getCampaignLeads();
		}

		for(CallbackData c : this.callbackData)
			if(c.getMonth().getQuarter() == quarter && c.getTeamId() == team.getId())
				if(callbacks == null)
					callbacks = c.getCallbackCount();
				else
					callbacks += c.getCallbackCount();

		return new KPI(indgåendeKald, 
				totalAktiviteter, 
				emails, 
				webdeskKald, 
				supportOpgaver, 
				kampagnePotentialer,
				callbacks,
				korrigeretLogin);

	}
	public KPI getProduktivitet(Team team, boolean medWebdesk, boolean medSupportOpgaver){
		Integer indgåendeKald = null;
		Double korrigeretLogin = null;
		Integer totalAktiviteter = null;
		Integer emails = null;
		Integer webdeskKald = null;
		Integer supportOpgaver = null;
		Integer kampagnePotentialer = null;
		Integer callbacks = null;

		for(CTIData c : this.ctiData)
			if(c.getTeamId() == team.getId()){
				if(indgåendeKald == null)
					indgåendeKald = c.getIndgaendeKald();
				else
					indgåendeKald += c.getIndgaendeKald();

				if(korrigeretLogin == null)
					korrigeretLogin = c.getKorrigeretLogin();
				else
					korrigeretLogin += c.getKorrigeretLogin();
			}

		for(DialogActivityData d : this.dialogActivityData)
			if(d.getTeamId() == team.getId()){
				if(totalAktiviteter == null)
					totalAktiviteter = d.getTotalActivities();
				else
					totalAktiviteter += d.getTotalActivities();

				if(medSupportOpgaver == false)
					if(emails == null)
						emails = d.getIncomingEmails();
					else
						emails += d.getIncomingEmails();
			}

		if(medWebdesk)
			if(team.toString().contains("Basis")){
				for(WebdeskData w : this.webdeskData)
					
						if(webdeskKald == null)
							webdeskKald = w.getAnsweredByBasis();
						else
							webdeskKald += w.getAnsweredByBasis();
			}else{
				for(WebdeskData w : this.webdeskData)
						if(webdeskKald == null)
							webdeskKald = w.getAnsweredByMailservice();
						else
							webdeskKald += w.getAnsweredByMailservice();
			}
		if(medSupportOpgaver){
			for(SupportTaskData s : this.supportTaskData)
					if(supportOpgaver == null)
						supportOpgaver = s.getTotalTasks();
					else
						supportOpgaver += s.getTotalTasks();

			for(DialogLeadsGeneratedData d: this.dialogLeadsGenerated)
				if(team.getId() == d.getTeamId())
					if(kampagnePotentialer == null)
						kampagnePotentialer = d.getCampaignLeads();
					else
						kampagnePotentialer += d.getCampaignLeads();
		}

		for(CallbackData c : this.callbackData)
			if(c.getTeamId() == team.getId())
				if(callbacks == null)
					callbacks = c.getCallbackCount();
				else
					callbacks += c.getCallbackCount();

		return new KPI(indgåendeKald, 
				totalAktiviteter, 
				emails, 
				webdeskKald, 
				supportOpgaver, 
				kampagnePotentialer,
				callbacks,
				korrigeretLogin);
	}
	public KPI getProduktivitet(EMonth month, ArrayList<Integer> teamIds, boolean medWebdesk, boolean medSupportOpgaver){
		Integer indgåendeKald = null;
		Double korrigeretLogin = null;
		Integer totalAktiviteter = null;
		Integer emails = null;
		Integer webdeskKald = null;
		Integer supportOpgaver = null;
		Integer kampagnePotentialer = null;
		Integer callbacks = null;

		for(CTIData c : this.ctiData)
			if(c.getMonth() == month && teamIds.contains(c.getTeamId())){
				if(indgåendeKald == null)
					indgåendeKald = c.getIndgaendeKald();
				else
					indgåendeKald += c.getIndgaendeKald();

				if(korrigeretLogin == null)
					korrigeretLogin = c.getKorrigeretLogin();
				else
					korrigeretLogin += c.getKorrigeretLogin();
			}

		for(DialogActivityData d : this.dialogActivityData)
			if(d.getMonth() == month && teamIds.contains(d.getTeamId())){
				if(totalAktiviteter == null)
					totalAktiviteter = d.getTotalActivities();
				else
					totalAktiviteter += d.getTotalActivities();

				if(medSupportOpgaver == false)
					if(emails == null)
						emails = d.getIncomingEmails();
					else
						emails += d.getIncomingEmails();
			}

		if(medWebdesk){
			int i = 0;
			if(teamIds.contains(99)){
				i++;
				for(WebdeskData w : this.webdeskData){
					
						if(webdeskKald == null){
							webdeskKald = w.getAnsweredByBasis();
						}else{
							webdeskKald += w.getAnsweredByBasis();
						}
				}
			}else{
				for(WebdeskData w : this.webdeskData)
					if(w.getMonth() == month)
						if(webdeskKald == null)
							webdeskKald = w.getAnsweredByMailservice();
						else
							webdeskKald += w.getAnsweredByMailservice();
			}
		}
		if(medSupportOpgaver){
			for(SupportTaskData s : this.supportTaskData)
				if(s.getMonth() == month)
					if(supportOpgaver == null)
						supportOpgaver = s.getTotalTasks();
					else
						supportOpgaver += s.getTotalTasks();

			for(DialogLeadsGeneratedData d: this.dialogLeadsGenerated)
				if(d.getMonth() == month)
					if(kampagnePotentialer == null)
						kampagnePotentialer = d.getCampaignLeads();
					else
						kampagnePotentialer += d.getCampaignLeads();
		}

		for(CallbackData c : this.callbackData)
			if(c.getMonth() == month && teamIds.contains(c.getTeamId()))
				if(callbacks == null)
					callbacks = c.getCallbackCount();
				else
					callbacks += c.getCallbackCount();

		return new KPI(indgåendeKald, 
				totalAktiviteter, 
				emails, 
				webdeskKald, 
				supportOpgaver, 
				kampagnePotentialer,
				callbacks,
				korrigeretLogin);
	}
	public KPI getProduktivitet(int quarter, ArrayList<Integer> teamIds, boolean medWebdesk, boolean medSupportOpgaver){
		Integer indgåendeKald = null;
		Double korrigeretLogin = null;
		Integer totalAktiviteter = null;
		Integer emails = null;
		Integer webdeskKald = null;
		Integer supportOpgaver = null;
		Integer kampagnePotentialer = null;
		Integer callbacks = null;

		for(CTIData c : this.ctiData)
			if(c.getMonth().getQuarter() == quarter && teamIds.contains(c.getTeamId())){
				if(indgåendeKald == null)
					indgåendeKald = c.getIndgaendeKald();
				else
					indgåendeKald += c.getIndgaendeKald();

				if(korrigeretLogin == null)
					korrigeretLogin = c.getKorrigeretLogin();
				else
					korrigeretLogin += c.getKorrigeretLogin();
			}

		for(DialogActivityData d : this.dialogActivityData)
			if(d.getMonth().getQuarter() == quarter && teamIds.contains(d.getTeamId())){
				if(totalAktiviteter == null)
					totalAktiviteter = d.getTotalActivities();
				else
					totalAktiviteter += d.getTotalActivities();

				if(medSupportOpgaver == false)
					if(emails == null)
						emails = d.getIncomingEmails();
					else
						emails += d.getIncomingEmails();
			}

		if(medWebdesk)
			if(teamIds.contains(99)){
				for(WebdeskData w : this.webdeskData)
					
						if(webdeskKald == null)
							webdeskKald = w.getAnsweredByBasis();
						else
							webdeskKald += w.getAnsweredByBasis();
			}else{
				for(WebdeskData w : this.webdeskData)
					if(w.getMonth().getQuarter() == quarter)
						if(webdeskKald == null)
							webdeskKald = w.getAnsweredByMailservice();
						else
							webdeskKald += w.getAnsweredByMailservice();
			}
		if(medSupportOpgaver){
			for(SupportTaskData s : this.supportTaskData)
				if(s.getMonth().getQuarter() == quarter)
					if(supportOpgaver == null)
						supportOpgaver = s.getTotalTasks();
					else
						supportOpgaver += s.getTotalTasks();

			for(DialogLeadsGeneratedData d: this.dialogLeadsGenerated)
				if(d.getMonth().getQuarter() == quarter)
					if(kampagnePotentialer == null)
						kampagnePotentialer = d.getCampaignLeads();
					else
						kampagnePotentialer += d.getCampaignLeads();
		}

		for(CallbackData c : this.callbackData)
			if(c.getMonth().getQuarter() == quarter && teamIds.contains(c.getTeamId()))
				if(callbacks == null)
					callbacks = c.getCallbackCount();
				else
					callbacks += c.getCallbackCount();

		return new KPI(indgåendeKald, 
				totalAktiviteter, 
				emails, 
				webdeskKald, 
				supportOpgaver, 
				kampagnePotentialer,
				callbacks,
				korrigeretLogin);
	}
	public KPI getProduktivitet(ArrayList<Integer> teamIds, boolean medWebdesk, boolean medSupportOpgaver){
		Integer indgåendeKald = null;
		Double korrigeretLogin = null;
		Integer totalAktiviteter = null;
		Integer emails = null;
		Integer webdeskKald = null;
		Integer supportOpgaver = null;
		Integer kampagnePotentialer = null;
		Integer callbacks = null;

		for(CTIData c : this.ctiData)
			if(teamIds.contains(c.getTeamId())){
				if(indgåendeKald == null)
					indgåendeKald = c.getIndgaendeKald();
				else
					indgåendeKald += c.getIndgaendeKald();

				if(korrigeretLogin == null)
					korrigeretLogin = c.getKorrigeretLogin();
				else
					korrigeretLogin += c.getKorrigeretLogin();
			}

		for(DialogActivityData d : this.dialogActivityData)
			if(teamIds.contains(d.getTeamId())){
				if(totalAktiviteter == null)
					totalAktiviteter = d.getTotalActivities();
				else
					totalAktiviteter += d.getTotalActivities();

				if(medSupportOpgaver == false)
					if(emails == null)
						emails = d.getIncomingEmails();
					else
						emails += d.getIncomingEmails();
			}

		if(medWebdesk)
			if(teamIds.contains(99)){
				for(WebdeskData w : this.webdeskData)
					
						if(webdeskKald == null){
							webdeskKald = w.getAnsweredByBasis();

						}else{
							webdeskKald += w.getAnsweredByBasis();
						}
			}else{
				for(WebdeskData w : this.webdeskData)
					
						if(webdeskKald == null)
							webdeskKald = w.getAnsweredByMailservice();
						else
							webdeskKald += w.getAnsweredByMailservice();
			}
		if(medSupportOpgaver){
			for(SupportTaskData s : this.supportTaskData)
					if(supportOpgaver == null)
						supportOpgaver = s.getTotalTasks();
					else
						supportOpgaver += s.getTotalTasks();

			for(DialogLeadsGeneratedData d: this.dialogLeadsGenerated)
					if(kampagnePotentialer == null)
						kampagnePotentialer = d.getCampaignLeads();
					else
						kampagnePotentialer += d.getCampaignLeads();
		}

		for(CallbackData c : this.callbackData)
			if(teamIds.contains(c.getTeamId()))
				if(callbacks == null)
					callbacks = c.getCallbackCount();
				else
					callbacks += c.getCallbackCount();

		return new KPI(indgåendeKald, 
				totalAktiviteter, 
				emails, 
				webdeskKald, 
				supportOpgaver, 
				kampagnePotentialer,
				callbacks,
				korrigeretLogin);
	}

	public Number getForsikringsSalg(EMonth month, Team team){
		Integer forsikringsSalg = null;

		for(DialogLeadsRealizedData d : this.dialogLeadsReadlizedData)
			if(d.getMonth() == month && d.getTeamId() == team.getId())
				if(forsikringsSalg == null)
					forsikringsSalg = d.getInsuranceSold();
				else
					forsikringsSalg += d.getInsuranceSold();

		return new Number(forsikringsSalg);
	}
	public Number getForsikringsSalg(int quarter, Team team){
		Integer forsikringsSalg = null;

		for(DialogLeadsRealizedData d : this.dialogLeadsReadlizedData)
			if(d.getMonth().getQuarter() == quarter && d.getTeamId() == team.getId())
				if(forsikringsSalg == null)
					forsikringsSalg = d.getInsuranceSold();
				else
					forsikringsSalg += d.getInsuranceSold();

		return new Number(forsikringsSalg);
	}
	public Number getForsikringsSalg(Team team){
		Integer forsikringsSalg = null;

		for(DialogLeadsRealizedData d : this.dialogLeadsReadlizedData)
			if(d.getMonth().before(EMonth.getCurrentMonth()) && d.getTeamId() == team.getId())
				if(forsikringsSalg == null)
					forsikringsSalg = d.getInsuranceSold();
				else
					forsikringsSalg += d.getInsuranceSold();

		return new Number(forsikringsSalg);
	}
	public Number getForsikringsSalg(EMonth month, ArrayList<Integer> teamIds){
		Integer forsikringsSalg = null;

		for(DialogLeadsRealizedData d : this.dialogLeadsReadlizedData)
			if(d.getMonth() == month && teamIds.contains(d.getTeamId()))
				if(forsikringsSalg == null)
					forsikringsSalg = d.getInsuranceSold();
				else
					forsikringsSalg += d.getInsuranceSold();

		return new Number(forsikringsSalg);
	}
	public Number getForsikringsSalg(int quarter, ArrayList<Integer> teamIds){
		Integer forsikringsSalg = null;

		for(DialogLeadsRealizedData d : this.dialogLeadsReadlizedData)
			if(d.getMonth().getQuarter() == quarter && teamIds.contains(d.getTeamId()))
				if(forsikringsSalg == null)
					forsikringsSalg = d.getInsuranceSold();
				else
					forsikringsSalg += d.getInsuranceSold();

		return new Number(forsikringsSalg);
	}
	public Number getForsikringsSalg(ArrayList<Integer> teamIds){
		Integer forsikringsSalg = null;

		for(DialogLeadsRealizedData d : this.dialogLeadsReadlizedData)
			if(d.getMonth().before(EMonth.getCurrentMonth()) && teamIds.contains(d.getTeamId()))
				if(forsikringsSalg == null)
					forsikringsSalg = d.getInsuranceSold();
				else
					forsikringsSalg += d.getInsuranceSold();

		return new Number(forsikringsSalg);
	}
	public Number getForsikringsSalgCenter(EMonth month){
		Integer forsikringsSalg = null;

		for(DialogLeadsRealizedData d : this.dialogLeadsReadlizedData)
			if(d.getMonth() == month)
				if(forsikringsSalg == null)
					forsikringsSalg = d.getInsuranceSold();
				else
					forsikringsSalg += d.getInsuranceSold();

		return new Number(forsikringsSalg);
	}
	public Number getForsikringsSalgCenter(int quarter){
		Integer forsikringsSalg = null;

		for(DialogLeadsRealizedData d : this.dialogLeadsReadlizedData)
			if(d.getMonth().getQuarter() == quarter)
				if(forsikringsSalg == null)
					forsikringsSalg = d.getInsuranceSold();
				else
					forsikringsSalg += d.getInsuranceSold();

		return new Number(forsikringsSalg);
	}
	public Number getForsikringsSalgCenter(){
		Integer forsikringsSalg = null;

		for(DialogLeadsRealizedData d : this.dialogLeadsReadlizedData)
			if(d.getMonth().before(EMonth.getCurrentMonth()))
				if(forsikringsSalg == null)
					forsikringsSalg = d.getInsuranceSold();
				else
					forsikringsSalg += d.getInsuranceSold();

		return new Number(forsikringsSalg);
	}

	public Kundetilfredshed getKundetilfredshed(EMonth month, Team team, boolean includePhone, boolean includeEmail){
		Integer telefonMegetTilfreds = null;
		Integer telefonSvar = null;

		Integer emailTilfreds = null;
		Integer emailSvar = null;

		if(includePhone)
			for(CustomerSatisfactionData c : this.customerSatisfactionData)
				if(c.getMonth() == month && c.getTeamId() == team.getId()){
					if(telefonMegetTilfreds == null)
						telefonMegetTilfreds = c.getVerySatisfied();
					else
						telefonMegetTilfreds += c.getVerySatisfied();

					if(telefonSvar == null)
						telefonSvar = c.getRespondents();
					else
						telefonSvar += c.getRespondents();
				}	

		if(includeEmail)
			for(TCMMiscStats s :this.miscStats)
				if(s.getMonth() == month){
					if(emailTilfreds == null)
						emailTilfreds = s.getMsKundetilfredshedSucces();
					else
						emailTilfreds += s.getMsKundetilfredshedSucces();

					if(emailSvar == null)
						emailSvar = s.getMsKundetilfredshedAntal();
					else
						emailSvar += s.getMsKundetilfredshedAntal();
				}

		return new Kundetilfredshed(telefonMegetTilfreds, telefonSvar, emailTilfreds, emailSvar);
	}
	public Kundetilfredshed getKundetilfredshed(int quarter, Team team, boolean includePhone, boolean includeEmail){
		Integer telefonMegetTilfreds = null;
		Integer telefonSvar = null;

		Integer emailTilfreds = null;
		Integer emailSvar = null;

		if(includePhone)
			for(CustomerSatisfactionData c : this.customerSatisfactionData)
				if(c.getMonth().getQuarter() == quarter && c.getTeamId() == team.getId()){
					if(telefonMegetTilfreds == null)
						telefonMegetTilfreds = c.getVerySatisfied();
					else
						telefonMegetTilfreds += c.getVerySatisfied();

					if(telefonSvar == null)
						telefonSvar = c.getRespondents();
					else
						telefonSvar += c.getRespondents();
				}

		if(includeEmail)
			for(TCMMiscStats s :this.miscStats)
				if(s.getMonth().getQuarter() == quarter){
					if(emailTilfreds == null)
						emailTilfreds = s.getMsKundetilfredshedSucces();
					else
						emailTilfreds += s.getMsKundetilfredshedSucces();

					if(emailSvar == null)
						emailSvar = s.getMsKundetilfredshedAntal();
					else
						emailSvar += s.getMsKundetilfredshedAntal();
				}

		return new Kundetilfredshed(telefonMegetTilfreds, telefonSvar, emailTilfreds, emailSvar);
	}
	public Kundetilfredshed getKundetilfredshed(Team team, boolean includePhone, boolean includeEmail){
		Integer telefonMegetTilfreds = null;
		Integer telefonSvar = null;

		Integer emailTilfreds = null;
		Integer emailSvar = null;

		if(includePhone)
			for(CustomerSatisfactionData c : this.customerSatisfactionData)
				if(c.getTeamId() == team.getId()){
					if(telefonMegetTilfreds == null)
						telefonMegetTilfreds = c.getVerySatisfied();
					else
						telefonMegetTilfreds += c.getVerySatisfied();

					if(telefonSvar == null)
						telefonSvar = c.getRespondents();
					else
						telefonSvar += c.getRespondents();
				}

		if(includeEmail)
			for(TCMMiscStats s :this.miscStats){
					if(emailTilfreds == null)
						emailTilfreds = s.getMsKundetilfredshedSucces();
					else
						emailTilfreds += s.getMsKundetilfredshedSucces();

					if(emailSvar == null)
						emailSvar = s.getMsKundetilfredshedAntal();
					else
						emailSvar += s.getMsKundetilfredshedAntal();
				}

		return new Kundetilfredshed(telefonMegetTilfreds, telefonSvar, emailTilfreds, emailSvar);
	}
	public Kundetilfredshed getKundetilfredshed(EMonth month, ArrayList<Integer> teamIds, boolean includePhone, boolean includeEmail){
		Integer telefonMegetTilfreds = null;
		Integer telefonSvar = null;

		Integer emailTilfreds = null;
		Integer emailSvar = null;

		if(includePhone)
			for(CustomerSatisfactionData c : this.customerSatisfactionData)
				if(c.getMonth() == month && teamIds.contains(c.getTeamId())){
					if(telefonMegetTilfreds == null)
						telefonMegetTilfreds = c.getVerySatisfied();
					else
						telefonMegetTilfreds += c.getVerySatisfied();

					if(telefonSvar == null)
						telefonSvar = c.getRespondents();
					else
						telefonSvar += c.getRespondents();
				}

		if(includeEmail)
			for(TCMMiscStats s :this.miscStats)
				if(s.getMonth() == month){
					if(emailTilfreds == null)
						emailTilfreds = s.getMsKundetilfredshedSucces();
					else
						emailTilfreds += s.getMsKundetilfredshedSucces();

					if(emailSvar == null)
						emailSvar = s.getMsKundetilfredshedAntal();
					else
						emailSvar += s.getMsKundetilfredshedAntal();
				}

		return new Kundetilfredshed(telefonMegetTilfreds, telefonSvar, emailTilfreds, emailSvar);
	}
	public Kundetilfredshed getKundetilfredshed(int quarter, ArrayList<Integer> teamIds, boolean includePhone, boolean includeEmail){
		Integer telefonMegetTilfreds = null;
		Integer telefonSvar = null;

		Integer emailTilfreds = null;
		Integer emailSvar = null;

		if(includePhone)
			for(CustomerSatisfactionData c : this.customerSatisfactionData)
				if(c.getMonth().getQuarter() == quarter && teamIds.contains(c.getTeamId())){
					if(telefonMegetTilfreds == null)
						telefonMegetTilfreds = c.getVerySatisfied();
					else
						telefonMegetTilfreds += c.getVerySatisfied();

					if(telefonSvar == null)
						telefonSvar = c.getRespondents();
					else
						telefonSvar += c.getRespondents();
				}

		if(includeEmail)
			for(TCMMiscStats s :this.miscStats)
				if(s.getMonth().getQuarter() == quarter){
					if(emailTilfreds == null)
						emailTilfreds = s.getMsKundetilfredshedSucces();
					else
						emailTilfreds += s.getMsKundetilfredshedSucces();

					if(emailSvar == null)
						emailSvar = s.getMsKundetilfredshedAntal();
					else
						emailSvar += s.getMsKundetilfredshedAntal();
				}

		return new Kundetilfredshed(telefonMegetTilfreds, telefonSvar, emailTilfreds, emailSvar);
	}
	public Kundetilfredshed getKundetilfredshed(ArrayList<Integer> teamIds, boolean includePhone, boolean includeEmail){
		Integer telefonMegetTilfreds = null;
		Integer telefonSvar = null;

		Integer emailTilfreds = null;
		Integer emailSvar = null;

		if(includePhone)
			for(CustomerSatisfactionData c : this.customerSatisfactionData)
				if(teamIds.contains(c.getTeamId())){
					if(telefonMegetTilfreds == null)
						telefonMegetTilfreds = c.getVerySatisfied();
					else
						telefonMegetTilfreds += c.getVerySatisfied();

					if(telefonSvar == null)
						telefonSvar = c.getRespondents();
					else
						telefonSvar += c.getRespondents();
				}

		if(includeEmail)
			for(TCMMiscStats s :this.miscStats){
				if(emailTilfreds == null)
					emailTilfreds = s.getMsKundetilfredshedSucces();
				else
					emailTilfreds += s.getMsKundetilfredshedSucces();

				if(emailSvar == null)
					emailSvar = s.getMsKundetilfredshedAntal();
				else
					emailSvar += s.getMsKundetilfredshedAntal();
			}

		return new Kundetilfredshed(telefonMegetTilfreds, telefonSvar, emailTilfreds, emailSvar);
	}
	public Kundetilfredshed getKundetilfredshedCenter(EMonth month, boolean includePhone, boolean includeEmail){
		Integer telefonMegetTilfreds = null;
		Integer telefonSvar = null;

		Integer emailTilfreds = null;
		Integer emailSvar = null;

		if(includePhone)
			for(CustomerSatisfactionData c : this.customerSatisfactionData)
				if(c.getMonth() == month){
					if(telefonMegetTilfreds == null)
						telefonMegetTilfreds = c.getVerySatisfied();
					else
						telefonMegetTilfreds += c.getVerySatisfied();

					if(telefonSvar == null)
						telefonSvar = c.getRespondents();
					else
						telefonSvar += c.getRespondents();
				}

		if(includeEmail)
			for(TCMMiscStats s :this.miscStats)
				if(s.getMonth() == month){
					if(emailTilfreds == null)
						emailTilfreds = s.getMsKundetilfredshedSucces();
					else
						emailTilfreds += s.getMsKundetilfredshedSucces();

					if(emailSvar == null)
						emailSvar = s.getMsKundetilfredshedAntal();
					else
						emailSvar += s.getMsKundetilfredshedAntal();
				}

		return new Kundetilfredshed(telefonMegetTilfreds, telefonSvar, emailTilfreds, emailSvar);
	}
	public Kundetilfredshed getKundetilfredshedCenter(int quarter, boolean includePhone, boolean includeEmail){
		Integer telefonMegetTilfreds = null;
		Integer telefonSvar = null;

		Integer emailTilfreds = null;
		Integer emailSvar = null;

		if(includePhone)
			for(CustomerSatisfactionData c : this.customerSatisfactionData)
				if(c.getMonth().getQuarter() == quarter){
					if(telefonMegetTilfreds == null)
						telefonMegetTilfreds = c.getVerySatisfied();
					else
						telefonMegetTilfreds += c.getVerySatisfied();

					if(telefonSvar == null)
						telefonSvar = c.getRespondents();
					else
						telefonSvar += c.getRespondents();
				}

		if(includeEmail)
			for(TCMMiscStats s :this.miscStats)
				if(s.getMonth().getQuarter() == quarter){
					if(emailTilfreds == null)
						emailTilfreds = s.getMsKundetilfredshedSucces();
					else
						emailTilfreds += s.getMsKundetilfredshedSucces();

					if(emailSvar == null)
						emailSvar = s.getMsKundetilfredshedAntal();
					else
						emailSvar += s.getMsKundetilfredshedAntal();
				}

		return new Kundetilfredshed(telefonMegetTilfreds, telefonSvar, emailTilfreds, emailSvar);
	}
	public Kundetilfredshed getKundetilfredshedCenter(boolean includePhone, boolean includeEmail){
		Integer telefonMegetTilfreds = null;
		Integer telefonSvar = null;

		Integer emailTilfreds = null;
		Integer emailSvar = null;

		if(includePhone)
			for(CustomerSatisfactionData c : this.customerSatisfactionData){
					if(telefonMegetTilfreds == null)
						telefonMegetTilfreds = c.getVerySatisfied();
					else
						telefonMegetTilfreds += c.getVerySatisfied();

					if(telefonSvar == null)
						telefonSvar = c.getRespondents();
					else
						telefonSvar += c.getRespondents();
			}

		if(includeEmail)
			for(TCMMiscStats s :this.miscStats){

				if(emailTilfreds == null)
					emailTilfreds = s.getMsKundetilfredshedSucces();
				else
					emailTilfreds += s.getMsKundetilfredshedSucces();

				if(emailSvar == null)
					emailSvar = s.getMsKundetilfredshedAntal();
				else
					emailSvar += s.getMsKundetilfredshedAntal();

			}
		return new Kundetilfredshed(telefonMegetTilfreds, telefonSvar, emailTilfreds, emailSvar);
	}

	public HaandteringsGrad getKundeAktiviteterIndenforTidsfristCenter(EMonth month){
		Integer aktiviteterIndenforTidsfrist = null;
		Integer aktiviteterTotal = null;

		Integer emailsIndenforTidsfrist = null;
		Integer emailsTotal = null;

		OpgaverIndenforTidsfrist supportOpgaver = this.getAktiviteterIndenforTidsfrist(month, new Team(26, "","","",null), true);

		for(DialogActivityData d : this.dialogActivityData)
			if(d.getMonth() == month && d.getTeamId() != 26){
				if(aktiviteterIndenforTidsfrist == null)
					aktiviteterIndenforTidsfrist = d.getCompletedWithinDeadline();
				else
					aktiviteterIndenforTidsfrist += d.getCompletedWithinDeadline();

				if(aktiviteterTotal == null)
					aktiviteterTotal = d.getCompletedWithinDeadline() + d.getExceededDeadline();
				else
					aktiviteterTotal += d.getCompletedWithinDeadline() + d.getExceededDeadline();
			}

		for(EmailData e : this.emailData)
			if(e.getMonth() == month){
				if(emailsIndenforTidsfrist == null)
					emailsIndenforTidsfrist = e.getTotalEmailsCompletedWithinDeadline();
				else
					emailsIndenforTidsfrist += e.getTotalEmailsCompletedWithinDeadline();

				if(emailsTotal == null)
					emailsTotal = e.getTotalEmailsReceived();
				else
					emailsTotal += e.getTotalEmailsReceived();

			}

		return new KundeaktiviteterIndenforTidsfrist(aktiviteterIndenforTidsfrist, 
				aktiviteterTotal, 
				emailsIndenforTidsfrist, 
				emailsTotal,
				supportOpgaver.getOpfyldt(),
				supportOpgaver.getTotal());
	}
	public HaandteringsGrad getKundeAktiviteterIndenforTidsfristCenter(int quarter){
		Integer aktiviteterIndenforTidsfrist = null;
		Integer aktiviteterTotal = null;

		Integer emailsIndenforTidsfrist = null;
		Integer emailsTotal = null;

		OpgaverIndenforTidsfrist supportOpgaver = this.getAktiviteterIndenforTidsfrist(quarter, new Team(26, "","","",null), true);

		for(DialogActivityData d : this.dialogActivityData)
			if(d.getMonth().getQuarter() == quarter && d.getTeamId() != 26){
				if(aktiviteterIndenforTidsfrist == null)
					aktiviteterIndenforTidsfrist = d.getCompletedWithinDeadline();
				else
					aktiviteterIndenforTidsfrist += d.getCompletedWithinDeadline();

				if(aktiviteterTotal == null)
					aktiviteterTotal = d.getCompletedWithinDeadline() + d.getExceededDeadline();
				else
					aktiviteterTotal += d.getCompletedWithinDeadline() + d.getExceededDeadline();
			}

		for(EmailData e : this.emailData)
			if(e.getMonth().getQuarter() == quarter){
				if(emailsIndenforTidsfrist == null)
					emailsIndenforTidsfrist = e.getTotalEmailsCompletedWithinDeadline();
				else
					emailsIndenforTidsfrist += e.getTotalEmailsCompletedWithinDeadline();

				if(emailsTotal == null)
					emailsTotal = e.getTotalEmailsReceived();
				else
					emailsTotal += e.getTotalEmailsReceived();
			}

		return new KundeaktiviteterIndenforTidsfrist(aktiviteterIndenforTidsfrist, 
				aktiviteterTotal, 
				emailsIndenforTidsfrist, 
				emailsTotal, 
				supportOpgaver.getOpfyldt(),
				supportOpgaver.getTotal());
	}
	public HaandteringsGrad getKundeAktiviteterIndenforTidsfristCenter(){
		Integer aktiviteterIndenforTidsfrist = null;
		Integer aktiviteterTotal = null;

		Integer emailsIndenforTidsfrist = null;
		Integer emailsTotal = null;

		OpgaverIndenforTidsfrist supportOpgaver = this.getAktiviteterIndenforTidsfrist(new Team(26, "","","",null), true);

		for(DialogActivityData d : this.dialogActivityData)
			if(d.getMonth().before(EMonth.getCurrentMonth()) && d.getTeamId() != 26){
				if(aktiviteterIndenforTidsfrist == null)
					aktiviteterIndenforTidsfrist = d.getCompletedWithinDeadline();
				else
					aktiviteterIndenforTidsfrist += d.getCompletedWithinDeadline();

				if(aktiviteterTotal == null)
					aktiviteterTotal = d.getCompletedWithinDeadline() + d.getExceededDeadline();
				else
					aktiviteterTotal += d.getCompletedWithinDeadline() + d.getExceededDeadline();
			}

		for(EmailData e : this.emailData){
			if(emailsIndenforTidsfrist == null)
				emailsIndenforTidsfrist = e.getTotalEmailsCompletedWithinDeadline();
			else
				emailsIndenforTidsfrist += e.getTotalEmailsCompletedWithinDeadline();

			if(emailsTotal == null)
				emailsTotal = e.getTotalEmailsReceived();
			else
				emailsTotal += e.getTotalEmailsReceived();
		}

		return new KundeaktiviteterIndenforTidsfrist(aktiviteterIndenforTidsfrist, 
				aktiviteterTotal, 
				emailsIndenforTidsfrist, 
				emailsTotal, 
				supportOpgaver.getOpfyldt(),
				supportOpgaver.getTotal());
	}

	public HaandteringsGrad getTlfKoeSvarPct(EMonth month, Queue queue){
		Integer indenfor25Sek = null;
		Integer total = null;

		for(PhoneQueueData p : this.phoneQueueData)
			if(p.getMonth() == month){
				if(queue == Queue.KunderOgStabe){
					if(indenfor25Sek == null)
						indenfor25Sek = p.getKundeOgStabeCallsAnsweredWithin25Sec();
					else
						indenfor25Sek += p.getKundeOgStabeCallsAnsweredWithin25Sec();

					if(total == null)
						total = p.getKunderOgStabeCalls();
					else
						total += p.getKunderOgStabeCalls();
				}else if(queue == Queue.Erhverv){
					if(indenfor25Sek == null)
						indenfor25Sek = p.getErhvervCallsAnsweredWithin25Sec();
					else
						indenfor25Sek += p.getErhvervCallsAnsweredWithin25Sec();

					if(total == null)
						total = p.getErhvervCalls();
					else
						total += p.getErhvervCalls();
				}else if(queue ==  Queue.Hotline){
					if(indenfor25Sek == null)
						indenfor25Sek = p.getHotlineCallsAnsweredWithin25Sec();
					else
						indenfor25Sek += p.getHotlineCallsAnsweredWithin25Sec();

					if(total == null)
						total = p.getHotlineCalls();
					else
						total += p.getHotlineCalls();
				}else if(queue == Queue.Andre){
					if(indenfor25Sek == null)
						indenfor25Sek = p.getOtherCallsAnsweredWithin25Sec();
					else
						indenfor25Sek += p.getOtherCallsAnsweredWithin25Sec();

					if(total == null)
						total = p.getOtherCalls();
					else
						total += p.getOtherCalls();
				}
			}

		return new TelefonSvarprocent(indenfor25Sek, total);
	}
	public HaandteringsGrad getTlfKoeSvarPct(int quarter, Queue queue){
		Integer indenfor25Sek = null;
		Integer total = null;

		for(PhoneQueueData p : this.phoneQueueData)
			if(p.getMonth().getQuarter() == quarter){
				if(queue == Queue.KunderOgStabe){
					if(indenfor25Sek == null)
						indenfor25Sek = p.getKundeOgStabeCallsAnsweredWithin25Sec();
					else
						indenfor25Sek += p.getKundeOgStabeCallsAnsweredWithin25Sec();

					if(total == null)
						total = p.getKunderOgStabeCalls();
					else
						total += p.getKunderOgStabeCalls();
				}else if(queue == Queue.Erhverv){
					if(indenfor25Sek == null)
						indenfor25Sek = p.getErhvervCallsAnsweredWithin25Sec();
					else
						indenfor25Sek += p.getErhvervCallsAnsweredWithin25Sec();

					if(total == null)
						total = p.getErhvervCalls();
					else
						total += p.getErhvervCalls();
				}else if(queue ==  Queue.Hotline){
					if(indenfor25Sek == null)
						indenfor25Sek = p.getHotlineCallsAnsweredWithin25Sec();
					else
						indenfor25Sek += p.getHotlineCallsAnsweredWithin25Sec();

					if(total == null)
						total = p.getHotlineCalls();
					else
						total += p.getHotlineCalls();
				}else if(queue == Queue.Andre){
					if(indenfor25Sek == null)
						indenfor25Sek = p.getOtherCallsAnsweredWithin25Sec();
					else
						indenfor25Sek += p.getOtherCallsAnsweredWithin25Sec();

					if(total == null)
						total = p.getOtherCalls();
					else
						total += p.getOtherCalls();
				}
			}

		return new TelefonSvarprocent(indenfor25Sek, total);
	}
	public HaandteringsGrad getTlfKoeSvarPct(Queue queue){
		Integer indenfor25Sek = null;
		Integer total = null;

		for(PhoneQueueData p : this.phoneQueueData){
			if(queue == Queue.KunderOgStabe){
				if(indenfor25Sek == null)
					indenfor25Sek = p.getKundeOgStabeCallsAnsweredWithin25Sec();
				else
					indenfor25Sek += p.getKundeOgStabeCallsAnsweredWithin25Sec();

				if(total == null)
					total = p.getKunderOgStabeCalls();
				else
					total += p.getKunderOgStabeCalls();
			}else if(queue == Queue.Erhverv){
				if(indenfor25Sek == null)
					indenfor25Sek = p.getErhvervCallsAnsweredWithin25Sec();
				else
					indenfor25Sek += p.getErhvervCallsAnsweredWithin25Sec();

				if(total == null)
					total = p.getErhvervCalls();
				else
					total += p.getErhvervCalls();
			}else if(queue ==  Queue.Hotline){
				if(indenfor25Sek == null)
					indenfor25Sek = p.getHotlineCallsAnsweredWithin25Sec();
				else
					indenfor25Sek += p.getHotlineCallsAnsweredWithin25Sec();

				if(total == null)
					total = p.getHotlineCalls();
				else
					total += p.getHotlineCalls();
			}else if(queue == Queue.Andre){
				if(indenfor25Sek == null)
					indenfor25Sek = p.getOtherCallsAnsweredWithin25Sec();
				else
					indenfor25Sek += p.getOtherCallsAnsweredWithin25Sec();

				if(total == null)
					total = p.getOtherCalls();
				else
					total += p.getOtherCalls();
			}
		}

		return new TelefonSvarprocent(indenfor25Sek, total);
	}
	public HaandteringsGrad getTlfKoeSvarPct(EMonth month, Queue[] queues){
		Integer indenfor25Sek = null;
		Integer total = null;

		for(PhoneQueueData p : this.phoneQueueData)
			if(p.getMonth() == month){
				if(Arrays.asList(queues).contains(Queue.KunderOgStabe)){
					if(indenfor25Sek == null)
						indenfor25Sek = p.getKundeOgStabeCallsAnsweredWithin25Sec();
					else
						indenfor25Sek += p.getKundeOgStabeCallsAnsweredWithin25Sec();

					if(total == null)
						total = p.getKunderOgStabeCalls();
					else
						total += p.getKunderOgStabeCalls();
				}
				if(Arrays.asList(queues).contains(Queue.Erhverv)){
					if(indenfor25Sek == null)
						indenfor25Sek = p.getErhvervCallsAnsweredWithin25Sec();
					else
						indenfor25Sek += p.getErhvervCallsAnsweredWithin25Sec();

					if(total == null)
						total = p.getErhvervCalls();
					else
						total += p.getErhvervCalls();
				}
				if(Arrays.asList(queues).contains(Queue.Hotline)){
					if(indenfor25Sek == null)
						indenfor25Sek = p.getHotlineCallsAnsweredWithin25Sec();
					else
						indenfor25Sek += p.getHotlineCallsAnsweredWithin25Sec();

					if(total == null)
						total = p.getHotlineCalls();
					else
						total += p.getHotlineCalls();
				}
				if(Arrays.asList(queues).contains(Queue.Andre)){
					if(indenfor25Sek == null)
						indenfor25Sek = p.getOtherCallsAnsweredWithin25Sec();
					else
						indenfor25Sek += p.getOtherCallsAnsweredWithin25Sec();

					if(total == null)
						total = p.getOtherCalls();
					else
						total += p.getOtherCalls();
				}
			}

		return new TelefonSvarprocent(indenfor25Sek, total);
	}
	public HaandteringsGrad getTlfKoeSvarPct(int quarter, Queue[] queues){
		Integer indenfor25Sek = null;
		Integer total = null;

		for(PhoneQueueData p : this.phoneQueueData)
			if(p.getMonth().getQuarter() == quarter){
				if(Arrays.asList(queues).contains(Queue.KunderOgStabe)){
					if(indenfor25Sek == null)
						indenfor25Sek = p.getKundeOgStabeCallsAnsweredWithin25Sec();
					else
						indenfor25Sek += p.getKundeOgStabeCallsAnsweredWithin25Sec();

					if(total == null)
						total = p.getKunderOgStabeCalls();
					else
						total += p.getKunderOgStabeCalls();
				}
				if(Arrays.asList(queues).contains(Queue.Erhverv)){
					if(indenfor25Sek == null)
						indenfor25Sek = p.getErhvervCallsAnsweredWithin25Sec();
					else
						indenfor25Sek += p.getErhvervCallsAnsweredWithin25Sec();

					if(total == null)
						total = p.getErhvervCalls();
					else
						total += p.getErhvervCalls();
				}
				if(Arrays.asList(queues).contains(Queue.Hotline)){
					if(indenfor25Sek == null)
						indenfor25Sek = p.getHotlineCallsAnsweredWithin25Sec();
					else
						indenfor25Sek += p.getHotlineCallsAnsweredWithin25Sec();

					if(total == null)
						total = p.getHotlineCalls();
					else
						total += p.getHotlineCalls();
				}
				if(Arrays.asList(queues).contains(Queue.Andre)){
					if(indenfor25Sek == null)
						indenfor25Sek = p.getOtherCallsAnsweredWithin25Sec();
					else
						indenfor25Sek += p.getOtherCallsAnsweredWithin25Sec();

					if(total == null)
						total = p.getOtherCalls();
					else
						total += p.getOtherCalls();
				}
			}

		return new TelefonSvarprocent(indenfor25Sek, total);
	}	
	public HaandteringsGrad getTlfKoeSvarPct(Queue[] queues){
		Integer indenfor25Sek = null;
		Integer total = null;

		for(PhoneQueueData p : this.phoneQueueData)
			if(p.getMonth().before(EMonth.getCurrentMonth())){
				if(Arrays.asList(queues).contains(Queue.KunderOgStabe)){
					if(indenfor25Sek == null)
						indenfor25Sek = p.getKundeOgStabeCallsAnsweredWithin25Sec();
					else
						indenfor25Sek += p.getKundeOgStabeCallsAnsweredWithin25Sec();

					if(total == null)
						total = p.getKunderOgStabeCalls();
					else
						total += p.getKunderOgStabeCalls();
				}
				if(Arrays.asList(queues).contains(Queue.Erhverv)){
					if(indenfor25Sek == null)
						indenfor25Sek = p.getErhvervCallsAnsweredWithin25Sec();
					else
						indenfor25Sek += p.getErhvervCallsAnsweredWithin25Sec();

					if(total == null)
						total = p.getErhvervCalls();
					else
						total += p.getErhvervCalls();
				}
				if(Arrays.asList(queues).contains(Queue.Hotline)){
					if(indenfor25Sek == null)
						indenfor25Sek = p.getHotlineCallsAnsweredWithin25Sec();
					else
						indenfor25Sek += p.getHotlineCallsAnsweredWithin25Sec();

					if(total == null)
						total = p.getHotlineCalls();
					else
						total += p.getHotlineCalls();
				}
				if(Arrays.asList(queues).contains(Queue.Andre)){
					if(indenfor25Sek == null)
						indenfor25Sek = p.getOtherCallsAnsweredWithin25Sec();
					else
						indenfor25Sek += p.getOtherCallsAnsweredWithin25Sec();

					if(total == null)
						total = p.getOtherCalls();
					else
						total += p.getOtherCalls();
				}
			}

		return new TelefonSvarprocent(indenfor25Sek, total);
	}
	public HaandteringsGrad getTlfKoeSvarPctCenter(EMonth month){
		Integer indenfor25Sek = null;
		Integer total = null;

		for(PhoneQueueData p : this.phoneQueueData)
			if(p.getMonth() == month){
				if(indenfor25Sek == null)
					indenfor25Sek = p.getTotalCallsAnsweredWithin25Sec();
				else
					indenfor25Sek += p.getTotalCallsAnsweredWithin25Sec();

				if(total == null)
					total = p.getTotalCalls();
				else
					total += p.getTotalCalls();
			}

		return new TelefonSvarprocent(indenfor25Sek, total);
	}
	public HaandteringsGrad getTlfKoeSvarPctCenter(int quarter){
		Integer indenfor25Sek = null;
		Integer total = null;

		for(PhoneQueueData p : this.phoneQueueData)
			if(p.getMonth().getQuarter() == quarter){
				if(indenfor25Sek == null)
					indenfor25Sek = p.getTotalCallsAnsweredWithin25Sec();
				else
					indenfor25Sek += p.getTotalCallsAnsweredWithin25Sec();

				if(total == null)
					total = p.getTotalCalls();
				else
					total += p.getTotalCalls();
			}

		return new TelefonSvarprocent(indenfor25Sek, total);
	}
	public HaandteringsGrad getTlfKoeSvarPctCenter(){
		Integer indenfor25Sek = null;
		Integer total = null;

		for(PhoneQueueData p : this.phoneQueueData){
				if(indenfor25Sek == null)
					indenfor25Sek = p.getTotalCallsAnsweredWithin25Sec();
				else
					indenfor25Sek += p.getTotalCallsAnsweredWithin25Sec();

				if(total == null)
					total = p.getTotalCalls();
				else
					total += p.getTotalCalls();
		}

		return new TelefonSvarprocent(indenfor25Sek, total);
	}

	public WebdeskSvarProcent getWebdeskSvarProcent(EMonth month){
		Integer besvaret = null;
		Integer kald = null;

		for(WebdeskData d : this.webdeskData)
			if(d.getMonth() == month){
				if(besvaret == null)
					besvaret = d.getAnsweredWithin30Sec();
				else
					besvaret += d.getAnsweredWithin30Sec();

				if(kald == null)
					kald = d.getCalls();
				else
					kald += d.getCalls();
			}

		return new WebdeskSvarProcent(besvaret, kald);
	}
	public WebdeskSvarProcent getWebdeskSvarProcent(int quarter){
		Integer besvaret = null;
		Integer kald = null;

		for(WebdeskData d : this.webdeskData)
			if(d.getMonth().getQuarter() == quarter){
				if(besvaret == null)
					besvaret = d.getAnsweredWithin30Sec();
				else
					besvaret += d.getAnsweredWithin30Sec();

				if(kald == null)
					kald = d.getCalls();
				else
					kald += d.getCalls();
			}

		return new WebdeskSvarProcent(besvaret, kald);
	}
	public WebdeskSvarProcent getWebdeskSvarProcent(){
		Integer besvaret = null;
		Integer kald = null;

		for(WebdeskData d : this.webdeskData){
			if(besvaret == null)
				besvaret = d.getAnsweredWithin30Sec();
			else
				besvaret += d.getAnsweredWithin30Sec();

			if(kald == null)
				kald = d.getCalls();
			else
				kald += d.getCalls();
		}

		return new WebdeskSvarProcent(besvaret, kald);
	}

	public EmailSvarprocent getEmailSvarProcent(EMonth month, EmailData.EmailGroups group){
		Integer besvaret = null;
		Integer modtaget = null;
		for(EmailData e : this.emailData){
			if(e.getMonth() == month){
				if(group == EmailGroups.Mailservice){
					if(besvaret == null)
						besvaret = e.getMailServiceEmailsCompletedWithinDeadline();
					else
						besvaret += e.getMailServiceEmailsCompletedWithinDeadline();

					if(modtaget == null)
						modtaget = e.getMailServiceEmailsReceived();
					else
						modtaget += e.getMailServiceEmailsReceived();
				} else if (group == EmailGroups.Kampagne) {
					if(besvaret == null)
						besvaret = e.getCampaignEmailsCompletedWithinDeadline();
					else
						besvaret += e.getCampaignEmailsCompletedWithinDeadline();

					if(modtaget == null)
						modtaget = e.getTotalCampaignEmailsReceived();
					else
						modtaget += e.getTotalCampaignEmailsReceived();
				}

				break;
			}
		}
		return new EmailSvarprocent(besvaret, modtaget);
	}
	public EmailSvarprocent getEmailSvarProcent(int quarter, EmailData.EmailGroups group){
		Integer besvaret = null;
		Integer modtaget = null;

		for(EmailData e : this.emailData){
			if(e.getMonth().getQuarter() == quarter){
				if(group == EmailGroups.Mailservice){
					if(besvaret == null)
						besvaret = e.getMailServiceEmailsCompletedWithinDeadline();
					else
						besvaret += e.getMailServiceEmailsCompletedWithinDeadline();

					if(modtaget == null)
						modtaget = e.getMailServiceEmailsReceived();
					else
						modtaget += e.getMailServiceEmailsReceived();
				} else if (group == EmailGroups.Kampagne) {
					if(besvaret == null)
						besvaret = e.getCampaignEmailsCompletedWithinDeadline();
					else
						besvaret += e.getCampaignEmailsCompletedWithinDeadline();

					if(modtaget == null)
						modtaget = e.getTotalCampaignEmailsReceived();
					else
						modtaget += e.getTotalCampaignEmailsReceived();
				}
			}
		}
		return new EmailSvarprocent(besvaret, modtaget);
	}
	public EmailSvarprocent getEmailSvarProcent(EmailData.EmailGroups group){
		Integer besvaret = null;
		Integer modtaget = null;

		for(EmailData e : this.emailData){
			if(group == EmailGroups.Mailservice){
				if(besvaret == null)
					besvaret = e.getMailServiceEmailsCompletedWithinDeadline();
				else
					besvaret += e.getMailServiceEmailsCompletedWithinDeadline();

				if(modtaget == null)
					modtaget = e.getMailServiceEmailsReceived();
				else
					modtaget += e.getMailServiceEmailsReceived();
			} else if (group == EmailGroups.Kampagne) {
				if(besvaret == null)
					besvaret = e.getCampaignEmailsCompletedWithinDeadline();
				else
					besvaret += e.getCampaignEmailsCompletedWithinDeadline();

				if(modtaget == null)
					modtaget = e.getTotalCampaignEmailsReceived();
				else
					modtaget += e.getTotalCampaignEmailsReceived();
			}
		}
		return new EmailSvarprocent(besvaret, modtaget);
	}

	public Number getGennemførteSalg(EMonth month, Team team){
		Integer gennemførteSalg = 0;

		for(CompletedSales c : this.completedSales)
			if(c.getMonth() == month && c.getTeamId() == team.getId()){
				if(gennemførteSalg == null)
					gennemførteSalg = c.getCompletedSales();
				else
					gennemførteSalg += c.getCompletedSales();
			}

		return new Number(gennemførteSalg);
	}
	public Number getGennemførteSalg(int quarter, Team team){
		Integer gennemførteSalg = 0;
		for(CompletedSales c : this.completedSales)
			if(c.getMonth().getQuarter() == quarter && c.getTeamId() == team.getId())
				if(gennemførteSalg == null)
					gennemførteSalg = c.getCompletedSales();
				else
					gennemførteSalg += c.getCompletedSales();

		return new Number(gennemførteSalg);
	}
	public Number getGennemførteSalg(Team team){
		Integer gennemførteSalg = 0;

		for(CompletedSales c : this.completedSales){
			if(c.getTeamId() == team.getId())
				if(gennemførteSalg == null)
					gennemførteSalg = c.getCompletedSales();
				else
					gennemførteSalg += c.getCompletedSales();
		}
		return new Number(gennemførteSalg);
	}
	public Number getGennemførteSalg(EMonth month, ArrayList<Integer> teamIds){
		Integer gennemførteSalg = 0;

		for(CompletedSales c : this.completedSales)
			if(teamIds.contains(c.getTeamId()) && c.getMonth() == month)
				if(gennemførteSalg == null)
					gennemførteSalg = c.getCompletedSales();
				else
					gennemførteSalg += c.getCompletedSales();

		return new Number(gennemførteSalg);
	}
	public Number getGennemførteSalg(int quarter, ArrayList<Integer> teamIds){
		Integer gennemførteSalg = 0;

		for(CompletedSales c : this.completedSales)
			if(teamIds.contains(c.getTeamId()) && c.getMonth().getQuarter() == quarter)
				if(gennemførteSalg == null)
					gennemførteSalg = c.getCompletedSales();
				else
					gennemførteSalg += c.getCompletedSales();

		return new Number(gennemførteSalg);		
	}
	public Number getGennemførteSalg(ArrayList<Integer> teamIds){
		Integer gennemførteSalg = 0;
		for(CompletedSales c : this.completedSales){
			if(teamIds.contains(c.getTeamId()))
				if(gennemførteSalg == null)
					gennemførteSalg = c.getCompletedSales();
				else
					gennemførteSalg += c.getCompletedSales();
		}
		return new Number(gennemførteSalg);
	}
	public Number getGennemførteSalgCenter(EMonth month){
		Integer gennemførteSalg = 0;

		for(CompletedSales c : this.completedSales)
			if(c.getMonth() == month)
				if(gennemførteSalg == null)
					gennemførteSalg = c.getCompletedSales();
				else
					gennemførteSalg += c.getCompletedSales();

		return new Number(gennemførteSalg);
	}
	public Number getGennemførteSalgCenter(int quarter){
		Integer gennemførteSalg = 0;

		for(CompletedSales c : this.completedSales)
			if(c.getMonth().getQuarter() == quarter)
				if(gennemførteSalg == null)
					gennemførteSalg = c.getCompletedSales();
				else
					gennemførteSalg += c.getCompletedSales();

		return new Number(gennemførteSalg);
	}
	public Number getGennemførteSalgCenter(){
		Integer gennemførteSalg = 0;

		for(CompletedSales c : this.completedSales){
			if(gennemførteSalg == null)

				gennemførteSalg = c.getCompletedSales();
			else
				gennemførteSalg += c.getCompletedSales();
		}
		return new Number(gennemførteSalg);
	}

	public Number getMerSalgTilBasisService(EMonth month, Team team){
		return new Number(0);
	}
	public Number getMerSalgTilBasisService(int quarter, Team team){
		return new Number(0);
	}
	public Number getMerSalgTilBasisService(Team team){
		return new Number(0);
	}

	public AHT getAHT(EMonth month, Team team){
		int handlingSeconds = 0;
		int calls = 0;

		for(CBR c : this.cbrData)
			if(c.getMonth() == month && c.getTeamId() == team.getId()){
				handlingSeconds += c.getHåndteringsSekunder();
				calls += c.getAntalKald();

				break;
			}

		return new AHT(handlingSeconds, calls);
	}
	public AHT getAHT(int quarter, Team team){
		int handlingSeconds = 0;
		int calls = 0;

		for(CBR c : this.cbrData)
			if(c.getMonth().getQuarter() == quarter && c.getTeamId() == team.getId()){
				handlingSeconds += c.getHåndteringsSekunder();
				calls += c.getAntalKald();
			}

		return new AHT(handlingSeconds, calls);	
	}
	public AHT getAHT(Team team){
		int handlingSeconds = 0;
		int calls = 0;

		for(CBR c : this.cbrData)
			if(c.getTeamId() == team.getId()){
				handlingSeconds += c.getHåndteringsSekunder();
				calls += c.getAntalKald();
			}

		return new AHT(handlingSeconds, calls);
	}
	public AHT getAHT(EMonth month, ArrayList<Integer> teamIds){
		int handlingSeconds = 0;
		int calls = 0;

		for(CBR c : this.cbrData)
			if(c.getMonth() == month && teamIds.contains(c.getTeamId())){
				handlingSeconds += c.getHåndteringsSekunder();
				calls += c.getAntalKald();
			}

		return new AHT(handlingSeconds, calls);		
	}
	public AHT getAHT(int quarter, ArrayList<Integer> teamIds){
		int handlingSeconds = 0;
		int calls = 0;

		for(CBR c : this.cbrData)
			if(c.getMonth().getQuarter() == quarter && teamIds.contains(c.getTeamId())){
				handlingSeconds += c.getHåndteringsSekunder();
				calls += c.getAntalKald();
			}

		return new AHT(handlingSeconds, calls);
	}
	public AHT getAHT(ArrayList<Integer> teamIds){
		int handlingSeconds = 0;
		int calls = 0;

		for(CBR c : this.cbrData)
			if(teamIds.contains(c.getTeamId())){
				handlingSeconds += c.getHåndteringsSekunder();
				calls += c.getAntalKald();
			}

		return new AHT(handlingSeconds, calls);
	}

	public MersalgsGrad getMersalg(EMonth month, Team team){
		int forsikringsSalg = 0;
		int mersalg = 0;

		for(DialogLeadsRealizedData d : this.dialogLeadsReadlizedData)
			if(d.getMonth() == month && d.getTeamId() == team.getId()){
				forsikringsSalg = d.getInsuranceSold();
				mersalg = d.getAdditionalInsuranceSales();

				break;
			}

		return new  MersalgsGrad(mersalg, forsikringsSalg);
	}
	public MersalgsGrad getMersalg(int quarter, Team team){
		int forsikringsSalg = 0;
		int mersalg = 0;

		for(DialogLeadsRealizedData d : this.dialogLeadsReadlizedData)
			if(d.getMonth().getQuarter() == quarter && d.getTeamId() == team.getId()){
				forsikringsSalg += d.getInsuranceSold();
				mersalg += d.getAdditionalInsuranceSales();
			}

		return new MersalgsGrad(mersalg, forsikringsSalg);
	}
	public MersalgsGrad getMersalg(Team team){
		int forsikringsSalg = 0;
		int mersalg = 0;

		for(DialogLeadsRealizedData d : this.dialogLeadsReadlizedData)
			if(d.getTeamId() == team.getId()){
				forsikringsSalg += d.getInsuranceSold();
				mersalg += d.getAdditionalInsuranceSales();
			}

		return new MersalgsGrad(mersalg, forsikringsSalg);
	}

	public OpgaverIndenforTidsfrist getAktiviteterIndenforTidsfrist(EMonth month, Team team, boolean includeSupport){
		Integer dialogIndenforTidsfrist = null;
		Integer dialogTotal = null;

		Integer supportIndenforTidsfrist = null;
		Integer supportTotal = null;

		Integer kampagneMailsIndenforTidsfrist = null;
		Integer kampagneMailsTotal = null;

		for(DialogActivityData d : this.dialogActivityData)
			if(d.getMonth() == month && d.getTeamId() == team.getId()){
				if(dialogIndenforTidsfrist == null)
					dialogIndenforTidsfrist = d.getCompletedWithinDeadline();
				else
					dialogIndenforTidsfrist += d.getCompletedWithinDeadline();

				if(dialogTotal == null)
					dialogTotal = d.getTotalActivities();
				else
					dialogTotal += d.getTotalActivities();
			}

		if(includeSupport){
			for(SupportTaskData s : this.supportTaskData)
				if(s.getMonth() == month){
					if(supportIndenforTidsfrist == null)
						supportIndenforTidsfrist = s.getCompletedWithinDeadline();
					else
						supportIndenforTidsfrist += s.getCompletedWithinDeadline();

					if(supportTotal == null)
						supportTotal = s.getTotalTasks();
					else
						supportTotal += s.getTotalTasks();
				}

			for(EmailData e : this.emailData)
				if(e.getMonth() == month){
					if(kampagneMailsIndenforTidsfrist == null)
						kampagneMailsIndenforTidsfrist = e.getCampaignEmailsCompletedWithinDeadline();
					else
						supportIndenforTidsfrist += e.getCampaignEmailsCompletedWithinDeadline();

					if(kampagneMailsTotal == null)
						kampagneMailsTotal = e.getTotalCampaignEmailsReceived();
					else
						kampagneMailsTotal += e.getTotalCampaignEmailsReceived();
				}
		}


		return new OpgaverIndenforTidsfrist(dialogIndenforTidsfrist, 
				dialogTotal, 
				supportIndenforTidsfrist, 
				supportTotal, 
				kampagneMailsIndenforTidsfrist, 
				kampagneMailsTotal);
	}	
	public OpgaverIndenforTidsfrist getAktiviteterIndenforTidsfrist(int quarter, Team team, boolean includeSupport){
		Integer dialogIndenforTidsfrist = null;
		Integer dialogTotal = null;

		Integer supportIndenforTidsfrist = null;
		Integer supportTotal = null;

		Integer kampagneMailsIndenforTidsfrist = null;
		Integer kampagneMailsTotal = null;

		for(DialogActivityData d : this.dialogActivityData)
			if(d.getMonth().getQuarter() == quarter && d.getTeamId() == team.getId()){
				if(dialogIndenforTidsfrist == null)
					dialogIndenforTidsfrist = d.getCompletedWithinDeadline();
				else
					dialogIndenforTidsfrist += d.getCompletedWithinDeadline();

				if(dialogTotal == null)
					dialogTotal = d.getTotalActivities();
				else
					dialogTotal += d.getTotalActivities();
			}

		if(includeSupport){
			for(SupportTaskData s : this.supportTaskData)
				if(s.getMonth().getQuarter() == quarter){
					if(supportIndenforTidsfrist == null)
						supportIndenforTidsfrist = s.getCompletedWithinDeadline();
					else
						supportIndenforTidsfrist += s.getCompletedWithinDeadline();

					if(supportTotal == null)
						supportTotal = s.getTotalTasks();
					else
						supportTotal += s.getTotalTasks();
				}

			for(EmailData e : this.emailData)
				if(e.getMonth().getQuarter() == quarter){
					if(kampagneMailsIndenforTidsfrist == null)
						kampagneMailsIndenforTidsfrist = e.getCampaignEmailsCompletedWithinDeadline();
					else
						supportIndenforTidsfrist += e.getCampaignEmailsCompletedWithinDeadline();

					if(kampagneMailsTotal == null)
						kampagneMailsTotal = e.getTotalCampaignEmailsReceived();
					else
						kampagneMailsTotal += e.getTotalCampaignEmailsReceived();
				}
		}

		return new OpgaverIndenforTidsfrist(dialogIndenforTidsfrist, 
				dialogTotal, 
				supportIndenforTidsfrist, 
				supportTotal, 
				kampagneMailsIndenforTidsfrist, 
				kampagneMailsTotal);
	}
	public OpgaverIndenforTidsfrist getAktiviteterIndenforTidsfrist(Team team, boolean includeSupport){
		Integer dialogIndenforTidsfrist = null;
		Integer dialogTotal = null;

		Integer supportIndenforTidsfrist = null;
		Integer supportTotal = null;

		Integer kampagneMailsIndenforTidsfrist = null;
		Integer kampagneMailsTotal = null;

		for(DialogActivityData d : this.dialogActivityData)
			if(d.getTeamId() == team.getId()){
				if(dialogIndenforTidsfrist == null)
					dialogIndenforTidsfrist = d.getCompletedWithinDeadline();
				else
					dialogIndenforTidsfrist += d.getCompletedWithinDeadline();

				if(dialogTotal == null)
					dialogTotal = d.getTotalActivities();
				else
					dialogTotal += d.getTotalActivities();
			}

		if(includeSupport){
			for(SupportTaskData s : this.supportTaskData){
				if(supportIndenforTidsfrist == null)
					supportIndenforTidsfrist = s.getCompletedWithinDeadline();
				else
					supportIndenforTidsfrist += s.getCompletedWithinDeadline();

				if(supportTotal == null)
					supportTotal = s.getTotalTasks();
				else
					supportTotal += s.getTotalTasks();
			}

			for(EmailData e : this.emailData){
				if(kampagneMailsIndenforTidsfrist == null)
					kampagneMailsIndenforTidsfrist = e.getCampaignEmailsCompletedWithinDeadline();
				else
					supportIndenforTidsfrist += e.getCampaignEmailsCompletedWithinDeadline();

				if(kampagneMailsTotal == null)
					kampagneMailsTotal = e.getTotalCampaignEmailsReceived();
				else
					kampagneMailsTotal += e.getTotalCampaignEmailsReceived();
			}
		}

		return new OpgaverIndenforTidsfrist(dialogIndenforTidsfrist, 
				dialogTotal, 
				supportIndenforTidsfrist, 
				supportTotal, 
				kampagneMailsIndenforTidsfrist, 
				kampagneMailsTotal);
	}
	public OpgaverIndenforTidsfrist getAktiviteterIndenforTidsfrist(EMonth month, ArrayList<Integer> teamIds, boolean includeSupport){
		Integer dialogIndenforTidsfrist = null;
		Integer dialogTotal = null;

		Integer supportIndenforTidsfrist = null;
		Integer supportTotal = null;

		Integer kampagneMailsIndenforTidsfrist = null;
		Integer kampagneMailsTotal = null;

		for(DialogActivityData d : this.dialogActivityData)
			if(d.getMonth() == month && teamIds.contains(d.getTeamId())){
				if(dialogIndenforTidsfrist == null)
					dialogIndenforTidsfrist = d.getCompletedWithinDeadline();
				else
					dialogIndenforTidsfrist += d.getCompletedWithinDeadline();

				if(dialogTotal == null)
					dialogTotal = d.getTotalActivities();
				else
					dialogTotal += d.getTotalActivities();
			}

		if(includeSupport){
			for(SupportTaskData s : this.supportTaskData)
				if(s.getMonth() == month){
					if(supportIndenforTidsfrist == null)
						supportIndenforTidsfrist = s.getCompletedWithinDeadline();
					else
						supportIndenforTidsfrist += s.getCompletedWithinDeadline();

					if(supportTotal == null)
						supportTotal = s.getTotalTasks();
					else
						supportTotal += s.getTotalTasks();
				}

			for(EmailData e : this.emailData)
				if(e.getMonth() == month){
					if(kampagneMailsIndenforTidsfrist == null)
						kampagneMailsIndenforTidsfrist = e.getCampaignEmailsCompletedWithinDeadline();
					else
						kampagneMailsIndenforTidsfrist += e.getCampaignEmailsCompletedWithinDeadline();

					if(kampagneMailsTotal == null)
						kampagneMailsTotal = e.getTotalCampaignEmailsReceived();
					else
						kampagneMailsTotal += e.getTotalCampaignEmailsReceived();
				}	
		}

		return new OpgaverIndenforTidsfrist(dialogIndenforTidsfrist, 
				dialogTotal, 
				supportIndenforTidsfrist, 
				supportTotal, 
				kampagneMailsIndenforTidsfrist, 
				kampagneMailsTotal);		
	}
	public OpgaverIndenforTidsfrist getAktiviteterIndenforTidsfrist(int quarter, ArrayList<Integer> teamIds, boolean includeSupport){
		Integer dialogIndenforTidsfrist = null;
		Integer dialogTotal = null;

		Integer supportIndenforTidsfrist = null;
		Integer supportTotal = null;

		Integer kampagneMailsIndenforTidsfrist = null;
		Integer kampagneMailsTotal = null;

		for(DialogActivityData d : this.dialogActivityData)
			if(d.getMonth().getQuarter() == quarter && teamIds.contains(d.getTeamId())){
				if(dialogIndenforTidsfrist == null)
					dialogIndenforTidsfrist = d.getCompletedWithinDeadline();
				else
					dialogIndenforTidsfrist += d.getCompletedWithinDeadline();

				if(dialogTotal == null)
					dialogTotal = d.getTotalActivities();
				else
					dialogTotal += d.getTotalActivities();
			}

		if(includeSupport){
			for(SupportTaskData s : this.supportTaskData)
				if(s.getMonth().getQuarter() == quarter){
					if(supportIndenforTidsfrist == null)
						supportIndenforTidsfrist = s.getCompletedWithinDeadline();
					else
						supportIndenforTidsfrist += s.getCompletedWithinDeadline();

					if(supportTotal == null)
						supportTotal = s.getTotalTasks();
					else
						supportTotal += s.getTotalTasks();
				}

			for(EmailData e : this.emailData)
				if(e.getMonth().getQuarter() == quarter){
					if(kampagneMailsIndenforTidsfrist == null)
						kampagneMailsIndenforTidsfrist = e.getCampaignEmailsCompletedWithinDeadline();
					else
						kampagneMailsIndenforTidsfrist += e.getCampaignEmailsCompletedWithinDeadline();

					if(kampagneMailsTotal == null)
						kampagneMailsTotal = e.getTotalCampaignEmailsReceived();
					else
						kampagneMailsTotal += e.getTotalCampaignEmailsReceived();
				}
		}

		return new OpgaverIndenforTidsfrist(dialogIndenforTidsfrist, 
				dialogTotal, 
				supportIndenforTidsfrist, 
				supportTotal, 
				kampagneMailsIndenforTidsfrist, 
				kampagneMailsTotal);
	}
	public OpgaverIndenforTidsfrist getAktiviteterIndenforTidsfrist(ArrayList<Integer> teamIds, boolean includeSupport){
		Integer dialogIndenforTidsfrist = null;
		Integer dialogTotal = null;

		Integer supportIndenforTidsfrist = null;
		Integer supportTotal = null;

		Integer kampagneMailsIndenforTidsfrist = null;
		Integer kampagneMailsTotal = null;

		for(DialogActivityData d : this.dialogActivityData)
			if(teamIds.contains(d.getTeamId())){
				if(dialogIndenforTidsfrist == null)
					dialogIndenforTidsfrist = d.getCompletedWithinDeadline();
				else
					dialogIndenforTidsfrist += d.getCompletedWithinDeadline();

				if(dialogTotal == null)
					dialogTotal = d.getTotalActivities();
				else
					dialogTotal += d.getTotalActivities();
			}

		if(includeSupport){
			for(SupportTaskData s : this.supportTaskData){
				if(supportIndenforTidsfrist == null)
					supportIndenforTidsfrist = s.getCompletedWithinDeadline();
				else
					supportIndenforTidsfrist += s.getCompletedWithinDeadline();

				if(supportTotal == null)
					supportTotal = s.getTotalTasks();
				else
					supportTotal += s.getTotalTasks();
			}

			for(EmailData e : this.emailData){
				if(kampagneMailsIndenforTidsfrist == null)
					kampagneMailsIndenforTidsfrist = e.getCampaignEmailsCompletedWithinDeadline();
				else
					kampagneMailsIndenforTidsfrist += e.getCampaignEmailsCompletedWithinDeadline();

				if(kampagneMailsTotal == null)
					kampagneMailsTotal = e.getTotalCampaignEmailsReceived();
				else
					kampagneMailsTotal += e.getTotalCampaignEmailsReceived();
			}	
		}

		return new OpgaverIndenforTidsfrist(dialogIndenforTidsfrist, 
				dialogTotal, 
				supportIndenforTidsfrist, 
				supportTotal, 
				kampagneMailsIndenforTidsfrist, 
				kampagneMailsTotal);
	}
	public OpgaverIndenforTidsfrist getAktiviteterIndenforTidsfristCenter(EMonth month, boolean includeSupport){
		Integer dialogIndenforTidsfrist = null;
		Integer dialogTotal = null;

		Integer supportIndenforTidsfrist = null;
		Integer supportTotal = null;

		Integer kampagneMailsIndenforTidsfrist = null;
		Integer kampagneMailsTotal = null;

		for(DialogActivityData d : this.dialogActivityData)
			if(d.getMonth() == month){
				if(dialogIndenforTidsfrist == null)
					dialogIndenforTidsfrist = d.getCompletedWithinDeadline();
				else
					dialogIndenforTidsfrist += d.getCompletedWithinDeadline();

				if(dialogTotal == null)
					dialogTotal = d.getTotalActivities();
				else
					dialogTotal += d.getTotalActivities();
			}

		if(includeSupport)
			for(SupportTaskData s : this.supportTaskData)
				if(s.getMonth() == month){
					if(supportIndenforTidsfrist == null)
						supportIndenforTidsfrist = s.getCompletedWithinDeadline();
					else
						supportIndenforTidsfrist += s.getCompletedWithinDeadline();

					if(supportTotal == null)
						supportTotal = s.getTotalTasks();
					else
						supportTotal += s.getTotalTasks();
				}

		return new OpgaverIndenforTidsfrist(dialogIndenforTidsfrist, 
				dialogTotal, 
				supportIndenforTidsfrist, 
				supportTotal, 
				kampagneMailsIndenforTidsfrist, 
				kampagneMailsTotal);		
	}
	public OpgaverIndenforTidsfrist getAktiviteterIndenforTidsfristCenter(int quarter, boolean includeSupport){
		Integer dialogIndenforTidsfrist = null;
		Integer dialogTotal = null;

		Integer supportIndenforTidsfrist = null;
		Integer supportTotal = null;

		Integer kampagneMailsIndenforTidsfrist = null;
		Integer kampagneMailsTotal = null;

		for(DialogActivityData d : this.dialogActivityData)
			if(d.getMonth().getQuarter() == quarter){
				if(dialogIndenforTidsfrist == null)
					dialogIndenforTidsfrist = d.getCompletedWithinDeadline();
				else
					dialogIndenforTidsfrist += d.getCompletedWithinDeadline();

				if(dialogTotal == null)
					dialogTotal = d.getTotalActivities();
				else
					dialogTotal += d.getTotalActivities();
			}

		if(includeSupport){
			for(SupportTaskData s : this.supportTaskData)
				if(s.getMonth().getQuarter() == quarter){
					if(supportIndenforTidsfrist == null)
						supportIndenforTidsfrist = s.getCompletedWithinDeadline();
					else
						supportIndenforTidsfrist += s.getCompletedWithinDeadline();

					if(supportTotal == null)
						supportTotal = s.getTotalTasks();
					else
						supportTotal += s.getTotalTasks();
				}

			for(EmailData e : this.emailData)
				if(e.getMonth().getQuarter() == quarter){
					if(kampagneMailsIndenforTidsfrist == null)
						kampagneMailsIndenforTidsfrist = e.getCampaignEmailsCompletedWithinDeadline();
					else
						supportIndenforTidsfrist += e.getCampaignEmailsCompletedWithinDeadline();

					if(kampagneMailsTotal == null)
						kampagneMailsTotal = e.getTotalCampaignEmailsReceived();
					else
						kampagneMailsTotal += e.getTotalCampaignEmailsReceived();
				}
		}

		return new OpgaverIndenforTidsfrist(dialogIndenforTidsfrist, 
				dialogTotal, 
				supportIndenforTidsfrist, 
				supportTotal, 
				kampagneMailsIndenforTidsfrist, 
				kampagneMailsTotal);		
	}
	public OpgaverIndenforTidsfrist getAktiviteterIndenforTidsfristCenter(boolean includeSupport){
		Integer dialogIndenforTidsfrist = null;
		Integer dialogTotal = null;

		Integer supportIndenforTidsfrist = null;
		Integer supportTotal = null;

		Integer kampagneMailsIndenforTidsfrist = null;
		Integer kampagneMailsTotal = null;

		for(DialogActivityData d : this.dialogActivityData){
			if(dialogIndenforTidsfrist == null)
				dialogIndenforTidsfrist = d.getCompletedWithinDeadline();
			else
				dialogIndenforTidsfrist += d.getCompletedWithinDeadline();

			if(dialogTotal == null)
				dialogTotal = d.getTotalActivities();
			else
				dialogTotal += d.getTotalActivities();
		}

		if(includeSupport){
			for(SupportTaskData s : this.supportTaskData){
				if(supportIndenforTidsfrist == null)
					supportIndenforTidsfrist = s.getCompletedWithinDeadline();
				else
					supportIndenforTidsfrist += s.getCompletedWithinDeadline();

				if(supportTotal == null)
					supportTotal = s.getTotalTasks();
				else
					supportTotal += s.getTotalTasks();
			}

			for(EmailData e : this.emailData){
				if(kampagneMailsIndenforTidsfrist == null)
					kampagneMailsIndenforTidsfrist = e.getCampaignEmailsCompletedWithinDeadline();
				else
					supportIndenforTidsfrist += e.getCampaignEmailsCompletedWithinDeadline();

				if(kampagneMailsTotal == null)
					kampagneMailsTotal = e.getTotalCampaignEmailsReceived();
				else
					kampagneMailsTotal += e.getTotalCampaignEmailsReceived();
			}
		}

		return new OpgaverIndenforTidsfrist(dialogIndenforTidsfrist, 
				dialogTotal, 
				supportIndenforTidsfrist, 
				supportTotal, 
				kampagneMailsIndenforTidsfrist, 
				kampagneMailsTotal);
	}

	public HaandteringsGrad getKontaktFørOpsigelser(EMonth month, int teamId){
		Integer antal = null;
		Integer succes = null;

		for(DialogActivityData a : this.dialogActivityData)
			if(a.getMonth() == month && a.getTeamId() == teamId){
				if(antal == null)
					antal = a.getContactBeforeCancelation();
				else
					antal += a.getContactBeforeCancelation();

				if(succes == null)
					succes = a.getContactBeforeCancelationWithinDeadline();
				else
					succes += a.getContactBeforeCancelationWithinDeadline();
			}

		return new MiscHaandteringsgrad(succes, antal);
	}
	public HaandteringsGrad getKontaktFørOpsigelser(int quarter, int teamId){
		Integer antal = null;
		Integer succes = null;

		for(DialogActivityData a : this.dialogActivityData)
			if(a.getMonth().getQuarter() == quarter && a.getTeamId() == teamId){
				if(antal == null)
					antal = a.getContactBeforeCancelation();
				else
					antal += a.getContactBeforeCancelation();

				if(succes == null)
					succes = a.getContactBeforeCancelationWithinDeadline();
				else
					succes += a.getContactBeforeCancelationWithinDeadline();
			}

		return new MiscHaandteringsgrad(succes, antal);
	}
	public HaandteringsGrad getKontaktFørOpsigelser(int teamId){
		Integer antal = null;
		Integer succes = null;

		for(DialogActivityData a : this.dialogActivityData)
			if(a.getTeamId() == teamId){
				if(antal == null)
					antal = a.getContactBeforeCancelation();
				else
					antal += a.getContactBeforeCancelation();

				if(succes == null)
					succes = a.getContactBeforeCancelationWithinDeadline();
				else
					succes += a.getContactBeforeCancelationWithinDeadline();
			}

		return new MiscHaandteringsgrad(succes, antal);
	}

	public HaandteringsGrad getEkspeditionsServiceKvalitet(EMonth month){
		Integer antal = null;
		Integer succes = null;

		for(TCMMiscStats t : this.miscStats)
			if(t.getMonth() == month){
				if(antal == null)
					antal = t.getEkKvalitetAntal();
				else
					antal += t.getEkKvalitetAntal();

				if(succes == null)
					succes = t.getEkKvalitetSucces();
				else
					succes += t.getEkKvalitetSucces();
			}

		return new MiscHaandteringsgrad(succes, antal);
	}
	public HaandteringsGrad getEkspeditionsServiceKvalitet(int quarter){
		Integer antal = null;
		Integer succes = null;

		for(TCMMiscStats t : this.miscStats)
			if(t.getMonth().getQuarter() == quarter){
				if(antal == null)
					antal = t.getEkKvalitetAntal();
				else
					antal += t.getEkKvalitetAntal();

				if(succes == null)
					succes = t.getEkKvalitetSucces();
				else
					succes += t.getEkKvalitetSucces();
			}

		return new MiscHaandteringsgrad(succes, antal);
	}
	public HaandteringsGrad getEkspeditionsServiceKvalitet(){
		Integer antal = null;
		Integer succes = null;

		for(TCMMiscStats t : this.miscStats){
			if(antal == null)
				antal = t.getEkKvalitetAntal();
			else
				antal += t.getEkKvalitetAntal();

			if(succes == null)
				succes = t.getEkKvalitetSucces();
			else
				succes += t.getEkKvalitetSucces();
		}

		return new MiscHaandteringsgrad(succes, antal);
	}

	public HaandteringsGrad getServiceCenterProduktivitet(EMonth month){
		Integer antal = null;
		Integer succes = null;

		for(TCMMiscStats t : this.miscStats)
			if(t.getMonth() == month){
				if(antal == null)
					antal = t.getScProduktivitetsMålAntal();
				else
					antal += t.getScProduktivitetsMålAntal();

				if(succes == null)
					succes = t.getScProduktivitetsMålSucces();
				else
					succes += t.getScProduktivitetsMålSucces();
			}

		return new MiscHaandteringsgrad(succes, antal);
	}
	public HaandteringsGrad getServiceCenterProduktivitet(int quarter){
		Integer antal = null;
		Integer succes = null;

		for(TCMMiscStats t : this.miscStats)
			if(t.getMonth().getQuarter() == quarter){
				if(antal == null)
					antal = t.getScProduktivitetsMålAntal();
				else
					antal += t.getScProduktivitetsMålAntal();

				if(succes == null)
					succes = t.getScProduktivitetsMålSucces();
				else
					succes += t.getScProduktivitetsMålSucces();
			}

		return new MiscHaandteringsgrad(succes, antal);
	}
	public HaandteringsGrad getServiceCenterProduktivitet(){
		Integer antal = null;
		Integer succes = null;

		for(TCMMiscStats t : this.miscStats){
			if(antal == null)
				antal = t.getScProduktivitetsMålAntal();
			else
				antal += t.getScProduktivitetsMålAntal();

			if(succes == null)
				succes = t.getScProduktivitetsMålSucces();
			else
				succes += t.getScProduktivitetsMålSucces();
		}

		return new MiscHaandteringsgrad(succes, antal);
	}

	public HaandteringsGrad getServiceCenterKvalitet(EMonth month){
		Integer antal = null;
		Integer succes = null;

		for(TCMMiscStats t : this.miscStats)
			if(t.getMonth() == month){
				if(antal == null)
					antal = t.getScKvalitetAntal();
				else
					antal += t.getScKvalitetAntal();

				if(succes == null)
					succes = t.getScKvalitetSucces();
				else
					succes += t.getScKvalitetSucces();
			}

		return new MiscHaandteringsgrad(succes, antal);
	}
	public HaandteringsGrad getServiceCenterKvalitet(int quarter){
		Integer antal = null;
		Integer succes = null;

		for(TCMMiscStats t : this.miscStats)
			if(t.getMonth().getQuarter() == quarter){
				if(antal == null)
					antal = t.getScKvalitetAntal();
				else
					antal += t.getScKvalitetAntal();

				if(succes == null)
					succes = t.getScKvalitetSucces();
				else
					succes += t.getScKvalitetSucces();
			}

		return new MiscHaandteringsgrad(succes, antal);
	}
	public HaandteringsGrad getServiceCenterKvalitet(){
		Integer antal = null;
		Integer succes = null;

		for(TCMMiscStats t : this.miscStats){
			if(antal == null)
				antal = t.getScKvalitetAntal();
			else
				antal += t.getScKvalitetAntal();

			if(succes == null)
				succes = t.getScKvalitetSucces();
			else
				succes += t.getScKvalitetSucces();
		}

		return new MiscHaandteringsgrad(succes, antal);
	}

	public HaandteringsGrad getDpWFCMPrognose(EMonth m){
		Integer prognoseKald = null;
		Integer differenceKald = null;

		for(TCMMiscStats s : this.miscStats)
			if(s.getMonth() == m){
				if(prognoseKald == null)
					prognoseKald = s.getDpWFCMPrognoseAntalKald();
				else
					prognoseKald += s.getDpWFCMPrognoseAntalKald();

				if(differenceKald == null)
					differenceKald = s.getDpWFCMPrognoseDifference();
				else
					differenceKald += s.getDpWFCMPrognoseDifference();
			}

		return new MiscHaandteringsgrad(differenceKald, prognoseKald);
	}
	public HaandteringsGrad getDpWFCMPrognose(int quarter){
		Integer prognoseKald = null;
		Integer differenceKald = null;

		for(TCMMiscStats s : this.miscStats)
			if(s.getMonth().getQuarter() == quarter){
				if(prognoseKald == null)
					prognoseKald = s.getDpWFCMPrognoseAntalKald();
				else
					prognoseKald += s.getDpWFCMPrognoseAntalKald();

				if(differenceKald == null)
					differenceKald = s.getDpWFCMPrognoseDifference();
				else
					differenceKald += s.getDpWFCMPrognoseDifference();
			}

		return new MiscHaandteringsgrad(differenceKald, prognoseKald);
	}
	public HaandteringsGrad getDpWFCMPrognose(){
		Integer prognoseKald = null;
		Integer differenceKald = null;

		for(TCMMiscStats s : this.miscStats){
			if(prognoseKald == null)
				prognoseKald = s.getDpWFCMPrognoseAntalKald();
			else
				prognoseKald += s.getDpWFCMPrognoseAntalKald();

			if(differenceKald == null)
				differenceKald = s.getDpWFCMPrognoseDifference();
			else
				differenceKald += s.getDpWFCMPrognoseDifference();
		}

		return new MiscHaandteringsgrad(differenceKald, prognoseKald);
	}

	public HaandteringsGrad getDpVidenbarometer(EMonth month){
		Integer antal = null;
		Integer succes = null;

		for(TCMMiscStats t : this.miscStats)
			if(t.getMonth() == month){
				if(antal == null)
					antal = t.getDpVidenbarometerAntal();
				else
					antal += t.getDpVidenbarometerAntal();

				if(succes == null)
					succes = t.getDpVidenbarometerSucces();
				else
					succes += t.getDpVidenbarometerSucces();
			}

		return new MiscHaandteringsgrad(succes, antal);
	}
	public HaandteringsGrad getDpVidenbarometer(int quarter){
		Integer antal = null;
		Integer succes = null;

		for(TCMMiscStats t : this.miscStats)
			if(t.getMonth().getQuarter() == quarter){
				if(antal == null)
					antal = t.getDpVidenbarometerAntal();
				else
					antal += t.getDpVidenbarometerAntal();

				if(succes == null)
					succes = t.getDpVidenbarometerSucces();
				else
					succes += t.getDpVidenbarometerSucces();
			}

		return new MiscHaandteringsgrad(succes, antal);
	}
	public HaandteringsGrad getDpVidenbarometer(){
		Integer antal = null;
		Integer succes = null;

		for(TCMMiscStats t : this.miscStats){
			if(antal == null)
				antal = t.getDpVidenbarometerAntal();
			else
				antal += t.getDpVidenbarometerAntal();

			if(succes == null)
				succes = t.getDpVidenbarometerSucces();
			else
				succes += t.getDpVidenbarometerSucces();
		}

		return new MiscHaandteringsgrad(succes, antal);
	}

	public HaandteringsGrad getDpAHTReduktion(EMonth month){
		return new MiscHaandteringsgrad(0, 0);
	}
	public HaandteringsGrad getDpAHTReduktion(int quarter){
		return new MiscHaandteringsgrad(0, 0);
	}	
	public HaandteringsGrad getDpAHTReduktion(){
		return new MiscHaandteringsgrad(0, 0);
	}

	public HaandteringsGrad getDpSmart(EMonth month){
		int antal = 0;
		int succes = 0;

		for(TCMMiscStats s : this.miscStats)
			if(s.getMonth() == month){
				antal = s.getDpSMARTAntal();
				succes = s.getDpSMARTSucces();

				break;
			}

		return new MiscHaandteringsgrad(succes, antal);
	}	
	public HaandteringsGrad getDpSmart(int quarter){
		int antal = 0;
		int succes = 0;

		for(TCMMiscStats s : this.miscStats)
			if(s.getMonth().getQuarter() == quarter){
				antal += s.getDpSMARTAntal();
				succes += s.getDpSMARTSucces();
			}

		return new MiscHaandteringsgrad(succes, antal);
	}
	public HaandteringsGrad getDpSmart(){
		int antal = 0;
		int succes = 0;

		for(TCMMiscStats s : this.miscStats){
			if(s.getMonth().before(EMonth.getCurrentMonth())){
				antal += s.getDpSMARTAntal();
				succes += s.getDpSMARTSucces();
			}
		}

		return new MiscHaandteringsgrad(succes, antal);
	}

	public Number getKaGennemførelseAfKonkurrence(EMonth month){
		Integer antal = null;

		for(TCMMiscStats s : this.miscStats)
			if(s.getMonth() == month)
				if(antal == null)
					antal = s.getKaGennemførteKonkurrencer();
				else
					antal += s.getKaGennemførteKonkurrencer();

		return new Number(antal);
	}
	public Number getKaGennemførelseAfKonkurrence(int quarter){
		Integer antal = null;

		for(TCMMiscStats s : this.miscStats)
			if(s.getMonth().getQuarter() == quarter)
				if(antal == null)
					antal = s.getKaGennemførteKonkurrencer();
				else
					antal += s.getKaGennemførteKonkurrencer();

		return new Number(antal);
	}
	public Number getKaGennemførelseAfKonkurrence(){
		Integer antal = null;

		for(TCMMiscStats s : this.miscStats){
			if(antal == null)
				antal = s.getKaGennemførteKonkurrencer();
			else
				antal += s.getKaGennemførteKonkurrencer();
		}
		return new Number(antal);
	}

	public Number getKaOmsætningAfKampagner(EMonth month){
		Integer antal = null;

		for(TCMMiscStats s : this.miscStats)
			if(s.getMonth() == month)
				if(antal == null)
					antal = s.getKaOmsætningAfNYKKampagnerISc();
				else
					antal += s.getKaOmsætningAfNYKKampagnerISc();

		return new Number(antal);
	}
	public Number getKaOmsætningAfKampagner(int quarter){
		Integer antal = null;

		for(TCMMiscStats s : this.miscStats)
			if(s.getMonth().getQuarter() == quarter)
				if(antal == null)
					antal = s.getKaOmsætningAfNYKKampagnerISc();
				else
					antal += s.getKaOmsætningAfNYKKampagnerISc();

		return new Number(antal);
	}
	public Number getKaOmsætningAfKampagner(){
		Integer antal = null;

		for(TCMMiscStats s : this.miscStats){
			if(antal == null)

				antal = s.getKaOmsætningAfNYKKampagnerISc();
			else
				antal += s.getKaOmsætningAfNYKKampagnerISc();
		}
		return new Number(antal);
	}

	public Number getKaKundetilfredshedsmåling(EMonth month){
		Integer antal = null;

		for(TCMMiscStats s : this.miscStats)
			if(s.getMonth() == month)
				if(antal == null)
					antal = s.getKaKundetilfredshedsMåling();
				else
					antal += s.getKaKundetilfredshedsMåling();

		return new Number(antal);
	}
	public Number getKaKundetilfredshedsmåling(int quarter){
		Integer antal = null;

		for(TCMMiscStats s : this.miscStats)
			if(s.getMonth().getQuarter() == quarter)
				if(antal == null)
					antal = s.getKaKundetilfredshedsMåling();
				else
					antal += s.getKaKundetilfredshedsMåling();

		return new Number(antal);
	}
	public Number getKaKundetilfredshedsmåling(){
		Integer antal = null;

		for(TCMMiscStats s : this.miscStats){
			if(antal == null)

				antal = s.getKaKundetilfredshedsMåling();
			else
				antal += s.getKaKundetilfredshedsMåling();
		}
		return new Number(antal);
	}

	public Number getKaOnlineKvalitet(EMonth month){
		Integer antal = null;

		for(TCMMiscStats s : this.miscStats)
			if(s.getMonth() == month)
				if(antal == null)
					antal = s.getKaOnlineKvalitet();
				else
					antal += s.getKaOnlineKvalitet();

		return new Number(antal);
	}
	public Number getKaOnlineKvalitet(int quarter){
		Integer antal = null;

		for(TCMMiscStats s : this.miscStats)
			if(s.getMonth().getQuarter() == quarter)
				if(antal == null)
					antal = s.getKaOnlineKvalitet();
				else
					antal += s.getKaOnlineKvalitet();

		return new Number(antal);		
	}
	public Number getKaOnlineKvalitet(){
		Integer antal = null;

		for(TCMMiscStats s : this.miscStats){
			if(antal == null)

				antal = s.getKaOnlineKvalitet();
			else
				antal += s.getKaOnlineKvalitet();
		}
		return new Number(antal);
	}

	public HaandteringsGrad getSPMailbesvarelse(EMonth month){
		int antal = 0;
		int succes = 0;

		for(TCMMiscStats s : this.miscStats)
			if(s.getMonth() == month){
				antal = s.getPsMailbesvarelseAntal();
				succes = s.getPsMailbesvarelseSucces();

				break;
			}

		return new MiscHaandteringsgrad(succes, antal);
	}
	public HaandteringsGrad getSPMailbesvarelse(int quarter){
		int antal = 0;
		int succes = 0;

		for(TCMMiscStats s : this.miscStats)
			if(s.getMonth().getQuarter() == quarter){
				antal += s.getPsMailbesvarelseAntal();
				succes += s.getPsMailbesvarelseSucces();
			}

		return new MiscHaandteringsgrad(succes, antal);
	}
	public HaandteringsGrad getSPMailbesvarelse(){
		int antal = 0;
		int succes = 0;

		for(TCMMiscStats s : this.miscStats){
			antal += s.getPsMailbesvarelseAntal();
			succes += s.getPsMailbesvarelseSucces();

		}

		return new MiscHaandteringsgrad(succes, antal);
	}

	public HaandteringsGrad getSPOprettelseAfMedarbejdere(EMonth month){
		int antal = 0;
		int succes = 0;

		for(TCMMiscStats s : this.miscStats)
			if(s.getMonth() == month){
				antal = s.getPsOprettelseAfMedarbejdereAntal();
				succes = s.getPsOprettelseAfMedarbejdereSucces();

				break;
			}

		return new MiscHaandteringsgrad(succes, antal);
	}
	public HaandteringsGrad getSPOprettelseAfMedarbejdere(int quarter){
		int antal = 0;
		int succes = 0;

		for(TCMMiscStats s : this.miscStats)
			if(s.getMonth().getQuarter() == quarter){
				antal += s.getPsOprettelseAfMedarbejdereAntal();
				succes += s.getPsOprettelseAfMedarbejdereSucces();
			}

		return new MiscHaandteringsgrad(succes, antal);
	}
	public HaandteringsGrad getSPOprettelseAfMedarbejdere(){
		int antal = 0;
		int succes = 0;

		for(TCMMiscStats s : this.miscStats){
			antal += s.getPsOprettelseAfMedarbejdereAntal();
			succes += s.getPsOprettelseAfMedarbejdereSucces();

		}

		return new MiscHaandteringsgrad(succes, antal);
	}

	public Number getSPOptimeringAfProcesser(EMonth month){
		Integer antal = null;
		for(TCMMiscStats s : this.miscStats)
			if(s.getMonth() == month)
				if(antal == null)
					antal = s.getPsOptimeringAfProcesser();
				else
					antal += s.getPsOptimeringAfProcesser();

		return new Number(antal);
	}
	public Number getSPOptimeringAfProcesser(int quarter){
		Integer antal = null;
		for(TCMMiscStats s : this.miscStats)
			if(s.getMonth().getQuarter() == quarter)
				if(antal == null)
					antal = s.getPsOptimeringAfProcesser();
				else
					antal += s.getPsOptimeringAfProcesser();

		return new Number(antal);
	}
	public Number getSPOptimeringAfProcesser(){
		Integer antal = null;

		for(TCMMiscStats s : this.miscStats){
			if(antal == null)
				antal = s.getPsOptimeringAfProcesser();
			else
				antal += s.getPsOptimeringAfProcesser();
		}
		return new Number(antal);
	}

	public HaandteringsGrad getProjektLeverancer(EMonth month){
		Integer antal = null;
		Integer succes = null;

		for(TCMMiscStats s : this.miscStats)
			if(s.getMonth() == month)
				if(antal == null){
					antal = s.getPrProjektLeverancerSucces();
					succes = s.getPrProjektLeverancerAntal();
				}else{
					antal += s.getPrProjektLeverancerSucces();
					succes += s.getPrProjektLeverancerAntal();
				}

		return new MiscHaandteringsgrad(succes, antal);
	}
	public HaandteringsGrad getProjektLeverancer(int quarter){
		Integer antal = null;
		Integer succes = null;

		for(TCMMiscStats s : this.miscStats)
			if(s.getMonth().getQuarter() == quarter)
				if(antal == null){
					antal = s.getPrProjektLeverancerSucces();
					succes = s.getPrProjektLeverancerAntal();
				}else{
					antal += s.getPrProjektLeverancerSucces();
					succes += s.getPrProjektLeverancerAntal();
				}

		return new MiscHaandteringsgrad(succes, antal);
	}
	public HaandteringsGrad getProjektLeverancer(){
		Integer antal = null;
		Integer succes = null;

		for(TCMMiscStats s : this.miscStats){
			if(antal == null){

				antal = s.getPrProjektLeverancerSucces();
				succes = s.getPrProjektLeverancerAntal();
			}else{
				antal += s.getPrProjektLeverancerSucces();
				succes += s.getPrProjektLeverancerAntal();
			}
		}
		return new MiscHaandteringsgrad(succes, antal);
	}

	public HaandteringsGrad getProjektTelefoniforvaltning(EMonth month){
		Integer antal = null;
		Integer succes = null;

		for(TCMMiscStats s : this.miscStats)
			if(s.getMonth() == month)
				if(antal == null){
					antal = s.getPrSpørgsmålOmkringTelefonforvaltningSucces();
					succes = s.getPrSpørgsmålOmkringTelefonforvaltningAntal();
				}else{
					antal += s.getPrSpørgsmålOmkringTelefonforvaltningSucces();
					succes += s.getPrSpørgsmålOmkringTelefonforvaltningAntal();
				}

		return new MiscHaandteringsgrad(succes, antal);
	}
	public HaandteringsGrad getProjektTelefoniforvaltning(int quarter){
		Integer antal = null;
		Integer succes = null;

		for(TCMMiscStats s : this.miscStats)
			if(s.getMonth().getQuarter() == quarter)
				if(antal == null){
					antal = s.getPrSpørgsmålOmkringTelefonforvaltningSucces();
					succes = s.getPrSpørgsmålOmkringTelefonforvaltningAntal();
				}else{
					antal += s.getPrSpørgsmålOmkringTelefonforvaltningSucces();
					succes += s.getPrSpørgsmålOmkringTelefonforvaltningAntal();
				}

		return new MiscHaandteringsgrad(succes, antal);
	}
	public HaandteringsGrad getProjektTelefoniforvaltning(){
		Integer antal = null;
		Integer succes = null;

		for(TCMMiscStats s : this.miscStats)
			if(antal == null){
				antal = s.getPrSpørgsmålOmkringTelefonforvaltningSucces();
				succes = s.getPrSpørgsmålOmkringTelefonforvaltningAntal();
			}else{
				antal += s.getPrSpørgsmålOmkringTelefonforvaltningSucces();
				succes += s.getPrSpørgsmålOmkringTelefonforvaltningAntal();
			}

		return new MiscHaandteringsgrad(succes, antal);
	}

	public Number getProjektMedarbejderMøder(EMonth month){
		Integer antal = null;

		for(TCMMiscStats s : this.miscStats)
			if(s.getMonth() == month)
				if(antal == null)
					antal = s.getPrMedarbejdermøder();
				else
					antal += s.getPrMedarbejdermøder();

		return new Number(antal);
	}
	public Number getProjektMedarbejderMøder(int quarter){
		Integer antal = null;

		for(TCMMiscStats s : this.miscStats)
			if(s.getMonth().getQuarter() == quarter)
				if(antal == null)
					antal = s.getPrMedarbejdermøder();
				else
					antal += s.getPrMedarbejdermøder();

		return new Number(antal);
	}
	public Number getProjektMedarbejderMøder(){
		Integer antal = null;

		for(TCMMiscStats s : this.miscStats)
			if(antal == null)
				antal = s.getPrMedarbejdermøder();
			else
				antal += s.getPrMedarbejdermøder();

		return new Number(antal);
	}

	public Boolean getProjektPlanproces(EMonth month){
		Boolean planproces = null;

		for(TCMMiscStats s : this.miscStats)
			if(s.getMonth() == month)
				return s.getPrPlanproces();

		return planproces;
	}
	public Boolean getProjektPlanproces(int quarter){
		Boolean planproces = null;

		for(TCMMiscStats s : this.miscStats)
			if(s.getMonth().getQuarter() == quarter && s.getPrPlanproces())
				planproces = true;

		return planproces;
	}
	public Boolean getProjektPlanproces(){
		Boolean planproces = null;

		for(TCMMiscStats s : this.miscStats)
			if(s.getPrPlanproces())
				planproces = true;

		return planproces;
	}

	public HaandteringsGrad getAdministrationServiceService(EMonth month){
		int antal = 0;
		int succes = 0;

		for(TCMMiscStats s : this.miscStats)
			if(s.getMonth() == month){
				antal = s.getAsServiceAntal();
				succes = s.getAsServiceSucces();

				break;
			}

		return new MiscHaandteringsgrad(succes, antal);
	}
	public HaandteringsGrad getAdministrationServiceService(int quarter){
		int antal = 0;
		int succes = 0;

		for(TCMMiscStats s : this.miscStats)
			if(s.getMonth().getQuarter() == quarter){
				antal += s.getAsServiceAntal();
				succes += s.getAsServiceSucces();
			}

		return new MiscHaandteringsgrad(succes, antal);
	}
	public HaandteringsGrad getAdministrationServiceService(){
		int antal = 0;
		int succes = 0;

		for(TCMMiscStats s : this.miscStats){
			antal += s.getAsServiceAntal();
			succes += s.getAsServiceSucces();

		}

		return new MiscHaandteringsgrad(succes, antal);
	}

	public HaandteringsGrad getBetalingsadministrationDigitalisering(EMonth month){
		int antal = 0;
		int succes = 0;

		for(TCMMiscStats s : this.miscStats)
			if(s.getMonth() == month){
				antal = s.getBaDigitaliseringAntal();
				succes = s.getBaDigitaliseringSucces();

				break;
			}

		return new MiscHaandteringsgrad(succes, antal);
	}
	public HaandteringsGrad getBetalingsadministrationDigitalisering(int quarter){
		int antal = 0;
		int succes = 0;

		for(TCMMiscStats s : this.miscStats)
			if(s.getMonth().getQuarter() == quarter){
				antal += s.getBaDigitaliseringAntal();
				succes += s.getBaDigitaliseringSucces();
			}

		return new MiscHaandteringsgrad(succes, antal);
	}
	public HaandteringsGrad getBetalingsadministrationDigitalisering(){
		int antal = 0;
		int succes = 0; 

		for(TCMMiscStats s : this.miscStats){
			antal += s.getBaDigitaliseringAntal();
			succes += s.getBaDigitaliseringSucces();
		}


		return new MiscHaandteringsgrad(succes, antal);
	}
}
