package com.nykredit.kundeservice.tcm;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import org.joda.time.LocalTime;

/**
 * 
 * Formats used when presenting data in tables
 * 
 * @author thra
 * 
 */
public class Formatter {
		
	public static String toNumberString(Double number){
		if(number == null)
			return "-";
		
		NumberFormat formatter = new DecimalFormat("#,###,##0.#");
		
		return formatter.format(number);
	}
	public static String toNumberString(Integer number){
		if(number == null)
			return "-";
		
		NumberFormat formatter = new  DecimalFormat("##,###,##0.#");
		
		return formatter.format(number);
	}
	public static String toPercentString(Double number){
		if(number == null)
			return null;
		
		NumberFormat formatter = new DecimalFormat("##0");
		
		return formatter.format(Math.round(number * 100)) + "%";
	}
	public static String toPercentString(Integer number){
		if(number == null)
			return null;
		
		NumberFormat formatter = new DecimalFormat("##0");
		
		return formatter.format(Math.round(number * 100)) + "%";
	} 
	
	public static String toTimeString(Integer seconds){
		return new LocalTime(0, 
							 seconds / 60, 
							 seconds % 60).toString("mm:ss");
	}

	public static String toBooleanString(Boolean value){
		if(value == null || value == false)
			return "Ikke opfyldt";
		else
			return "Opfyldt";
	}
}
