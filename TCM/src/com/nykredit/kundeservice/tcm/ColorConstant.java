package com.nykredit.kundeservice.tcm;

import java.awt.Color;

public class ColorConstant {
	public static Color red = new Color(255,60,60);
	public static Color green = new Color(146,208,87);
	public static Color yellow = new Color(255,255,87);
}
