package com.nykredit.kundeservice.workforce;

import java.util.ArrayList;
import java.util.Date;

import com.nykredit.kundeservice.baseTypes.Agent;
import com.nykredit.kundeservice.workforce.ExceptionInWorkDay.WorkdayExceptionType;

public class AgentSchedule extends Agent {

	private boolean isFullTime;
	
	private ArrayList<WorkDay> workDays = new ArrayList<WorkDay>();
	
	public boolean getIsFullTime() {
		return this.isFullTime;
	}
	public void setIsFullTime(boolean isFullTime) {
		this.isFullTime = isFullTime;
	}
	
	public AgentSchedule(int id, String initials, String firstName,	String lastName) {
		super(id, initials, firstName, lastName);
	}

	public ArrayList<WorkDay> getWorkdays(){
		return this.workDays;
	}
	public WorkDay getWorkDay(Date date){
	    WorkDay	returnDay = null;
	    
		for (WorkDay w : this.workDays){
			if (w.getDate().equals(date)){
				returnDay = w;
				break;
			}
		}
		
		return returnDay;
	}
	
	public ArrayList<WorkDay> getDaysContainingException(WorkdayExceptionType type){
		ArrayList<WorkDay> workDays = new ArrayList<WorkDay>();
		
		for (WorkDay w : this.getWorkdays()){
			for (ExceptionInWorkDay e : w.getExceptions())
				if (e.getType() == WorkdayExceptionType.SYG){
					workDays.add(w);
					break;
				}
		}
		
		return workDays;
	}
}
