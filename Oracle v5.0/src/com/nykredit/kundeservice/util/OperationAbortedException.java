package com.nykredit.kundeservice.util;

import com.nykredit.kundeservice.data.DopeException;

public class OperationAbortedException extends DopeException {

	private static final long serialVersionUID = 1L;

	public OperationAbortedException(Exception e, String exceptionMsg, String dopeErrorMsg) {
		super(e, exceptionMsg, dopeErrorMsg);
	}
	
}
