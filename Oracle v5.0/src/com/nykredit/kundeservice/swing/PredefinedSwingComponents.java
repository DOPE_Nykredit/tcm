package com.nykredit.kundeservice.swing;

import java.awt.Component;
import java.awt.Font;
import java.awt.Rectangle;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSlider;
import javax.swing.JToggleButton;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.nykredit.kundeservice.util.SF;

public class PredefinedSwingComponents {
	
	public JButton Button(String text, int x, int y, final SF sf) {return Button(text,x, y, 210, 25, sf);}
	public JButton Button(String text, int x, int y, int w, int h, final SF sf) {
		JButton but = new JButton(text);
		but.setFocusPainted(false);
		but.setBounds(new Rectangle(x, y, w, h));
		but.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseReleased(java.awt.event.MouseEvent e) {
				sf.func();
			}
		});
		return but;
	}
	public JToggleButton Toggle(int x, int y, JToggleButton toggle) {
		toggle.setFocusPainted(false);
		toggle.setBounds(new Rectangle(x, y, 210, 25));
		return toggle;
	}
	public JLabel CopyRight(String Agent, int left, int top) {
		JLabel copyrightLable = new JLabel(Agent + " � Nykredit");
		copyrightLable.setBounds(new Rectangle(left, top, 85, 16));
		copyrightLable.setFont(new Font("Dialog", Font.PLAIN, 10));
		return copyrightLable;
	}
	public void PromtClosing(String program, Component app) {
		Object[] options = {"Ja", "Nej"};
		int confirmed = JOptionPane.showOptionDialog(app,
			"Er du sikker p� du vil lukke programmet?",
			"Afslut " + program,
			JOptionPane.YES_NO_OPTION,
			JOptionPane.QUESTION_MESSAGE,
			null,     	//do not use a custom Icon
			options,  	//the titles of buttons
			options[0] 	//default button title
		);
		
		if (confirmed == 0){System.exit(0);}
	}
/** Defines a slider with snap to tick
 */	public JSlider Slider(JSlider JS, int x, int y, int w, int min, int max, int small, int big, final SF sf) {
		JS.setSnapToTicks(true);
		JS.setPaintTicks(true);
		JS.setPaintLabels(true);
		JS.setMinimum(min);
		JS.setMaximum(max);
		JS.setMinorTickSpacing(small);
		JS.setMajorTickSpacing(big);
		JS.setValue(min);
		JS.setBounds(new Rectangle(x, y, w, 43));
		JS.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				sf.func();
			}
		});
/*		JS.setUI(new MetalSliderUI() { 
			protected TrackListener createTrackListener(final JSlider slider) { 
				return new TrackListener() { 
					public void mouseDragged(MouseEvent e) { 
						if(!slider.getSnapToTicks() || slider.getMajorTickSpacing()==0) { 
							super.mouseDragged(e); 
							return; 
						} 
						//case JSlider.HORIZONTAL: 
						int halfThumbWidth    = thumbRect.width / 2; 
						int trackLength = trackRect.width; 
						int trackLeft  = trackRect.x-halfThumbWidth; 
						int trackRight = trackRect.x+(trackRect.width-1)+halfThumbWidth; 
						int xPos = e.getX(); 
						int snappedPos = xPos; 
						if (xPos <= trackLeft) { 
							snappedPos = trackLeft; 
						} else if (xPos >= trackRight) { 
							snappedPos = trackRight; 
						} else { 
							//int tickSpacing = slider.getMajorTickSpacing(); 
							//float actualPixelsForOneTick = trackLength * tickSpacing 
							//                                 / (float)slider.getMaximum(); 
							// a problem if you choose to set a negative MINIMUM for 
							// the JSlider; the calculated drag-positions are wrong. 
							// Fixed by bobndrew: 
							int possibleTickPositions=slider.getMaximum()-slider.getMinimum(); 
							int tickSpacing = (slider.getMinorTickSpacing()==0) 
							? slider.getMajorTickSpacing() 
									: slider.getMinorTickSpacing(); 
							float actualPixelsForOneTick = trackLength * tickSpacing 
							/ (float)possibleTickPositions; 
							xPos -= trackLeft; 
							snappedPos=(int)( 
									(Math.round(xPos/actualPixelsForOneTick)*actualPixelsForOneTick)+0.5) 
									+trackLeft; 
							offset = 0; 
						} 
						MouseEvent me = new MouseEvent( 
								e.getComponent(), e.getID(), e.getWhen(), e.getModifiers(), 
								snappedPos, e.getY(), 
								e.getXOnScreen(), e.getYOnScreen(), 
								e.getClickCount(), e.isPopupTrigger(), e.getButton()); 
						super.mouseDragged(me); 
					} 
				}; 
			} 
		});
*/		return JS;
	}
}
