package com.nykredit.kundeservice.swing;
import java.awt.event.KeyEvent;

import javax.swing.JTextField;

public class AgentTextField extends JTextField {
	private static final long serialVersionUID = 1L;
	
	private String badchars = "`~!@#$%^&*()_+=\\|\"':;?/>.<,-���{[]}�� ";
	private int limit = 4;
	
	public void setMaxLength(int val) {
		limit = val;
	}
	
	public int getMaxLength() {
		return limit;
	}
	
	public void processKeyEvent(KeyEvent ev) {
		char c = ev.getKeyChar();
		
		if(badchars.indexOf(c) > -1) {
			ev.consume();
			return;
		}
		if(		!(ev.getKeyCode() ==  8) & // Backspace
				!(ev.getKeyCode() ==127) & // Delete
				!(ev.getKeyCode() == 37) & // Left arrow
				!(ev.getKeyCode() == 39) & // Right arrow
				getDocument().getLength() >= limit)
			ev.consume();
		else super.processKeyEvent(ev);
	}

	public String getBadchars() {
		return badchars;
	}

	public void setBadchars(String badchars) {
		this.badchars = badchars;
	}


}