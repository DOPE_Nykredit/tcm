package com.nykredit.kundeservice.swing;
import java.awt.Component;

import javax.swing.JDesktopPane;

public class DopeDesktopPane extends JDesktopPane {
	private static final long serialVersionUID = 1L;
	
	public Component getComponentByName(String Name) {
		Component comp = null;
		for (int i=0 ; i<getComponentCount() ; i++) {
			comp = getComponent(i);
			if (comp.getName() != null) {if (comp.getName().endsWith(Name)) {
				return comp;
			}}
		}
		System.out.println("Unknown component !!");
		return getComponent(0);
	}
}