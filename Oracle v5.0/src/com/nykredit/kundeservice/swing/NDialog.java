package com.nykredit.kundeservice.swing;

import java.awt.image.BufferedImage;
import java.net.URL;
import javax.imageio.ImageIO;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

public class NDialog extends JDialog {

	private static final long serialVersionUID = 1L;
	private URL url = this.getClass().getClassLoader().getResource("com/nykredit/kundeservice/images/NykreditLogo.jpg");
	
	public NDialog(){
		BufferedImage logo = null;
		try {
			logo = ImageIO.read(url);
			this.setIconImage(logo);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this,e,"Fejl",JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
			}
	}

}
