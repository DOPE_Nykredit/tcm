package com.nykredit.kundeservice.swing;

import java.util.Comparator;

public class NComparator implements Comparator<Object>{
	
	private String getNumericChar(String str){
		
		String returnString = "";
		if(str.length()>0)
			if(Character.isDigit(str.charAt(0))){
				char[] chr = str.toCharArray();
				for (char c:chr){
					if(Character.isDigit(c))
						returnString += c;			
				}
			}
		return returnString;
	}

	@Override
	public int compare(Object o1, Object o2) {
		
    	String string1 = getNumericChar(o1.toString());
    	String string2 = getNumericChar(o2.toString());
    	
    	Integer i1 = 0;
    	try{
    		i1 = Integer.parseInt(string1);
    	}catch(Exception e){}
    	Integer i2 = 0;
    	
    	try{
    		i2 = Integer.parseInt(string2);
    	}catch(Exception e){}
    	
        return i1.compareTo(i2);
	}

}
