package com.nykredit.kundeservice.swing;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.UIManager;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;

import com.nykredit.kundeservice.data.CTIRConnection;
import com.nykredit.kundeservice.util.SF;

import java.sql.SQLException;


public class Access extends NDialog {
	private static final long serialVersionUID = 1L;

	private SF				sf = null;
	private CTIRConnection			dwh = new CTIRConnection();
	private String			agent = System.getenv("USERNAME").toUpperCase(),
							initialer = "",
							teams = "";
	private boolean			state1 = false,
							state2 = false;
	private JLabel			labelKode = new JLabel("Kode",JLabel.LEFT),
							labelGentag = new JLabel("Gentag",JLabel.LEFT);
	private JButton 		buttonSkiftKode = new JButton("Skift kode"),
							buttonLogInd = new JButton("Log ind");
	private JTextField 		textFieldNavn = new JTextField(),
							textFieldTeam = new JTextField();
	private AgentTextField 	agentTextField = new AgentTextField();
	private JPasswordField 	passwordField = new JPasswordField(),
							passwordFieldRepeat = new JPasswordField();

/*	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX Class Constructor XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 */
	public static void main(String[] args) {
		try {UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());}
		catch (Exception e) {}
	    
		new Access(null,null,null);
	}

	public Access(String initialer,String teams,SF sf) {
		this.sf = sf;
		if (initialer != null) this.initialer = " AND INITIALER IN (" + initialer + ")";
		if (teams != null) this.teams = " AND VT.TEAM IN (" + teams + ")";
		try {
			dwh.Connect();
		} catch (SQLException e) {
		}
		this.setContentPane(getMainPane());
		this.setLocationRelativeTo(this.getRootPane());
		this.setTitle("Access v1.0");
		this.setResizable(false);
		this.setAlwaysOnTop(true);
		this.setModal(true);
		this.setModalExclusionType( ModalExclusionType.NO_EXCLUDE );
		this.pack();
		this.setVisible(true);
	}
/*	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX GUI Builder XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 */

	private JPanel getMainPane() {
		JPanel p = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		
		c.insets = new Insets(5,5,0,5);
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridy = 0;
		p.add(new JLabel("Initialer",JLabel.LEFT),c);
		
		c.insets = new Insets(0,5,0,5);
		c.gridy = 1;		
		p.add(getTextField(agentTextField,new SF(){public void func(){event();}}),c);
		
		c.gridy = 2;
		p.add(new JLabel("Navn",JLabel.LEFT),c);
		
		c.gridy = 3;
		p.add(getTextField(textFieldNavn,null),c);
		
		c.gridy = 4;
		p.add(new JLabel("Team",JLabel.LEFT),c);
		
		c.gridy = 5;
		p.add(getTextField(textFieldTeam,null),c);
		
		c.gridy = 6;
		p.add(labelKode,c);
		
		c.gridy = 7;
		p.add(getPasswordField(passwordField),c);
		
		c.gridy = 8;
		p.add(labelGentag,c);
		
		c.gridy = 9;
		p.add(getPasswordField(passwordFieldRepeat),c);
		
		c.gridy = 10;
		c.gridwidth = 2;
		p.add(getButton(buttonLogInd,new SF(){public void func(){login();}}),c);
		
		c.gridy = 11;
		c.insets = new Insets(0,5,5,5);
		p.add(getButton(buttonSkiftKode,new SF(){public void func(){skift();}}),c);
		
		p.setOpaque(false);
		agentTextField.setText(agent);
		agentTextField.setEditable(true);
		agentTextField.setEnabled(true);
		event();
		return p;
	}

	private JButton getButton(JButton b,final SF sf) {
		b.setFocusPainted(false);
		b.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseReleased(java.awt.event.MouseEvent e) {
				sf.func();
			}
		});
		return b;
	}
	private JTextField getTextField(JTextField t,final SF sf) {
		t.setHorizontalAlignment(JTextField.CENTER);
		t.setPreferredSize(new Dimension(150,20));
		t.setEditable(false);
		t.setEnabled(false);
		t.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent keyEvent) {
				sf.func();
			}
		});
		return t;
	}
	private JPasswordField getPasswordField(JPasswordField p) {
		p.setHorizontalAlignment(JTextField.CENTER);
		return p;
	}
/*	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX Private Functions XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 */
	private void update() {
			 if	(!state1) 	{ GlKode(state2); }
		else if (!state2)	{ NyKode("Opret"); }
		else				{ NyKode("Skift"); }
		this.repaint();
	}
	private void NyKode(String event) {
		labelKode.setText("Ny kode");
		passwordField.setText(null);
		passwordField.setEditable(true);
		passwordField.setEnabled(true);
		buttonSkiftKode.setVisible(false);
		labelGentag.setVisible(true);
		passwordFieldRepeat.setText(null);
		passwordFieldRepeat.setVisible(true);
		buttonLogInd.setEnabled(true);
		buttonLogInd.setText(event + " & log ind");
		this.pack();
	}
	private void GlKode(boolean Enabled) {
		labelKode.setText("Kode");
		passwordField.setText(null);
		passwordField.setEditable(Enabled);
		passwordField.setEnabled(Enabled);
		buttonSkiftKode.setVisible(true);
		buttonSkiftKode.setEnabled(Enabled);
		labelGentag.setVisible(false);
		passwordFieldRepeat.setVisible(false);
		buttonLogInd.setEnabled(Enabled);
		buttonLogInd.setText("Log ind");
		this.pack();
	}
	private void Warning() {
		Object[] options = {"Pr�v igen"};
		JOptionPane.showOptionDialog(this,
			"De indtastede kodeord skal v�re ens,\nog have minimum 6 tegn.",
			"Kodeord er ikke sikkert",
			JOptionPane.OK_OPTION,
			JOptionPane.QUESTION_MESSAGE,
			null,     	//do not use a custom Icon
			options,  	//the titles of buttons
			options[0] 	//default button title
		);
	}
	private void event() {
		agent = agentTextField.getText().toUpperCase();
		state1 = false;
		state2 = false;
		if (getAgent()) {
			if (getTeam()) {
				if (testAgent()) {
					state2 = true;
				} else {
					state1 = true;
				}
			}
		} else {
			textFieldTeam.setText(null);
		}
		update();
	}
	private void skift() {
		if (testPass()) {
			state1 = true;
		} else {
			passwordField.setText(null);
		}
		update();
	}
	private void login() {
		if (state1) {
			if (safePass()) {
				if (state2) {
					skiftPass();
				} else {
					opretPass();
				}
				LOGIN();
				return;
			} else {
				Warning();
				passwordField.setText(null);
				passwordFieldRepeat.setText(null);
				return;
			}
		} else {
			if (testPass()) {
				LOGIN();
				return;
			}
			passwordField.setText(null);
		}
	}
	private void LOGIN() {
		sf.func();
		this.dispose();
	}
/*	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX Oracle Statements XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 */
	private boolean getAgent() {
		try {
			textFieldNavn.setText(dwh.Hent_String(1, 
			"SELECT FORNAVN || ' ' || EFTERNAVN AS NAVN	" +
			"FROM KS_DRIFT.AGENTER_LISTE				" +
			"WHERE INITIALER = '" + agent + "'			" +
			initialer									));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return !textFieldNavn.getText().equalsIgnoreCase("");
	}
	private boolean getTeam() {
		try {
			textFieldTeam.setText(dwh.Hent_String(1, 
			"SELECT AT.TEAM_NAVN						" +
			"FROM KS_DRIFT.V_TEAM_DATO VT				" +
			"INNER JOIN KS_DRIFT.AGENTER_TEAMS AT		" +
			"ON VT.TEAM = AT.TEAM						" +
			"AND VT.INITIALER = '" + agent + "'			" +
			"AND VT.DATO = TRUNC(SYSDATE)				" +
			teams										));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return !textFieldTeam.getText().equalsIgnoreCase("");
	}
	private boolean testAgent() {
		try {
			return !"".equalsIgnoreCase(dwh.Hent_String(1, 
			"SELECT INITIALER							" +
			"FROM KS_DRIFT.AGENTER_PASSWORD				" +
			"WHERE INITIALER = '"+ agent +"'			"));
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	@SuppressWarnings("deprecation")
	private boolean testPass() {
		String pass = passwordField.getText();
		try {
			return !"".equalsIgnoreCase(dwh.Hent_String(1, 
			"SELECT INITIALER							" +
			"FROM KS_DRIFT.AGENTER_PASSWORD				" +
			"WHERE INITIALER = '"+ agent +"'			" +
			"AND PASSWORD = HEXTORAW(MD5('"+ pass +"'))	"));
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	@SuppressWarnings("deprecation")
	private boolean safePass() {
		String	pass = passwordField.getText();
		String	pas1 = passwordFieldRepeat.getText();
		return	pass.equalsIgnoreCase(pas1) &
				pass.length() >= 6;
	}
	@SuppressWarnings("deprecation")
	private void opretPass() {
		String pass = passwordField.getText();
		try {
			dwh.Hent_intet( 
			"INSERT INTO KS_DRIFT.AGENTER_PASSWORD	" +
			"(INITIALER,PASSWORD)					" +
			"VALUES									" +
			"('"+ agent +"',MD5('"+ pass +"')		");
		} catch (SQLException e) {}
	}
	@SuppressWarnings("deprecation")
	private void skiftPass() {
		String pass = passwordField.getText();
		try {
			dwh.Hent_intet( 
			"UPDATE KS_DRIFT.AGENTER_PASSWORD		" +
			"SET PASSWORD = MD5('"+ pass +"')		" +
			"WHERE INITIALER = '"+ agent +"'		");
		} catch (SQLException e) {}
	}

/*	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX Public Functions XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 */
	
	public String getInitials(){
		return agent;
	}
	
	
	
}
