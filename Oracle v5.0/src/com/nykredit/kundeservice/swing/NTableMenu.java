package com.nykredit.kundeservice.swing;

import java.awt.Event;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;

class NTableMenu extends JPopupMenu {
	private static final long serialVersionUID = 1L;
	
	NTable table;

	public NTableMenu(NTable jTable){
		this.table = jTable;
		
		JMenuItem MarkAllMenuItem = new JMenuItem("Mark�r alt");
		MarkAllMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, Event.CTRL_MASK, true));
		
		MarkAllMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				NTableMenu.this.table.selectAll();
			}
		});
		
		super.add(MarkAllMenuItem);
		
		JMenuItem copySelectedCells = new JMenuItem((jTable.getSelectedColumnCount() > 1 || jTable.getSelectedRowCount() > 1) ? "Kopier celler" : "Kopier celle");
		if(jTable.getSelectedColumnCount() == 0)
			copySelectedCells.setEnabled(false);
		
		copySelectedCells.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, Event.CTRL_MASK, true));
		
		copySelectedCells.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				javax.swing.Action copy = NTableMenu.this.table.getActionMap().get("copy");
				ActionEvent ae = new ActionEvent(NTableMenu.this.table, ActionEvent.ACTION_PERFORMED, "");
				copy.actionPerformed(ae); 
			}
		});
		
		super.add(copySelectedCells);
			
		JMenuItem copyTableImage = new JMenuItem("Kopier billede");
		copyTableImage.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, Event.CTRL_MASK, true));
		
		copyTableImage.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				NTableMenu.this.table.copyTableImageToClipboard();
			}
		});	
		
		super.add(copyTableImage);
	}
} 