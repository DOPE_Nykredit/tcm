package com.nykredit.kundeservice.swing;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.image.BufferedImage;

import javax.swing.JTable;
import javax.swing.table.JTableHeader;

public class NTable extends JTable {
	
	private static final long serialVersionUID = 1L;

	public NTable() {
		this.setDefaultRenderer(Object.class, new BasicRenderer()); 
		this.setColumnSelectionAllowed(false);
		this.setRowSelectionAllowed(false);
		this.setCellSelectionEnabled(true);
		this.getTableHeader().setReorderingAllowed(false);
		
		this.addMouseListener(new NTablePopClickListener(this));
	}
	
	private class BasicRenderer extends NTableRenderer{
		private static final long serialVersionUID = 1L;
		public Component getTableCellRendererComponent (JTable table,Object obj, boolean isSelected, boolean hasFocus, int row, int column) {
			Component cell = super.getTableCellRendererComponent(table, obj, isSelected, hasFocus, row, column);
			super.addMarking(cell, isSelected,new Color(0, 0, 128), 80);
			return cell;
	    }
	}
	
    public BufferedImage getTableImage(){ 
        JTableHeader tableHeaderComp = super.getTableHeader(); 
          
        int totalWidth = tableHeaderComp.getWidth(); 
        int totalHeight = tableHeaderComp.getHeight() + super.getHeight(); 
          
        BufferedImage tableImage = new BufferedImage(totalWidth, totalHeight, BufferedImage.TYPE_INT_RGB); 
 
        Graphics2D g2D = (Graphics2D)tableImage.getGraphics(); 
          
        tableHeaderComp.paint(g2D); 
          
        g2D.translate(0, tableHeaderComp.getHeight()); 
          
        super.paint(g2D); 
          
        return tableImage;  
    }
    public void copyTableImageToClipboard(){
    	ImageSelection clipboardImage = new ImageSelection(this.getTableImage());
    	clipboardImage.copyImageToClipboard();
    }
    
    private class ImageSelection implements Transferable {    
    	private Image image;       
    	
    	public void copyImageToClipboard(){        
    		ImageSelection imageSelection = new ImageSelection(image);        
    		Toolkit toolkit = Toolkit.getDefaultToolkit();        
    		toolkit.getSystemClipboard().setContents(imageSelection, null);    
    	}       
    	
    	public ImageSelection(Image image) {        
    		this.image = image;    
    	}
    	
    	public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException {        
    		if (flavor.equals(DataFlavor.imageFlavor) == false){            
    			throw new UnsupportedFlavorException(flavor);        
    		}        
    		
    		return image;    
    	}       
    	
    	public boolean isDataFlavorSupported(DataFlavor flavor){        
    		return flavor.equals(DataFlavor.imageFlavor);    
    	}       
    	
    	public DataFlavor[] getTransferDataFlavors(){        
    		return new DataFlavor[] {            
    			DataFlavor.imageFlavor        
    		};    
    	}
    }
}