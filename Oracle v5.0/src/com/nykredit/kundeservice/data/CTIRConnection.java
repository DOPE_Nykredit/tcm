package com.nykredit.kundeservice.data;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;

import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleTypes;
import oracle.jdbc.pool.OracleDataSource;

public class CTIRConnection {
	private Connection con = null;
	private Statement db = null;
	private String LogBes = "'nsOracle v1.3'";
	private String LogTid = "SYSDATE";
	public String LogWho = "'"+System.getenv("COMPUTERNAME")+": "+System.getenv("USERNAME").toUpperCase()+"'";
	private int LogAnt = 0;

	public void Connect() throws SQLException {
    	DriverManager.registerDriver(new oracle.jdbc.OracleDriver());
		OracleDataSource ods = new OracleDataSource();
		ods.setUser("KS_DRIFT");
		ods.setPassword("KS_DRIFT");
		ods.setDriverType("thin");
		ods.setServerName("ORAAIXP2");
		ods.setPortNumber(1527);
		ods.setDatabaseName("CTIR");
		con = ods.getConnection();
		ods.close();
		db = con.createStatement();
    }
	
	public void ConnectKSAPP() throws SQLException {
    	DriverManager.registerDriver(new oracle.jdbc.OracleDriver());
		OracleDataSource ods = new OracleDataSource();
		ods.setUser("KS_APPLIKATION");
		ods.setPassword("APPLIKATION_4711");
		ods.setDriverType("thin");
		ods.setServerName("ORAAIXP2");
		ods.setPortNumber(1527);
		ods.setDatabaseName("CTIR");
		con = ods.getConnection();
		ods.close();
		db = con.createStatement();
    }
	
	
	public void SetupSpy(String program) {
		LogBes = "'"+program+"'";
    }
	public void Close() throws SQLException {
    	db.close();
		con.close();
    }

	private void set_spy() throws SQLException {
        SimpleDateFormat tid = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        LogTid = tid.format(new java.util.Date());
        LogTid = "TO_DATE('" + LogTid + "','YYYY-MM-DD HH24:MI:SS')";
	    db.execute(
	    "   INSERT INTO KS_DRIFT.NYK_BATCH_STATISTICS               			"+
	    "   (JOBNAME,JOBDATE,LOGTEXT,LOGNUMBER)                     			"+
		"	VALUES ("+LogBes+","+LogTid+","+LogWho+",-1)						");
	    
	}
	private void add_spy() throws SQLException {
	    db.executeQuery(
	    "   UPDATE KS_DRIFT.NYK_BATCH_STATISTICS                    			"+
	    "   SET LOGNUMBER = -"+LogAnt+"                         				"+
	    "   WHERE JOBNAME =  "+LogBes+"                        					"+
	    "   AND   LOGTEXT =  "+LogWho+"                        					"+
	    "   AND   JOBDATE =  "+LogTid+"											");
	
	}
	

	public ResultSet Hent_tabel(String Execute) throws SQLException {
	    return db.executeQuery(Execute);
	}
	
	public int executeCallableStatement(String query) throws SQLException{
		OracleCallableStatement cs = (OracleCallableStatement) con.prepareCall(query);
		cs.registerOutParameter(1, OracleTypes.NUMBER);
		cs.execute();
		return cs.getInt(1);
	}

	public String Hent_String(int col, String Execute) throws SQLException {

			ResultSet rs;
			rs = db.executeQuery(Execute);
		    rs.next();
		    return rs.getString(col);
	}
	public int Hent_int(int col, String Execute) throws SQLException {

			ResultSet rs;
			rs = db.executeQuery(Execute);
		    rs.next();
		    return rs.getInt(col);

	}
	public void Hent_intet(String Execute) throws SQLException {
	    db.execute(Execute);
	}
	public String getColumnName(ResultSet rs, int col) {
		if (rs == null) {
			return "";
		}
		try {
			ResultSetMetaData rsM = rs.getMetaData();
			return rsM.getColumnName(col);
		} catch (SQLException e) {
			System.err.println("Exception: " + e);
			return "";
		}
	}
	public int getColumnCount(ResultSet rs) {
		if (rs == null) {
			return 0;
		}
		try {
			ResultSetMetaData rsM = rs.getMetaData();
			return rsM.getColumnCount();
		}
		catch (SQLException e) {
			System.err.println("Exception: " + e);
			return 0;
		}
	}
	public void SS_spy() {
	    try {
	    	LogAnt++;
		    if (LogAnt == 1) {set_spy();}
		    else {add_spy();}
		} catch (SQLException e) {}
	}


}