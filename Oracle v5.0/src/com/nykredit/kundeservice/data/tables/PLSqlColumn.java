package com.nykredit.kundeservice.data.tables;

public interface PLSqlColumn {
	
	public String getName();
	public String getAlias();
	
}
