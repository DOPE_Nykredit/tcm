package com.nykredit.kundeservice.data.sql;

import com.nykredit.kundeservice.data.DopeDBException;

public interface ISqlListener {

	public void executed();
	public void executionFailed(DopeDBException e);
	
}
