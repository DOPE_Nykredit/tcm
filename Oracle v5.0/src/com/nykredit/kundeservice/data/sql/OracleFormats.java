package com.nykredit.kundeservice.data.sql;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;

public final class OracleFormats {

	public static String convertDate(java.util.Date dateToConvert){
		if (dateToConvert == null)
			return "null";
		else
			return OracleFormats.convertString(new SimpleDateFormat("dd-MM-yyyy").format(dateToConvert));
	}
	public static String convertCalendar(Calendar calendarToConvert){
		if (calendarToConvert == null)
			return "null";
		else
			return OracleFormats.convertDate(calendarToConvert.getTime());
	}
	public static String convertString(String stringToConvert){
		if (stringToConvert == null)
			return "null";
		else
			return "'" + stringToConvert + "'";
	}
	public static String convertDateTime(DateTime dateTimeToConvert){
		if(dateTimeToConvert == null)
			return "null";
		else
			return "'" + dateTimeToConvert.toString("dd-MM-yyyy") + "'";
	}
	public static String convertLocalDate(LocalDate localDateToConvert){
		if(localDateToConvert == null)
			return "null";
		else
			return "'" + localDateToConvert.toString("dd-MM-yyyy") + "'";
	}
}